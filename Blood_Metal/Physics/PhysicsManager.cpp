#include "PhysicsManager.h"

PhysicsManager::PhysicsManager(Ogre::SceneManager* sceneManager,
                               const Ogre::AxisAlignedBox & bounds,
                               const Ogre::Vector3& gravity
                               )
{
    _sceneManager = sceneManager;

    // Physics World
    _world = new DynamicsWorld(_sceneManager, bounds, gravity, true, true, 10000);

    // Debug World
    _debugDrawer = new DebugDrawer();
    _world->setDebugDrawer(_debugDrawer);

    _debugNode = sceneManager->getRootSceneNode()->createChildSceneNode("debugDrawer", Ogre::Vector3::ZERO);
    _debugNode->attachObject(static_cast <Ogre::SimpleRenderable*> (_debugDrawer));
}

PhysicsManager::~PhysicsManager()
{
}

DynamicsWorld *PhysicsManager::getWorld()
{
    return _world;
}


