#ifndef COLLISIONCLASS_H
#define COLLISIONCLASS_H

#include <btBulletCollisionCommon.h>

class CollisionClass
{
public:

    enum CollisionType{
        CHARACTER,
        PLAYER,
        ENEMY,
        DESTRUCTOR,
        INDESTRUCTIBLE,
        DESTRUCTIBLE,
        CRATE_LIFE,
        CRATE_SHOT,
        CRATE_CANNON,
        CRATE_POINTS,
        START,
        FINISH,
        OTHER
    };


    CollisionClass() {}
    virtual void getHit() = 0;
    virtual void setCollisionType(int type) = 0;
    virtual int getCollisionType() = 0;

protected:
    int CollisionType;


};


#endif // COLLISIONCLASS_H
