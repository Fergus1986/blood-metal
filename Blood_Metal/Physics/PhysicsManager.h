#ifndef PHYSICSMANAGER_H
#define PHYSICSMANAGER_H

// Shape types
#define BOX         0
#define CONVEX      2
#define CYLINDER    3
#define SPHERE      4
#define TRIMESH     5
#define CAPSULE     6


#include <OGRE/OgreSingleton.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreSceneNode.h>

#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"

#include <OgreBullet/Dynamics/OgreBulletDynamicsWorld.h>
#include <OgreBullet/Collisions/Debug/OgreBulletCollisionsDebugDrawer.h>
#include <OgreBullet/Collisions/OgreBulletCollisionsShape.h>
#include <OgreBullet/Collisions/OgreBulletCollisionsRay.h>

#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>

#include <OgreBullet/Collisions/OgreBulletCollisions.h>

#include "CollisionClass.h"


using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;

class PhysicsManager

{
public:
    PhysicsManager(Ogre::SceneManager *sceneManager,
                   const Ogre::AxisAlignedBox &bounds,
                   const Ogre::Vector3 &gravity);
    ~PhysicsManager();

    // Singleton
    static PhysicsManager& getSingleton ();
    static PhysicsManager* getSingletonPtr ();


    DynamicsWorld *getWorld();

private:
    Ogre::SceneManager* _sceneManager;
    Ogre::SceneNode* _debugNode;

    DynamicsWorld* _world;
    DebugDrawer* _debugDrawer;
};

#endif // PHYSICSMANAGER_H
