#include "Object3D.h"

Object3D::Object3D()
{
}

Object3D::~Object3D()
{
//    std::cout << "Destroying object3D" << std::endl;

    if(_physcis){
        _sceneManager->getRootSceneNode()->removeAndDestroyChild(_node->getName());
        _physicsManager->getWorld()->getBulletDynamicsWorld()->removeRigidBody(this->_body->getBulletRigidBody());
        _physicsManager->getWorld()->removeObject(this->_body);
    }else{
        _sceneManager->getRootSceneNode()->removeAndDestroyChild(_node->getName());
    }
}

// graphical object with full new physical object
Object3D::Object3D(const Ogre::String name,
                   const Ogre::Vector3 position,
                   const Ogre::Quaternion rotation,
                   const float restitution,
                   const float friction,
                   const float mass,
                   const int shapeType,
                   Ogre::SceneManager *sceneManager,
                   PhysicsManager *physicsManager)
{
    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    _name = name;
    _position = position;
    _rotation = rotation;
    _restitution = restitution;
    _friction = friction;
    _mass = mass;
    _shapeType = shapeType;

    _physcis = true;

    createGraphicalObject();
    setGraphicalObject();
    createBody();
    createPhysicalObject();
    setPhysicalObject();
}

Object3D::Object3D(const Ogre::String name,
                   const Ogre::Vector3 position,
                   const Ogre::Quaternion rotation,
                   Ogre::SceneManager *sceneManager)
{
    _sceneManager = sceneManager;

    _name = name;
    _position = position;
    _rotation = rotation;
    _restitution = -1;
    _friction = -1;
    _mass = -1;
    _shapeType = -1;

    _physcis = false;

    createGraphicalObject();
    setGraphicalObject();
    _node->setOrientation(_rotation);
    _node->setPosition(_position);

}

void Object3D::setMaterial(const Ogre::String &material)
{
    _entity->setMaterialName(_name.append("_").append(material));
}

Ogre::SceneNode *Object3D::getNode()
{
    return _node;
}

RigidBody *Object3D::getRigidBody()
{
    return _body;
}

CollisionShape *Object3D::getShape()
{
    return _shape;
}

void Object3D::createGraphicalObject()
{
    _entity = _sceneManager->createEntity(_name + ".mesh");
}


void Object3D::setGraphicalObject()
{
    _node = _sceneManager->getRootSceneNode()->createChildSceneNode();
    _node->attachObject(_entity);
}

void Object3D::createBody()
{
    _body = new RigidBody(_node->getName(), _physicsManager->getWorld());
//   std::cout << "createBody: " << _node->getName() << std::endl;
}

void Object3D::createPhysicalObject()
{
    _shape = NULL;
    _converter = new OgreBulletCollisions:: StaticMeshToShapeConverter( _entity);
    Ogre::AxisAlignedBox boundingB = _entity->getBoundingBox();
    Ogre::Vector3 size = boundingB.getSize();

    switch(_shapeType){

    case BOX:
        size /= 2;   // El tamano en Bullet se indica desde el centro
        size *= 0.98f;   // Bullet margin is a bit bigger so we need a smaller size
        _shape = new OgreBulletCollisions::BoxCollisionShape(size);
        break;

    case CONVEX:
        _shape =_converter->createConvex();
        break;

    case CYLINDER:
        _shape =_converter->createCylinder();
        break;

    case SPHERE:
        _shape = _converter->createSphere();
        break;

    case TRIMESH:
        _shape = _converter->createTrimesh();
        break;

    case CAPSULE:
        _shape = new OgreBulletCollisions::CapsuleCollisionShape(size.x/4, size.y/2, Ogre::Vector3(0, 1, 0));
        break;

    default:
        _shape =_converter->createBox();
        break;
    }

//    std::cout << "createPhysicalObject: " << _node->getName() << std::endl;
}

void Object3D::setPhysicalObject()
{
    _body->setShape(_node, _shape, _restitution, _friction, _mass, _position, _rotation);
//    std::cout << "PhysicalObject: " << _node->getName() << std::endl;
}
