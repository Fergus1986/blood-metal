#ifndef OBJECT3D_H
#define OBJECT3D_H

#include <sstream>

#include "PhysicsManager.h"
#include "AdvancedOgreFramework/AdvancedOgreFramework.hpp"

#include <OGRE/OgreString.h>
#include <OGRE/OgreSceneNode.h>


#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/OgreBulletCollisionsShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsSphereShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsConvexHullShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCylinderShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCapsuleShape.h>

#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include "CollisionClass.h"

class Object3D
{
public:
    Object3D();
    ~Object3D();

    // Graphical object with full physical object
    Object3D(const Ogre::String name,
             const Ogre::Vector3 position,
             const Ogre::Quaternion rotation,
             const float restitution,
             const float friction,
             const float mass,
             const int shapeType,
             Ogre::SceneManager *sceneManager,
             PhysicsManager* physicsManager);

    // Graphical object without physical object
    Object3D(const Ogre::String name,
             const Ogre::Vector3 position,
             const Ogre::Quaternion rotation,
             Ogre::SceneManager *sceneManager);


    void setMaterial(const Ogre::String& material);

    Ogre::SceneNode* getNode();
    OgreBulletDynamics::RigidBody* getRigidBody();
    OgreBulletCollisions::CollisionShape* getShape();

protected:
    Ogre::SceneManager* _sceneManager;
    PhysicsManager *_physicsManager;


    // Graphical object
    Ogre::String _name;
    Ogre::SceneNode* _node;
    Ogre::Entity* _entity;
    Ogre::String _material;

    // Physical object
    bool _physcis;
    OgreBulletDynamics::RigidBody* _body;
    OgreBulletCollisions::StaticMeshToShapeConverter *_converter;
    OgreBulletCollisions::CollisionShape* _shape;
    float _restitution;
    float _friction;
    float _mass;
    int _shapeType;

    // Position & Rotation
    Ogre::Vector3 _position;
    Ogre::Quaternion _rotation;

private:
    void createGraphicalObject();
    void setGraphicalObject();
    void createBody();
    void createPhysicalObject();
    void setPhysicalObject();


};

#endif // OBJECT3D_H
