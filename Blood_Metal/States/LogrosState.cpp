#include "LogrosState.hpp"

LogrosState::LogrosState()
{
    m_bQuit = false;
}

void LogrosState::enter()
{

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering LogrosState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "LogrosSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    m_pCamera = m_pSceneMgr->createCamera("AcercaCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    _overlayManager= Ogre::OverlayManager::getSingletonPtr();
    _ceguiSystem = CEGUI::System::getSingletonPtr();
    m_bQuit = false;
    createScene();
    buildGUI();
    mostrarLogros();
    CEGUI::MouseCursor::getSingleton().show();

}

void LogrosState::createScene()
{
    _overlayLogros = _overlayManager->getByName("Logros");
    _overlayLogros->show();
}

void LogrosState::buildGUI()
{
    _gui = CEGUI::WindowManager::getSingleton().loadWindowLayout("logros.layout");
    _backButton = CEGUI::WindowManager::getSingleton().getWindow("Logros/Volver");
    _backButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&LogrosState::exit, this));
    _gui->addChildWindow(_backButton);
    CEGUI::System::getSingleton().setGUISheet(_gui);
    _logr = new Achievement();

}


void LogrosState::mostrarLogros()
{
    std::vector<_achievementStruct> v;
    int i=0;
    std::stringstream auxNombre,auxPuntos,auxNombrePlayer,auxPuntosPlayer;

    v = this->_logr->getAchievements();

    for(i;i<TAM_MAX_LOGROS && i < v.size();i++){
        auxNombre << "Logros/Nombre" << i;
        auxPuntos << "Logros/Puntuacion"<< i;
        textNombre = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow(auxNombre.str()));
        //textNombre->setText(v[i]._nombre);
        auxNombrePlayer << v[i]._name;
        textNombre->setProperty("Text","[colour='FF000000']" + auxNombrePlayer.str());

        textPuntos = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow(auxPuntos.str()));
        //textPuntos->setText(static_cast<ostringstream*>( &(ostringstream() << v[i]._puntos))->str());
        auxPuntosPlayer << v[i]._score;
        textPuntos->setProperty("Text","[colour='FF000000']" + auxPuntosPlayer.str());
        //limpieza del stringstream
        auxNombre.str(std::string());
        auxPuntos.str(std::string());
        auxNombrePlayer.str(std::string());
        auxPuntosPlayer.str(std::string());
    }
}

void LogrosState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo LogrosState...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    CEGUI::MouseCursor::getSingleton().hide();
    _overlayLogros->hide();
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo LogrosState exito...");
}

void LogrosState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;

    if(m_bQuit == true)
    {
        changeAppState((findByName("MenuState")));
    }
}

bool LogrosState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE)){
        OgreFramework::getSingletonPtr()->m_pLog->logMessage("Se pulso escape en AcercaState...");
        this->m_bQuit=true;
    }

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
    return true;
}

bool LogrosState::keyReleased(const OIS::KeyEvent &keyEventRef)
{  
    return true;
}

bool LogrosState::mouseMoved(const OIS::MouseEvent &evt)
{
    _ceguiSystem->injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
    // Scroll wheel
    if (evt.state.Z.rel) _ceguiSystem->injectMouseWheelChange(evt.state.Z.rel / 120.0f);
    return true;
}

bool LogrosState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
    return true;
}

bool LogrosState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
    return true;
}

bool LogrosState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool LogrosState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool LogrosState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    return true;
}

bool LogrosState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    return true;
}

CEGUI::MouseButton LogrosState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

bool LogrosState::exit(const CEGUI::EventArgs &e)
{
    m_bQuit = true;
}

