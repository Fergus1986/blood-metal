#include "EnhorabuenaState.hpp"

EnhorabuenaState::EnhorabuenaState()
{
    m_bQuit         = false;
    m_FrameEvent    = Ogre::FrameEvent();
    m_bQuitMenu     = false;
}

void EnhorabuenaState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering EnhorabuenaState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "EnhorabuenaSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    m_pCamera = m_pSceneMgr->createCamera("EnhorabuenaCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    m_bQuit = false;
    m_bQuitMenu = false;
    // CEGUI
    _ceguiSystem = CEGUI::System::getSingletonPtr();

    // Overlays
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    recoveScoreExisting();
    createScene();
    crearEnhorabuena();
}


void EnhorabuenaState::createScene()
{
    _overlayEnhorabuena = _overlayManager->getByName("siguienteNivel");
    _overlayEnhorabuena->show();
    _ceguiWindow = CEGUI::WindowManager::getSingleton().loadWindowLayout("enhorabuena.layout");
    this->crearEnhorabuena();
    _ceguiWindow->setVisible(true);
    CEGUI::System::getSingleton().setGUISheet(_ceguiWindow);
    CEGUI::MouseCursor::getSingleton().show();
}


bool EnhorabuenaState::salir(const CEGUI::EventArgs &e)
{
    m_bQuitMenu = true;
    return true;
}

bool EnhorabuenaState::continuar(const CEGUI::EventArgs &e)
{
    m_bQuit = true;
    return true;
}

void EnhorabuenaState::crearEnhorabuena()
{
    _name = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("Enhorabuena/Nombre"));
    _score = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("Enhorabuena/Puntos"));
    menuButton = CEGUI::WindowManager::getSingleton().getWindow("Enhorabuena/Menu");
    continueButton = CEGUI::WindowManager::getSingleton().getWindow("Enhorabuena/Continuar");
    menuButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&EnhorabuenaState::salir, this));
    continueButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&EnhorabuenaState::continuar, this));
    _name->setProperty("Text","[colour='FF000000']" +_namePlayer);
    _score->setProperty("Text", "[colour='FF000000']" + _scorePlayer);
    _ceguiWindow->addChildWindow(menuButton);
    _ceguiWindow->addChildWindow(continueButton);
}

void EnhorabuenaState::recoveScoreExisting()
{
    //leo un fichero de texto y muestro la puntuacion actual
    fstream fs;
    fs.open(FILE_TEMP,fstream::in);

    /* Orden de recuperacion
     *
     *  1. Nombre
     *  2. Puntuacion
     */
    fs >> _namePlayer;
    fs >> _scorePlayer;
    fs.close();

    //limpiar archivo
    fs.open(FILE_TEMP,ios::out | ios::trunc);
    fs << " ";
    fs.close();

}

void EnhorabuenaState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Leaving EnhorabuenaState...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr)
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);

    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    this->hideGUI();
}

bool EnhorabuenaState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        m_bQuit = true;

    }
    return true;
}

bool EnhorabuenaState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);

    return true;
}

bool EnhorabuenaState::mouseMoved(const OIS::MouseEvent &evt)
{
    _ceguiSystem->injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
    // Scroll wheel
    if (evt.state.Z.rel) _ceguiSystem->injectMouseWheelChange(evt.state.Z.rel / 120.0f);
    return true;
}

bool EnhorabuenaState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));

    return true;
}

bool EnhorabuenaState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
    return true;
}

bool EnhorabuenaState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool EnhorabuenaState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool EnhorabuenaState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    return true;
}

bool EnhorabuenaState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    return true;
}

void EnhorabuenaState::hideGUI()
{
    //CEGUI::MouseCursor::getSingleton().hide();
    _overlayEnhorabuena->hide();
}

void EnhorabuenaState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;

    //volver a la partida
    if(m_bQuit == true)
    {
        hideGUI();
        popAppState();
        return;
    }
    //volver a menu principal
    if(m_bQuitMenu == true){
        hideGUI();
        popAppState();
        changeAppState((findByName("MenuState")));
        return;
    }

}

CEGUI::MouseButton EnhorabuenaState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

