#ifndef LOGROSSTATE_HPP
#define LOGROSSTATE_HPP

#include "AppState.hpp"
#include "Achievement.h"

using namespace std;
using namespace Ogre;

class LogrosState: public AppState
{
public:
    LogrosState();
    DECLARE_APPSTATE_CLASS(LogrosState)

    void enter();

    void exit();
    void update(double timeSinceLastFrame);
    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
    bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
    bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
    bool povMoved(const OIS::JoyStickEvent &arg, int index);

private:
    bool m_bQuit;
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay * _overlayLogros;
    Achievement* _logr;

     CEGUI::Window* _gui;
     CEGUI::System* _ceguiSystem;
     CEGUI::DefaultWindow* textNombre;
     CEGUI::DefaultWindow* textPuntos;
     CEGUI::Window* _backButton;
     CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

     bool exit(const CEGUI::EventArgs &e);

     void createScene();
     void buildGUI();
     void mostrarLogros();
};

#endif // LOGROSSTATE_HPP
