#include "GameState.hpp"
#include "CollisionClass.h"

GameState::GameState()
{
    _score = 0;
    this->_gestionLogros = new Achievement();
    StateManager* sm=new StateManager();
    STATEMANAGER_CREATE(sm);

    OgreFramework::getSingletonPtr()->m_pSoundManager->loadAudio("Level_1.wav", &_soundPlay, false);
    OgreFramework::getSingletonPtr()->m_pSoundManager->loadAudio("Shot_1.wav", &_soundShot, false);
    OgreFramework::getSingletonPtr()->m_pSoundManager->loadAudio("Explotion_1.wav", &_soundExplotion, false);
    OgreFramework::getSingletonPtr()->m_pSoundManager->loadAudio("Crate_1.wav", &_soundCrate, false);

}

void GameState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering GameState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "GameSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));
    m_pSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    _camera = new EasyCamera("GameCamera",m_pSceneMgr);
    m_pCamera = _camera->getCamera();

    createPhysics();
    std::cout << "Game Physics created" << std::endl;
    defaultValues();
    std::cout << "Default values loaded" << std::endl;
    recoverConfig();
    recoverOptions();
    createScene();
    std::cout << "Game Scene created" << std::endl;
    loadLevel();
    std::cout << "Game Level loaded" << std::endl;
    buildUI();
    buildPanels();
    createAudio();
    std::cout << "Game GUI built" << std::endl;
}

bool GameState::pause()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Pausing GameState...");
    CEGUI::MouseCursor::getSingleton().hide();

    _timer->pausarTiempo();

    OgreFramework::getSingletonPtr()->m_pSoundManager->pauseAudio(_soundPlay);

    return true;
}

void GameState::resume()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Resuming GameState...");

    buildUI();
    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);
    if(m_bQuitPausa == true){
        _timer->continuarTiempo();
        m_bQuitPausa = false;
    }
    if(m_bQuitMuerte == true){
        m_bQuitMuerte = false;
    }
    if(m_bQuitEnhorabuena == true){
        m_bQuitEnhorabuena = false;
        nextLevel();
    }

    OgreFramework::getSingletonPtr()->m_pSoundManager->resumeAudio(_soundPlay);
}

void GameState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Leaving GameState...");
    //    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr)
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    CEGUI::MouseCursor::getSingleton().hide();

    _timer->pararTiempo();
    OgreFramework::getSingletonPtr()->m_pSoundManager->stopAllAudio();

}

void GameState::createPhysics()
{
    // Bounds and gravity
    AxisAlignedBox worldBounds = AxisAlignedBox(
                Vector3 (-1000, -1000, -1000),
                Vector3 (1000,  1000,  1000));

    Vector3 gravity = Vector3(0, -9.8, 0);

    // World
    _physicsManager = new PhysicsManager(m_pSceneMgr, worldBounds, gravity);
    _bulletWorld = _physicsManager->getWorld()->getBulletDynamicsWorld();
}


void GameState::createScene()
{
    Ogre::Plane plane;
    plane.d = 1000;
    plane.normal = Ogre::Vector3::UNIT_Z;

    m_pSceneMgr->setSkyPlane(true, plane, "Skyline", 1000, 75, true);
    m_pSceneMgr->createLight("Light")->setPosition(75,75,75);

    // Character
    _player = new Player ("Frank",
                          Ogre::Vector3(0,0,0),
                          1000,
                          _armour,
                          _ammunition,
                          _power,
                          3,
                          m_pSceneMgr,
                          _physicsManager);

    _player->changeTexture(_textureSelected);

    // Level
    _levelManager = new LevelManager(m_pSceneMgr, _physicsManager);
    _levelCurrent = 0;
    _levelList.clear();
    _levelList.push_back("media/levels/level_00.tmx");
    _levelList.push_back("media/levels/level_01.tmx");
}

void GameState::loadLevel()
{
    _level = _levelManager->loadLevel(_levelList.at(_levelCurrent));
    _player->teleport(_level->getPlayerPosition());

    //Crear Enemigos
    //Iniciar StateManager
    StateManager::getSingletonPtr()->setEnemies(_level->getEnemies());
    StateManager::getSingletonPtr()->init();
}

void GameState::rebootLevel()
{
    _score = 0;
    _player->reboot();
    OgreFramework::getSingletonPtr()->m_pSoundManager->stopAudio(_soundPlay);
    _levelManager->destroyLevel();
    loadLevel();
    _camera->instantUpdate(Vector3(_player->getNode()->getPosition().x,
                                   _player->getNode()->getPosition().y+8,
                                   25),
                           Vector3(_player->getNode()->getPosition().x,
                                   _player->getNode()->getPosition().y+4,
                                   _player->getNode()->getPosition().z));

    _timer->contar(ATRAS, 4,0);
    //reseteamos el tiempo para evitar que aparezca un tiempo erroneo
    _timer->resetearTiempo();
    _timer->pausarTiempo();

    _reboot = true;
}

void GameState::nextLevel()
{
    std::cout << "Load next Level... " << std::endl;
    OgreFramework::getSingletonPtr()->m_pSoundManager->stopAudio(_soundPlay);
    _timer->contar(ATRAS, 4,0);
    //reseteamos el tiempo para evitar que aparezca un tiempo erroneo
    _timer->resetearTiempo();
    _timer->pausarTiempo();

    _levelManager->destroyLevel();
    _levelCurrent +=1;

    if(_levelCurrent < _levelList.size()){
        loadLevel();
//        _player->reboot();
    }else{
        std::cout << "No more levels!!" << std::endl;
        m_bQuitMuerte = true;
    }
}

void GameState::defaultValues()
{
    // Default values
    m_bQuit = false;
    m_bQuitMuerte = false;
    m_bQuitEnhorabuena = false;
    m_bQuitPausa = false;
    _reboot = false;
    _addAchievement = true;
    this->_timer = new EasyTimer();
    _timer->contar(ATRAS,4,0);
    _timer->pausarTiempo();
}


bool GameState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    switch(keyEventRef.key){

    case OIS::KC_SPACE:
        _player->signal_jump();
        break;

    case OIS::KC_F:
        _player->signal_shootPrincipal();
        break;

    case OIS::KC_RETURN:
        _player->signal_shootSecondary();
        break;

    default:
        break;

    }

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        m_bQuitPausa=true;
    }

    return true;
}


bool GameState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
    return true;
}


bool GameState::mouseMoved(const OIS::MouseEvent &evt)
{
    return true;
}


bool GameState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}


bool GameState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}

bool GameState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    OgreFramework::getSingletonPtr()->buttonPressed(arg, button);

    switch(button){

    case XBOX360_BUTTON_A:
        _player->signal_jump();
        break;

    case XBOX360_BUTTON_LB:
        _player->shootSecondary();
        break;

    case XBOX360_BUTTON_RB:
        _player->shootPrincipal();
        break;

    default:
        break;
    }
    return true;
}

bool GameState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    OgreFramework::getSingletonPtr()->buttonReleased(arg, button);
    return true;
}

bool GameState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    OgreFramework::getSingletonPtr()->axisMoved(arg, axis);

    switch(axis){

    //    case XBOX360_RIGHT_TRIGGER:
    //        if(arg.state.mAxes[axis].abs > XBOX360_JOY_MAX/2){
    //            _character->shootPrincipal();
    //        }
    //        break;

    //    case XBOX360_LEFT_TRIGGER:
    //        if(arg.state.mAxes[axis].abs > XBOX360_JOY_MAX/2){
    //            _character->shootSecondary();
    //        }
    //        break;

    default:
        break;
    }
    return true;
}

bool GameState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    OgreFramework::getSingletonPtr()->povMoved(arg, index);

    //    cout << "NORTH: " << arg.state.mPOV[index].North << endl;
    //    cout << "NORTHEST: " << arg.state.mPOV[index].NorthEast << endl;
    //    cout << "EST: " << arg.state.mPOV[index].East << endl;
    //    cout << "SOUTHEST: " << arg.state.mPOV[index].SouthEast << endl;
    //    cout << "SOUTH: " << arg.state.mPOV[index].South << endl;
    //    cout << "SOUTHWEST: " << arg.state.mPOV[index].SouthWest << endl;
    //    cout << "WEST: " << arg.state.mPOV[index].West << endl;
    //    cout << "NORTHWEST: " << arg.state.mPOV[index].NorthWest << endl;

    //    cout << "OIS::povMoved at: " << index << " " << arg.state.mPOV[index].direction << endl;
    return true;
}

void GameState::increaseScore(int increment)
{
    _score += increment;
}

void GameState::decreaseScore(int decrement)
{
    _score -= decrement;

    if (_score < 0){
        _score = 0;
    }
}

void GameState::resetScore()
{
    _score = 0;
}

void GameState::createAudio()
{
    // Sound
    OgreFramework::getSingletonPtr()->m_pSoundManager->stopAllAudio();
    OgreFramework::getSingletonPtr()->m_pSoundManager->setListener(
                _camera->getCameraPosition(),
                Ogre::Vector3::ZERO,
                Ogre::Vector3::NEGATIVE_UNIT_Z,
                Ogre::Vector3::UNIT_Y);


    OgreFramework::getSingletonPtr()->m_pSoundManager->setSource(_soundPlay,
                                                               _player->getNode()->getPosition(),
                                                               Ogre::Vector3::ZERO);

    OgreFramework::getSingletonPtr()->m_pSoundManager->setSource(_soundShot,
                                                               _player->getNode()->getPosition(),
                                                               Ogre::Vector3::ZERO);

    OgreFramework::getSingletonPtr()->m_pSoundManager->setSource(_soundExplotion,
                                                               _player->getNode()->getPosition(),
                                                               Ogre::Vector3::ZERO);

    OgreFramework::getSingletonPtr()->m_pSoundManager->setSource(_soundCrate,
                                                               _player->getNode()->getPosition(),
                                                               Ogre::Vector3::ZERO);
}

void GameState::recoverConfig()
{
    /*
     * Orden:
     *        1. name
     *        2. model
     *        3. armour;
     *        4. ammunition;
     *        5. power;
     */
    Ogre::String auxAmmunition;
    Ogre::String auxArmour;
    Ogre::String auxPower;

    ifstream fe(FILE_CONFIG);
    fe >> _nameSelected;
    fe >> _textureSelected;
    fe >> auxArmour;
    _armour = atoi(auxArmour.c_str());
    fe >> auxAmmunition;
    _ammunition = atoi(auxAmmunition.c_str());
    fe >> auxPower;
    _power = atoi(auxPower.c_str());
    fe.close();
}

void GameState::recoverOptions()
{
    opt = new Options();
    opt->recoverOptions();
}




void GameState::moveCamera()
{
    _origin = Vector3(_player->getNode()->getPosition().x,
                      _player->getNode()->getPosition().y+8,
                      25);

    _destiny = Vector3(_player->getNode()->getPosition().x,
                       _player->getNode()->getPosition().y+4,
                       _player->getNode()->getPosition().z);

    _camera->update(_origin, _destiny);
}


void GameState::getInput()
{
    if(OgreFramework::getSingletonPtr()->m_pJoystick){

        if(OgreFramework::getSingletonPtr()->m_pJoystick->getJoyStickState().mAxes[XBOX360_JOY_LEFT_VERTICAL].abs > XBOX360_JOY_DEATH_ZONE){
            _player->signal_down();
        }


        if(OgreFramework::getSingletonPtr()->m_pJoystick->getJoyStickState().mAxes[XBOX360_JOY_LEFT_VERTICAL].abs < -XBOX360_JOY_DEATH_ZONE){
            _player->signal_up();
        }

        if(OgreFramework::getSingletonPtr()->m_pJoystick->getJoyStickState().mAxes[XBOX360_JOY_LEFT_HORIZONTAL].abs > XBOX360_JOY_DEATH_ZONE){
            _player->signal_right();
        }

        if(OgreFramework::getSingletonPtr()->m_pJoystick->getJoyStickState().mAxes[XBOX360_JOY_LEFT_HORIZONTAL].abs < -XBOX360_JOY_DEATH_ZONE){
            _player->signal_left();
        }
    }

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_W))
        _player->signal_up();

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_S))
        _player->signal_down();

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_A))
        _player->signal_left();

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_D))
        _player->signal_right();

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_F5)){
        rebootLevel();
    }
}

void GameState::buildPanels()
{
    // Information Panels
    _rebootBbs = m_pSceneMgr->createBillboardSet(1);
    _rebootBbs->createBillboard(Ogre::Vector3::ZERO);
    _rebootBbs->setMaterialName("reboot");
    _rebootBbs->setDefaultDimensions(10,10);
    _rebootBbs->setCommonDirection(Ogre::Vector3(0,1,0));

    _rebootNode = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
    _rebootNode->attachObject(_rebootBbs);
    _rebootNode->setVisible(false);
}

void GameState::buildUI()
{
    CEGUI::MouseCursor::getSingleton().hide();

    _layoutUI = CEGUI::WindowManager::getSingleton().loadWindowLayout("ui.layout");
    CEGUI::System::getSingleton().setGUISheet(_layoutUI);

    _timeUI = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("UI/temporizador/TextoTemporizacion"));
    std::stringstream playerName;
    playerName <<_nameSelected;
    _playerUI = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("UI/nombreJugador"));
    _playerUI->setText(playerName.str());
    _primaryWeaponUI = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("UI/vida/TextoArmaPrincipal"));
    _secondaryWeaponUI = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("UI/vida/TextoArmaSecundaria"));
    _lifeUI = static_cast<CEGUI::ProgressBar*>(_layoutUI->getChild("UI/progresoVida"));
    _scoreUI = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("UI/vida/Puntuacion"));
    _heart1 = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("UI/vida/corazon1"));
    _heart2 = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("UI/vida/corazon2"));
    _heart3 = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("UI/vida/corazon3"));

    this->getLife();
}

void GameState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
//    OgreFramework::getSingletonPtr()->m_pTrayMgr->frameRenderingQueued(m_FrameEvent);

    // Change of states
    if(m_bQuit == true)
    {
        popAppState();
        return;
    }
    if(m_bQuitPausa == true){
        pushAppState(findByName("PauseState"));
        return;
    }
    if(m_bQuitMuerte == true){
        saveScoreActual();
        if(_addAchievement == true){
            aniadirLogro(_nameSelected,_score);
        }
        else _addAchievement = false;
        pushAppState(findByName("MuerteState"));
        return;
    }
    if(m_bQuitEnhorabuena == true){
        saveScoreActual();
        aniadirLogro(_nameSelected,_score);
        _addAchievement = false;
        pushAppState(findByName("EnhorabuenaState"));
        return;
    }

    // Physics
    updatePhysics(timeSinceLastFrame);

    // UI
    updateUI();

    if(_timer->getTiempoCorriendo()){

        if(_reboot){
            showPanel(_rebootNode);
            _reboot=false;
        }

        if(_level->outOfLimits(_player->getNode()->getPosition())){
            _player->kill();
            rebootLevel();
        }

        getInput();
        moveCamera();
        _player->update(timeSinceLastFrame);

    }
    else{
        // Time Limit Finished
        _player->kill();
        rebootLevel();
    }

    if(_player->isDeath()){
        m_bQuitMuerte = true;
    }

    //UPDATE STATE MANAGER --> ENEMIGOS
    StateManager::getSingletonPtr()->UpdateState(timeSinceLastFrame);
}

void GameState::updatePhysics(double timeSinceLastFrame)
{
    // Physics
    _physicsManager->getWorld()->stepSimulation(timeSinceLastFrame);

    _nManifolds = _bulletWorld->getDispatcher()->getNumManifolds();

    for (unsigned int i=0; i<_nManifolds; i++) {

        _contactManifold = _bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);

        _obA = static_cast<btCollisionObject*>(_contactManifold->getBody0());
        _obB = static_cast<btCollisionObject*>(_contactManifold->getBody1());

        // Check if we already done that btCollisionObject
        if (std::find(_recentContact.begin(), _recentContact.end(), _obA) == _recentContact.end()){
            if (std::find(_recentContact.begin(), _recentContact.end(), _obB) == _recentContact.end()){

                CollisionClass* o1=(CollisionClass*)_obA->getUserPointer();
                CollisionClass* o2=(CollisionClass*)_obB->getUserPointer();

                if( (o1->getCollisionType() == CollisionClass::DESTRUCTOR) ||
                        (o2->getCollisionType() == CollisionClass::DESTRUCTOR)){

                    processDestructionHit(o1, o2);


                }else if ( (o1->getCollisionType() == CollisionClass::PLAYER) ||
                           (o2->getCollisionType() == CollisionClass::PLAYER)){

                    processTriggerHit(o1, o2);

                }
            }
        }
    }

    _recentContact.clear();
}

void GameState::processDestructionHit(CollisionClass *o1, CollisionClass *o2)
{
    if(o1->getCollisionType() == CollisionClass::DESTRUCTOR){

        switch(o2->getCollisionType()){

        case CollisionClass::DESTRUCTIBLE:
            increaseScore(50);
            o2->getHit();
            _recentContact.push_back(_obB);
            break;

        case CollisionClass::PLAYER:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundExplotion, true);
            decreaseScore(100);
            o2->getHit();
            _recentContact.push_back(_obB);
            break;

        case CollisionClass::ENEMY:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundExplotion, true);
            increaseScore(300);
            o2->getHit();
            _recentContact.push_back(_obB);
            break;

        default:
            break;
        }
        OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundShot, true);
        o1->getHit();
        _recentContact.push_back(_obA);

    }else{

        switch(o1->getCollisionType()){

        case CollisionClass::DESTRUCTIBLE:
            increaseScore(50);
            o1->getHit();
            _recentContact.push_back(_obA);
            break;

        case CollisionClass::PLAYER:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundExplotion, true);
            decreaseScore(100);
            o1->getHit();
            _recentContact.push_back(_obA);
            break;

        case CollisionClass::ENEMY:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundExplotion, true);
            increaseScore(300);
            o1->getHit();
            _recentContact.push_back(_obA);
            break;

        default:
            break;
        }
        OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundShot, true);
        o2->getHit();
        _recentContact.push_back(_obB);
    }
}

void GameState::processTriggerHit(CollisionClass *o1, CollisionClass *o2)
{
    if(o1->getCollisionType() == CollisionClass::PLAYER){
        CharacterTerrestrial * character = (CharacterTerrestrial*) o1;
        CheckPoint* checkPoint;

        switch(o2->getCollisionType()){

        case CollisionClass::CRATE_CANNON:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundCrate, true);
            character->reloadSecondary(5);
            increaseScore(100);
            o2->getHit();
            _recentContact.push_back(_obB);
            break;

        case CollisionClass::CRATE_LIFE:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundCrate, true);
            character->reloadLife();
            increaseScore(200);
            o2->getHit();
            _recentContact.push_back(_obB);
            break;

        case CollisionClass::CRATE_POINTS:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundCrate, true);
            increaseScore(500);
            o2->getHit();
            _recentContact.push_back(_obB);
            break;

        case CollisionClass::CRATE_SHOT:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundCrate, true);
            character->reloadPrincipal(20);
            increaseScore(100);
            o2->getHit();
            _recentContact.push_back(_obB);
            break;

        case CollisionClass::START:
            checkPoint= (CheckPoint*) o2;
            if(!checkPoint->Checked()){
                checkPoint->Check(true);
                _timer->continuarTiempo();
                OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundPlay, true);
            }
            _recentContact.push_back(_obB);
            break;

        case CollisionClass::FINISH:
            checkPoint= (CheckPoint*) o2;
            if(!checkPoint->Checked()){
                checkPoint->Check(true);
                std::cout << "Finish" << std::endl;
                m_bQuitEnhorabuena = true;
            }
            break;

        default:
            break;
        }

    }else{
        CharacterTerrestrial * character = (CharacterTerrestrial*) o2;
        CheckPoint* checkPoint;
        switch(o1->getCollisionType()){

        case CollisionClass::CRATE_CANNON:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundCrate, true);
            character->reloadPrincipal(20);
            increaseScore(100);
            o1->getHit();
            _recentContact.push_back(_obA);

            break;

        case CollisionClass::CRATE_LIFE:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundCrate, true);
            character->reloadLife();
            increaseScore(200);
            o1->getHit();
            _recentContact.push_back(_obA);

            break;

        case CollisionClass::CRATE_POINTS:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundCrate, true);
            increaseScore(500);
            o1->getHit();
            _recentContact.push_back(_obA);
            break;

        case CollisionClass::CRATE_SHOT:
            OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundCrate, true);
            character->reloadPrincipal(20);
            increaseScore(100);
            o1->getHit();
            _recentContact.push_back(_obA);
            break;

        case CollisionClass::START:
            checkPoint = (CheckPoint*) o1;
            if(!checkPoint->Checked()){
                checkPoint->Check(true);
                _timer->continuarTiempo();
                OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundPlay, true);
            }
            _recentContact.push_back(_obA);
            break;

        case CollisionClass::FINISH:
            checkPoint = (CheckPoint*) o1;
            if(!checkPoint->Checked()){
                checkPoint->Check(true);
                std::cout << "Finish" << std::endl;
                m_bQuitEnhorabuena = true;
            }
            _recentContact.push_back(_obA);
            break;

        default:
            break;
        }
    }
}

void GameState::updateUI()
{

    // Weapons
    std::stringstream primaryWeapon;
    primaryWeapon << _player->getAmmunitationPrincipal();
    _primaryWeaponUI->setText(primaryWeapon.str());

    std::stringstream secondaryWeapon;
    secondaryWeapon << _player->getAmmunitationSecondary();
    _secondaryWeaponUI->setText(secondaryWeapon.str());

    // Time
    std::stringstream time;
    time <<_timer->getCadenaTiempo();
    _timeUI->setProperty("Text","[colour='FF000000']" + time.str());

    // Life
    _lifeUI->setProgress(_player->getLifetime()/10.0);

    //Life heart
    this->getLife();

    // Score
    std::stringstream score;
    score << _score;
    _scoreUI->setText(score.str());
}

void GameState::showPanel(SceneNode *node)
{
    Ogre::Vector3 position = _player->getNode()->getPosition();
    position.x = position.x - 2;
    position.y = position.y + 4;

    node->setPosition(position);
    node->setVisible(true);
    _panelTimer = SDL_AddTimer(1000, this->hidePanel, node);
}

Uint32 GameState::hidePanel(Uint32 interval, void *param)
{
    Ogre::SceneNode* node = static_cast <Ogre::SceneNode*> (param);
    node->setVisible(false);
    return 0;
}

void GameState::aniadirLogro(string name, int score)
{
    _gestionLogros->addAchievement(name,score);
}

void GameState::getLife()
{

    switch(_player->getLifes()){
    case 1:
        _heart1->setVisible(false);
        _heart2->setVisible(false);
        _heart3->setVisible((true));
        break;
    case 2:
        _heart1->setVisible(false);
        _heart2->setVisible(true);
        _heart3->setVisible((true));
        break;
    case 3:
        _heart1->setVisible(true);
        _heart2->setVisible(true);
        _heart3->setVisible((true));
        break;
    }
}

void GameState::saveScoreActual()
{

    /*Orden de guardado
     *
     *  1. Nombre
     *  2. Puntuacion
     */

    ofstream fs;
    fs.open(FILE_TEMP);
    fs << _nameSelected << "\n";
    fs << _score;
    fs.close();
}

