#ifndef MENU_STATE_HPP
#define MENU_STATE_HPP

#include "AppState.hpp"




using namespace std;
using namespace Ogre;

class MenuState : public AppState
{
public:
    MenuState();

	DECLARE_APPSTATE_CLASS(MenuState)

	void enter();

	void exit();
    bool pause ();
    void resume();

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &evt);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
    bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
    bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
    bool povMoved(const OIS::JoyStickEvent &arg, int index);

	void buttonHit(OgreBites::Button* button);

	void update(double timeSinceLastFrame);

private:
    bool m_bQuit;

    // CEGUI
    CEGUI::System * _ceguiSystem;
    CEGUI::Window * _ceguiWindow;

    CEGUI::Window *startButton;
    CEGUI::Window *logrosButton;
    CEGUI::Window *optionsButton;
    CEGUI::Window *acercaButton;
    CEGUI::Window *tutorialButton;
    CEGUI::Window *quitButton;
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay * _overlayMenu;

    // Sound
    ALuint _soundMenu;

    void createScene();
    void crearMenu();
    void drawGUI();
    void createAudio();
    void hideGUI();
    bool empezarJuego(const CEGUI::EventArgs &e);
    bool mostrarLogros(const CEGUI::EventArgs &e);
    bool mostrarAcerca(const CEGUI::EventArgs &e);
    bool mostrarTutorial(const CEGUI::EventArgs &e);
    bool mostrarOpciones(const CEGUI::EventArgs &e);
    bool salir(const CEGUI::EventArgs &e);
    void createDefaultOptions();
};


#endif

