#include "MenuState.hpp"

MenuState::MenuState()
{
    m_bQuit         = false;
    m_FrameEvent    = Ogre::FrameEvent();
    //siempre se crea unas opciones por defecto
    createDefaultOptions();

    OgreFramework::getSingletonPtr()->m_pSoundManager->loadAudio("Electro_1.wav", &_soundMenu,true);

}


void MenuState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering MenuState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "MenuSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    m_pCamera = m_pSceneMgr->createCamera("MenuCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    _ceguiSystem = CEGUI::System::getSingletonPtr();
    _overlayManager= Ogre::OverlayManager::getSingletonPtr();

    createScene();
    drawGUI();
    createAudio();
}


void MenuState::createScene()
{
    _overlayMenu = _overlayManager->getByName("Menu");

    CEGUI::SchemeManager::getSingleton().create("BloodMetal.scheme");
    CEGUI::SchemeManager::getSingleton().create("VanillaSkin.scheme");


    _ceguiWindow = CEGUI::WindowManager::getSingleton().loadWindowLayout("menubotones.layout");
    CEGUI::System::getSingleton().setDefaultMouseCursor("Puntero", "punteroRaton");

    this->crearMenu();
    _ceguiWindow->setVisible(true);
    CEGUI::System::getSingleton().setGUISheet(_ceguiWindow);

}


void MenuState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo MenuState...");
    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    CEGUI::MouseCursor::getSingleton().hide();
    _overlayMenu->hide();
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo MenuState exito...");
}

bool MenuState::pause()
{

}

void MenuState::resume()
{

}
bool MenuState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        m_bQuit = true;

    }
    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_RETURN)){
        //  changeAppState(findByName("GameState"));
        pushAppState(findByName("GameState"));
    }

    return true;
}


bool MenuState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
    return true;
}


bool MenuState::mouseMoved(const OIS::MouseEvent &evt)
{
    // if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;
    _ceguiSystem->injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
    return true;
}


bool MenuState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    // if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseDown(evt, id)) return true;
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
    return true;
}


bool MenuState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    // if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseUp(evt, id)) return true;
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
    return true;
}

bool MenuState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool MenuState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool MenuState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    return true;
}

bool MenuState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    return true;
}

void MenuState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;

    if(m_bQuit == true)
    {
        shutdown();
        return;
    }
}

CEGUI::MouseButton MenuState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

void MenuState::crearMenu()
{
    //Boton para Comenzar
    startButton = CEGUI::WindowManager::getSingleton().getWindow("MenuBotones/Jugar");

    //Boton de logros
    logrosButton = CEGUI::WindowManager::getSingleton().getWindow("MenuBotones/Logros");

    //options button
    optionsButton = CEGUI::WindowManager::getSingleton().getWindow("MenuBotones/Opciones");

    //Boton para el tutorial
    tutorialButton = CEGUI::WindowManager::getSingleton().getWindow("MenuBotones/Tutorial");

    //Boton para Acerca
    acercaButton = CEGUI::WindowManager::getSingleton().getWindow("MenuBotones/Acerca");

    //Quit button
    quitButton = CEGUI::WindowManager::getSingleton().getWindow("MenuBotones/Salir");


    //Suscribimos cada boton a un evento que controlara que hace si pulsas en el
    startButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::empezarJuego, this));
    logrosButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::mostrarLogros, this));
    acercaButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::mostrarAcerca, this));
    tutorialButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::mostrarTutorial, this));
    quitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::salir, this));
    optionsButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MenuState::mostrarOpciones,this));

    //Añadimos los botones a la ventana
    _ceguiWindow->addChildWindow(startButton);
    _ceguiWindow->addChildWindow(logrosButton);
    _ceguiWindow->addChildWindow(acercaButton);
    _ceguiWindow->addChildWindow(tutorialButton);
    _ceguiWindow->addChildWindow(quitButton);
    _ceguiWindow->addChildWindow(optionsButton);
}

void MenuState::drawGUI()
{
    _overlayMenu->show();
    startButton->show();
    logrosButton->show();
    acercaButton->show();
    tutorialButton->show();
    optionsButton->show();
    quitButton->show();
    CEGUI::MouseCursor::getSingleton().show();


}

void MenuState::createAudio()
{
    // Sound
    OgreFramework::getSingletonPtr()->m_pSoundManager->setSource(_soundMenu,
                                                               Ogre::Vector3::ZERO,
                                                               Ogre::Vector3::ZERO);

    OgreFramework::getSingletonPtr()->m_pSoundManager->setListener(
                Ogre::Vector3(0.0f, 0.0f, 4.0f),
                Ogre::Vector3::ZERO,
                Ogre::Vector3::NEGATIVE_UNIT_Z,
                Ogre::Vector3::UNIT_Y);

    OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundMenu, false);
}

void MenuState::hideGUI()
{
    _overlayMenu->hide();
    startButton->hide();
    logrosButton->hide();
    acercaButton->hide();
    tutorialButton->hide();
    optionsButton->hide();
    quitButton->hide();

    CEGUI::MouseCursor::getSingleton().hide();

}

bool MenuState::empezarJuego(const CEGUI::EventArgs &e)
{
    changeAppState(findByName("ConfigState"));
    return true;
}

bool MenuState::mostrarLogros(const CEGUI::EventArgs &e)
{

    changeAppState(findByName("LogrosState"));
    return true;
}

bool MenuState::mostrarAcerca(const CEGUI::EventArgs &e)
{
    changeAppState((findByName("AcercaState")));
    return true;
}

bool MenuState::mostrarTutorial(const CEGUI::EventArgs &e)
{
    changeAppState((findByName("TutorialState")));
    return true;
}

bool MenuState::mostrarOpciones(const CEGUI::EventArgs &e)
{
    changeAppState((findByName("OpcionesState")));
    return true;
}

bool MenuState::salir(const CEGUI::EventArgs &e)
{
    m_bQuit=true;
    return true;
}



void MenuState::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "ExitBtn")
        m_bQuit = true;
    else if(button->getName() == "EnterBtn")
        changeAppState(findByName("GameState"));
}
void MenuState::createDefaultOptions()
{
    ofstream fs;
    fs.open("options.dat");

    /*
     * VolumeAudio 100
     * activeAudio yes
     * VolumeFx 100
     * activeFx yes
     *mode keyboard */

    fs << "100" << "\n" << "yes" << "\n" << "100" << "\n" << "yes" << "\n" << "keyboard";

    fs.close();
}
