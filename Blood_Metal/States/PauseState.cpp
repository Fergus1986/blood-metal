#include "PauseState.hpp"

using namespace Ogre;

PauseState::PauseState()
{
    m_bQuit             = false;
    m_FrameEvent        = Ogre::FrameEvent();
}


void PauseState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering PauseState...");

    m_bQuit = false;
    m_bQuitMenu = false;
    // CEGUI
    _ceguiSystem = CEGUI::System::getSingletonPtr();

    // Overlays
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    createScene();
}


void PauseState::createScene()
{
    _overlayPausa = _overlayManager->getByName("Pausa");
    std::cout << "Overlay: " <<  _overlayPausa->getName() << std::endl;
    _overlayPausa->show();

    _ceguiWindow = CEGUI::WindowManager::getSingleton().loadWindowLayout("decision.layout");
    this->crearPausa();
    _ceguiWindow->setVisible(true);
    CEGUI::System::getSingleton().setGUISheet(_ceguiWindow);
    CEGUI::MouseCursor::getSingleton().show();
}


void PauseState::crearPausa()
{

    backButton = CEGUI::WindowManager::getSingleton().getWindow("Decision/Continuar");
    menuButton = CEGUI::WindowManager::getSingleton().getWindow("Decision/Menu");

    backButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&PauseState::continuar, this));
    menuButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&PauseState::salir, this));

    _ceguiWindow->addChildWindow(backButton);
    _ceguiWindow->addChildWindow(menuButton);

}

void PauseState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Leaving PauseState...");

//    m_pSceneMgr->destroyCamera(m_pCamera);
//    if(m_pSceneMgr)
//        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);

    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    CEGUI::MouseCursor::getSingleton().hide();
    _overlayPausa->hide();
}

bool PauseState::continuar(const CEGUI::EventArgs &e)
{
    m_bQuit = true;
    return true;
}

bool PauseState::salir(const CEGUI::EventArgs &e)
{
    m_bQuitMenu = true;
    return true;
}


bool PauseState::keyPressed(const OIS::KeyEvent &keyEventRef)
{

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if(keyEventRef.key == OIS::KC_ESCAPE || keyEventRef.key== OIS::KC_P)
    {
        m_bQuit = true;
    }
    return true;
}

bool PauseState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);

    return true;
}


bool PauseState::mouseMoved(const OIS::MouseEvent &evt)
{
    _ceguiSystem->injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
    // Scroll wheel
    if (evt.state.Z.rel) _ceguiSystem->injectMouseWheelChange(evt.state.Z.rel / 120.0f);
    return true;
}

bool PauseState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));

    return true;
}

bool PauseState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));

    return true;
}

bool PauseState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool PauseState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool PauseState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    return true;
}

bool PauseState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    return true;
}

void PauseState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;

    if(m_bQuit == true)
    {
        popAppState();
        return;
    }
    //volver a menu principal
    if(m_bQuitMenu == true){
        popAppState();
        changeAppState((findByName("MenuState")));
    }
}

CEGUI::MouseButton PauseState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}
