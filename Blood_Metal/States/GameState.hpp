#ifndef GAME_STATE_HPP
#define GAME_STATE_HPP

#include "AppState.hpp"

#include "DotSceneLoader.hpp"

#include <OgreSubEntity.h>
#include <OgreMaterialManager.h>

#include "Player/Player.h"

#include "LevelManager.h"
#include "EasyOgre/EasyCamera.h"
#include "PhysicsManager.h"
#include "EasyOgre/EasyTimer.h"
#include "Achievement.h"

#include <stdlib.h>
#include <SDL/SDL.h>

#include "Level/CheckPoint.h"

#include "Enemy/EnemyStates/StateManager.h"
#include "Options.h"

#include "EasyOgre/Xbox360.h"

#define STATEMANAGER_CREATE(x) (void)x

using namespace Ogre;

class GameState : public AppState
{
public:

    GameState();

    DECLARE_APPSTATE_CLASS(GameState)

    void enter();
    void exit();
    bool pause();
    void resume();

    void createPhysics();
    void createScene();
    void loadLevel();
    void rebootLevel();
    void nextLevel();
    void defaultValues();

    void moveCamera();
    void getInput();
    void buildPanels();
    void buildUI();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &arg);
    bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

    bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
    bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
    bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
    bool povMoved(const OIS::JoyStickEvent &arg, int index);

    void increaseScore(int increment);
    void decreaseScore(int decrement);
    void resetScore();

    void createAudio();

    void recoverConfig();
    void recoverOptions();
    void update(double timeSinceLastFrame);
    void updatePhysics(double timeSinceLastFrame);
    void processDestructionHit(CollisionClass* o1,  CollisionClass* o2);
    void processTriggerHit(CollisionClass* o1,  CollisionClass* o2);
    void updateUI();

    void showPanel(Ogre::SceneNode* node);
    static Uint32 hidePanel(Uint32 interval, void *param);

private:

    // CHANGE OF STATES
    bool m_bQuit;
    bool m_bQuitMuerte;
    bool m_bQuitEnhorabuena;
    bool m_bQuitPausa;

    // TIME
    EasyTimer* _timer;

    // PHYSCIS
    PhysicsManager* _physicsManager;
    btCollisionWorld * _bulletWorld;
    unsigned int _nManifolds;
    btCollisionObject* _obA;
    btCollisionObject* _obB;
    btPersistentManifold* _contactManifold;
    std::vector<btCollisionObject*> _recentContact;

    // CAMERA
    EasyCamera* _camera;
    Ogre::Vector3 _origin;
    Ogre::Vector3 _destiny;

    // UI
    CEGUI::System* _ceguiSystem;
    CEGUI::Window* _layoutUI;

    CEGUI::DefaultWindow* _timeUI;
    CEGUI::DefaultWindow* _scoreUI;
    CEGUI::DefaultWindow* _playerUI;
    CEGUI::ProgressBar* _lifeUI;
    CEGUI::DefaultWindow* _primaryWeaponUI;
    CEGUI::DefaultWindow* _secondaryWeaponUI;
    CEGUI::DefaultWindow* _heart1;
    CEGUI::DefaultWindow* _heart2;
    CEGUI::DefaultWindow* _heart3;

    SDL_TimerID _panelTimer;
    Ogre::SceneNode* _rebootNode;
    Ogre::BillboardSet* _rebootBbs;

    Ogre::SceneNode* _nextLevelNode;
    Ogre::BillboardSet* _nextLevelBbs;

    // LEVEL
    LevelManager* _levelManager;
    Level* _level;
    std::vector<Ogre::String> _levelList;
    int _levelCurrent;
    bool _reboot;

    // PLAYER
    Player* _player;

    // LOGROS
    int _nEnemies;
    int _score;
    Achievement* _gestionLogros;
    bool _addAchievement;

    // CONFIGURACION
    string _textureSelected;
    string _nameSelected;
    int     _armour;
    int    _ammunition;
    int    _power;

    //Options
   Options* opt;

   // Sounds
   ALuint _soundPlay;
   ALuint _soundShot;
   ALuint _soundExplotion;
   ALuint _soundCrate;


    void aniadirLogro(string name, int score);
    void getLife();
    void saveScoreActual();
};


#endif
