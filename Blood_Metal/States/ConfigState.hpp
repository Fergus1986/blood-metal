#ifndef CONFIGSTATE_HPP
#define CONFIGSTATE_HPP

#include "AdvancedOgreFramework.hpp"

#include "AppState.hpp"
#include "CharacterTerrestrial.h"

#include "PhysicsManager.h"
#include "Object3D.h"

#define MAX_ATTRIBUTE 5

using namespace std;
using namespace Ogre;

typedef struct
{
    Ogre::String _model;
    int _armour;
    int _ammunition;
    int _power;
} PlayerConfig;

class ConfigState: public AppState
{
public:
    ConfigState();
    DECLARE_APPSTATE_CLASS(ConfigState)
    void enter();

    void createPhysics();
    void createScene();
    void createOptionsPlayer();

    void createGUI();
    void drawGUI();
    void createAudio();
    void hideGUI();

    void saveOptions();

    void exit();
    void update(double timeSinceLastFrame);
    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
    bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
    bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
    bool povMoved(const OIS::JoyStickEvent &arg, int index);



private:

    PhysicsManager* _physicsManager;

    bool m_bQuit;
    Ogre::OverlayManager* _overlayManager;

    //   int _level;
    string _name;

    // Platform
    SceneNode* _nodePlatform;
    Object3D* _platform;

    // Character
    CharacterTerrestrial* _characterTank;
    double _angle;

    // Player Options
    int _choicePlayer;
    std::vector<PlayerConfig> _optionsPlayer;

    //CEGUI
    CEGUI::System* _ceguiSystem;
    CEGUI::Window* _gui;
    CEGUI::Window* _backButton;
    CEGUI::Window* _continueButton;
    CEGUI::Window* _leftButton;
    CEGUI::Window* _rightButton;
    CEGUI::Window* _armourImage;
    CEGUI::Window* _ammunitionImage;
    CEGUI::Window* _powerImage;
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

    // Sounds
    ALuint _soundMenu;
    ALuint _soundLeft;
    ALuint _soundRight;

    bool atras(const CEGUI::EventArgs &e);
    bool continuar(const CEGUI::EventArgs &e);
    bool derecha(const CEGUI::EventArgs &e);
    bool izquierda(const CEGUI::EventArgs &e);
    void changeArmour();
    void changeAmmunition();
    void changePower();


};


#endif // CONFIGSTATE_HPP
