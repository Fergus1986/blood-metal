#include "IntroState.hpp"

IntroState::IntroState()
{
    m_bQuit = false;
}

void IntroState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering IntroState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "IntroSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    m_pCamera = m_pSceneMgr->createCamera("IntroCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    _overlayManager= Ogre::OverlayManager::getSingletonPtr();
    m_bQuit = false;

    createScene();
}

void IntroState::createScene()
{
    _overlayIntro = _overlayManager->getByName("Intro");
    _overlayIntro->show();
}


void IntroState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo IntroState...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    CEGUI::MouseCursor::getSingleton().hide();
    _overlayIntro->hide();
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo IntroState exito...");
}


void IntroState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;

    if(m_bQuit == true)
    {
        changeAppState((findByName("MenuState")));
    }
}

bool IntroState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_RETURN)){
        this->m_bQuit=true;
    }

    return true;
}

bool IntroState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    return true;
}

bool IntroState::mouseMoved(const OIS::MouseEvent &evt)
{
    return true;
}

bool IntroState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}

bool IntroState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}

bool IntroState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool IntroState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool IntroState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    return true;
}

bool IntroState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    return true;
}


