#include "MuerteState.hpp"

MuerteState::MuerteState()
{
    m_bQuit         = false;
    m_FrameEvent    = Ogre::FrameEvent();
    m_bQuitMenu     = false;
}

void MuerteState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering MuerteState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "MuerteSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    m_pCamera = m_pSceneMgr->createCamera("MuerteCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
        Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    m_bQuit = false;
    m_bQuitMenu = false;
    // CEGUI
    _ceguiSystem = CEGUI::System::getSingletonPtr();

    // Overlays
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    recoveScoreExisting();
    createScene();
    crearMuerte();
}

void MuerteState::createScene()
{
    _overlayMuerte = _overlayManager->getByName("Muerte");
    _overlayMuerte->show();
    _ceguiWindow = CEGUI::WindowManager::getSingleton().loadWindowLayout("muerte.layout");
    this->crearMuerte();
    _ceguiWindow->setVisible(true);
    CEGUI::System::getSingleton().setGUISheet(_ceguiWindow);
    CEGUI::MouseCursor::getSingleton().show();
}
bool MuerteState::continuar(const CEGUI::EventArgs &e)
{
     m_bQuit = true;
}

bool MuerteState::salir(const CEGUI::EventArgs &e)
{
    m_bQuitMenu = true;
    return true;
}
void MuerteState::crearMuerte()
{
    _name = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("Muerte/Nombre"));
    _score = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow("Muerte/Puntos"));
    logrosButton = CEGUI::WindowManager::getSingleton().getWindow("Muerte/Continuar");
    menuButton = CEGUI::WindowManager::getSingleton().getWindow("Muerte/Menu");

    logrosButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MuerteState::continuar, this));
    menuButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&MuerteState::salir, this));

    _ceguiWindow->addChildWindow(logrosButton);
    _ceguiWindow->addChildWindow(menuButton);
    _name->setProperty("Text",_namePlayer);
    _score->setProperty("Text",_scorePlayer);
}

void MuerteState::recoveScoreExisting()
{
    //leo un fichero de texto y muestro la puntuacion actual
    fstream fs;
    fs.open(FILE_TEMP,fstream::in);

    /* Orden de recuperacion
     *
     *  1. Nombre
     *  2. Puntuacion
     */
    fs >> _namePlayer;
    fs >> _scorePlayer;
    fs.close();

    //limpiar archivo
    fs.open(FILE_TEMP,ios::out | ios::trunc);
    fs << " ";
    fs.close();
}

void MuerteState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Leaving MuerteState...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr)
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);

    CEGUI::WindowManager::getSingleton().destroyAllWindows();
   this->hideGUI();
}

bool MuerteState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        m_bQuit = true;

    }
    return true;
}

bool MuerteState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);

    return true;
}

bool MuerteState::mouseMoved(const OIS::MouseEvent &evt)
{
    _ceguiSystem->injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
    // Scroll wheel
    if (evt.state.Z.rel) _ceguiSystem->injectMouseWheelChange(evt.state.Z.rel / 120.0f);
    return true;
}

bool MuerteState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));

    return true;
}

bool MuerteState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));

    return true;
}

bool MuerteState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool MuerteState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool MuerteState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    return true;
}

bool MuerteState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    return true;
}

void MuerteState::hideGUI()
{
    CEGUI::MouseCursor::getSingleton().hide();
    _overlayMuerte->hide();
}

void MuerteState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;

    if(m_bQuit == true)
    {
        hideGUI();
        popAppState();
        changeAppState(findByName("LogrosState"));
        return;
    }
    //volver a menu principal
    if(m_bQuitMenu == true){
        hideGUI();
        popAppState();
        changeAppState((findByName("MenuState")));
    }
}
CEGUI::MouseButton MuerteState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}
