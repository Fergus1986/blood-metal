#include "OpcionesState.hpp"

OpcionesState::OpcionesState()
{
    m_bQuit = false;
}

void OpcionesState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering OpcionesState...");
    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "OpcionesSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    m_pCamera = m_pSceneMgr->createCamera("OpcionesCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    _overlayManager= Ogre::OverlayManager::getSingletonPtr();
    _ceguiSystem = CEGUI::System::getSingletonPtr();
    m_bQuit = false;
    opt = new Options();
    createScene();
    createGUI();
    drawGUI();

}

void OpcionesState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo OpcionesState...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    //CEGUI::MouseCursor::getSingleton().hide();
    this->hideGUI();
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo OpcionesState exito...");
}

void OpcionesState::createScene()
{
    _overlayOpciones = _overlayManager->getByName("Opciones");
}


void OpcionesState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;

    if(m_bQuit == true)
    {
        changeAppState((findByName("MenuState")));
    }
}

bool OpcionesState::keyPressed(const OIS::KeyEvent &keyEventRef)
{

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE)){
        OgreFramework::getSingletonPtr()->m_pLog->logMessage("Se pulso escape en AcercaState...");
        this->m_bQuit=true;
    }

    return true;
}

bool OpcionesState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    return true;
}

bool OpcionesState::mouseMoved(const OIS::MouseEvent &evt)
{
    _ceguiSystem->injectMouseMove(evt.state.X.rel, evt.state.Y.rel);

    return true;
}

bool OpcionesState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));

    return true;
}

bool OpcionesState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));

    return true;
}

bool OpcionesState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool OpcionesState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool OpcionesState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    return true;
}

bool OpcionesState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    return true;
}

void OpcionesState::createGUI()
{
    _gui = CEGUI::WindowManager::getSingleton().loadWindowLayout("opciones.layout");

    combobox = static_cast<CEGUI::Combobox*>(CEGUI::WindowManager::getSingleton().getWindow("Opciones/Controlador"));
    combobox->setReadOnly(true);

    CEGUI::ListboxTextItem* itemCombobox = new CEGUI::ListboxTextItem("teclado", TECLADO);
    itemCombobox->setSelectionBrushImage("TaharezLook", "MultiListSelectionBrush");
    combobox->addItem(itemCombobox);
    itemCombobox->setSelected(true);
    combobox->setText(itemCombobox->getText()); // Copy the item's text into the Editbox

    itemCombobox = new CEGUI::ListboxTextItem("mando", MANDO);
    itemCombobox->setSelectionBrushImage("TaharezLook", "MultiListSelectionBrush");
    combobox->addItem(itemCombobox);
    //itemCombobox->setSelected(false); // Select this item

    spinnerAudio = static_cast<CEGUI::Spinner*>(CEGUI::WindowManager::getSingleton().getWindow("Opciones/Volumen"));
    checkButtonAudio = static_cast<CEGUI::Checkbox*>(CEGUI::WindowManager::getSingleton().getWindow("Opciones/CheckAudio"));
    spinnerFx = static_cast<CEGUI::Spinner*>(CEGUI::WindowManager::getSingleton().getWindow("Opciones/Fx"));
    checkButtonFx= static_cast<CEGUI::Checkbox*>(CEGUI::WindowManager::getSingleton().getWindow("Opciones/CheckFx"));

    _continueButton = CEGUI::WindowManager::getSingleton().getWindow("Opciones/Continuar");
    _backButton = CEGUI::WindowManager::getSingleton().getWindow("Opciones/Volver");

    _continueButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&OpcionesState::saveAndContinue, this));
    _backButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&OpcionesState::exit, this));
    _gui->addChildWindow(_continueButton);
    _gui->addChildWindow(_backButton);
    CEGUI::System::getSingleton().setGUISheet(_gui);

}

void OpcionesState::drawGUI()
{
    _overlayOpciones->show();

    _gui->show();
    CEGUI::MouseCursor::getSingleton().show();
}

void OpcionesState::hideGUI()
{
    _overlayOpciones->hide();

}

CEGUI::MouseButton OpcionesState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

bool OpcionesState::saveAndContinue(const CEGUI::EventArgs &e)
{
    saveOptions();
    changeAppState(findByName("MenuState"));
}

bool OpcionesState::exit(const CEGUI::EventArgs &e)
{
    changeAppState(findByName("MenuState"));
}

void OpcionesState::saveOptions()
{


    _volumeAudio = spinnerAudio->getCurrentValue();
    if(checkButtonAudio->isSelected()){
        _activeAudio = "no";
    }
    else{
        _activeAudio = "yes";
    }
     _volumeFx = spinnerFx->getCurrentValue();

     if(checkButtonFx->isSelected()){
         _activeFx = "no";
     }
     else{
         _activeFx = "yes";
     }
     if(combobox->getID() == TECLADO){
         _controller = "keyboard";
     }
     else{
         _controller = "joystick";
     }

     opt->saveOptions(_volumeAudio,_activeAudio,_volumeFx,_activeFx,_controller);
}

