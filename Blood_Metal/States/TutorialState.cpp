#include "TutorialState.hpp"

TutorialState::TutorialState()
{
    m_bQuit = false;
}

void TutorialState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering TutorialState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "TutorialSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    m_pCamera = m_pSceneMgr->createCamera("TutorialCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    _overlayManager= Ogre::OverlayManager::getSingletonPtr();
    _ceguiSystem = CEGUI::System::getSingletonPtr();
    m_bQuit = false;
    createScene();
    buildGui();
    CEGUI::MouseCursor::getSingleton().show();

}

void TutorialState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo TutorialState...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    //CEGUI::MouseCursor::getSingleton().hide();
    _overlayTutorial->hide();
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo TutorialState exito...");
}

void TutorialState::createScene()
{
    _overlayTutorial = _overlayManager->getByName("Tutorial");
    _overlayTutorial->show();
}

void TutorialState::buildGui()
{
    _gui = CEGUI::WindowManager::getSingleton().loadWindowLayout("tutorial.layout");
    _backButton = CEGUI::WindowManager::getSingleton().getWindow("Tutorial/Volver");
    _backButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&TutorialState::back, this));
    _gui->addChildWindow(_backButton);
    CEGUI::System::getSingleton().setGUISheet(_gui);
}


void TutorialState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;

    if(m_bQuit == true)
    {
        changeAppState((findByName("MenuState")));
    }
}

bool TutorialState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE)){
        OgreFramework::getSingletonPtr()->m_pLog->logMessage("Se pulso escape en AcercaState...");
        this->m_bQuit=true;
    }

    return true;
}

bool TutorialState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    return true;
}

bool TutorialState::mouseMoved(const OIS::MouseEvent &evt)
{
    _ceguiSystem->injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
    // Scroll wheel
    if (evt.state.Z.rel) _ceguiSystem->injectMouseWheelChange(evt.state.Z.rel / 120.0f);
    return true;
}

bool TutorialState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
    return true;
}

bool TutorialState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
    return true;
}

bool TutorialState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool TutorialState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool TutorialState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    return true;
}

bool TutorialState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    return true;
}

CEGUI::MouseButton TutorialState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

bool TutorialState::back(const CEGUI::EventArgs &e)
{
    m_bQuit = true;
    return true;
}

