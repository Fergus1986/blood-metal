#ifndef OPCIONESSTATE_HPP
#define OPCIONESSTATE_HPP

#include "AppState.hpp"
#include "Options.h"
#define TECLADO 1
#define MANDO 2

using namespace std;
using namespace Ogre;

class OpcionesState: public AppState
{
public:
    OpcionesState();
    DECLARE_APPSTATE_CLASS(OpcionesState)
    void enter();

    void exit();
    void update(double timeSinceLastFrame);
    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
    bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
    bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
    bool povMoved(const OIS::JoyStickEvent &arg, int index);
    void createGUI();
    void drawGUI();
    void hideGUI();
private:
    bool m_bQuit;
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay * _overlayOpciones;
    Options * opt;

    int _volumeAudio;
    int _volumeFx;
    Ogre::String _controller;
    Ogre::String _activeAudio;
    Ogre::String _activeFx;
    CEGUI::System* _ceguiSystem;
    CEGUI::Window* _gui;
    CEGUI::Window* _continueButton;
    CEGUI::Window* _backButton;
    CEGUI::Combobox* combobox;
    CEGUI::Spinner* spinnerAudio;
    CEGUI::Spinner* spinnerFx;
    CEGUI::Checkbox* checkButtonAudio;
    CEGUI::Checkbox* checkButtonFx;
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

    bool saveAndContinue(const CEGUI::EventArgs &e);
    bool exit(const CEGUI::EventArgs &e);

    void saveOptions();
    void createScene();

};

#endif // OPCIONESSTATE_HPP
