#ifndef MUERTESTATE_HPP
#define MUERTESTATE_HPP

#include "AppState.hpp"

using namespace std;
using namespace Ogre;

class MuerteState : public AppState
{
public:
    MuerteState();
    DECLARE_APPSTATE_CLASS(MuerteState)

    void enter();

    void exit();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
    bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
    bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
    bool povMoved(const OIS::JoyStickEvent &arg, int index);

    void hideGUI();
    void update(double timeSinceLastFrame);
private:
    void createScene();
    bool m_bQuit;
    bool m_bQuitMenu;
    bool continuar(const CEGUI::EventArgs &e);
    bool salir(const CEGUI::EventArgs &e);
    void crearMuerte();
    void recoveScoreExisting();
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
    CEGUI::System * _ceguiSystem;
    CEGUI::Window * _ceguiWindow;
    CEGUI::Window* menuButton;
    CEGUI::Window* logrosButton;
    CEGUI::Window* _name;
    CEGUI::Window* _score;
    Ogre::String _namePlayer;
    Ogre::String _scorePlayer;
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay * _overlayMuerte;

};

#endif // MUERTESTATE_HPP
