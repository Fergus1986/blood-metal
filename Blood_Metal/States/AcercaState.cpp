#include "AcercaState.hpp"

AcercaState::AcercaState()
{
    m_bQuit = false;
}

void AcercaState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering AcercaState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "AcercaSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    m_pCamera = m_pSceneMgr->createCamera("AcercaCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    _overlayManager= Ogre::OverlayManager::getSingletonPtr();
    m_bQuit = false;
    _ceguiSystem = CEGUI::System::getSingletonPtr();
    createScene();
    buildGui();
    CEGUI::MouseCursor::getSingleton().show();
}

void AcercaState::createScene()
{

    _overlayAcerca = _overlayManager->getByName("Acerca");
    _overlayAcerca->show();

}

void AcercaState::buildGui()
{
    _gui = CEGUI::WindowManager::getSingleton().loadWindowLayout("acerca.layout");
    _backButton = CEGUI::WindowManager::getSingleton().getWindow("Acerca/Volver");
    _backButton->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&AcercaState::back, this));
    _gui->addChildWindow(_backButton);
    CEGUI::System::getSingleton().setGUISheet(_gui);
}

void AcercaState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo AcercaState...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr){
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    //CEGUI::MouseCursor::getSingleton().hide();
    _overlayAcerca->hide();
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo MenuState exito...");
}

void AcercaState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;

    if(m_bQuit == true)
    {
        changeAppState((findByName("MenuState")));
    }
}

bool AcercaState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);


    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE)){
        OgreFramework::getSingletonPtr()->m_pLog->logMessage("Se pulso escape en AcercaState...");
        this->m_bQuit=true;
    }

    return true;
}

bool AcercaState::keyReleased(const OIS::KeyEvent &keyEventRef)
{

    return true;
}

bool AcercaState::mouseMoved(const OIS::MouseEvent &evt)
{
    _ceguiSystem->injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
    // Scroll wheel
    if (evt.state.Z.rel) _ceguiSystem->injectMouseWheelChange(evt.state.Z.rel / 120.0f);
    return true;
}

bool AcercaState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
    return true;
}

bool AcercaState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
    return true;
}

bool AcercaState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool AcercaState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool AcercaState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    return true;
}

bool AcercaState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    return true;
}

CEGUI::MouseButton AcercaState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

bool AcercaState::back(const CEGUI::EventArgs &e)
{
    m_bQuit = true;
    return true;
}

