#ifndef ENHORABUENASTATE_HPP
#define ENHORABUENASTATE_HPP

#include "AppState.hpp"

using namespace std;
using namespace Ogre;

class EnhorabuenaState : public AppState
{
public:
    EnhorabuenaState();
     DECLARE_APPSTATE_CLASS(EnhorabuenaState)
     void enter();
     void exit();

     bool keyPressed(const OIS::KeyEvent &keyEventRef);
     bool keyReleased(const OIS::KeyEvent &keyEventRef);

     bool mouseMoved(const OIS::MouseEvent &evt);
     bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
     bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

     bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
     bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
     bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
     bool povMoved(const OIS::JoyStickEvent &arg, int index);

     void hideGUI();
     void update(double timeSinceLastFrame);
 private:
     void createScene();
     bool m_bQuit;
     bool m_bQuitMenu;

     bool salir(const CEGUI::EventArgs &e);
     bool continuar(const CEGUI::EventArgs &e);
     void crearEnhorabuena();
     void recoveScoreExisting();
     CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
     CEGUI::System * _ceguiSystem;
     CEGUI::Window * _ceguiWindow;
     CEGUI::Window* menuButton;
     CEGUI::Window* continueButton;
     CEGUI::Window* _name;
     CEGUI::Window* _score;
     Ogre::OverlayManager* _overlayManager;
     Ogre::Overlay * _overlayEnhorabuena;

     Ogre::String _scorePlayer;
     Ogre::String _namePlayer;
};

#endif // ENHORABUENASTATE_HPP
