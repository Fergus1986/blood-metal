#include "ConfigState.hpp"

ConfigState::ConfigState()
{
    m_bQuit = false;

    OgreFramework::getSingletonPtr()->m_pSoundManager->loadAudio("Techno_1.wav", &_soundMenu, true);

}

void ConfigState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering ConfigState...");
    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "ConfigSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));
    m_pSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    m_pCamera = m_pSceneMgr->createCamera("ConfigCam");
    m_pCamera->setPosition(Vector3(0, 0, 15));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
                              Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

    _overlayManager= Ogre::OverlayManager::getSingletonPtr();
    m_bQuit = false;
    _ceguiSystem = CEGUI::System::getSingletonPtr();

    //this->_level = 1;
    this->_name = " ";

    createGUI();
    createPhysics();
    createScene();
    drawGUI();
    createAudio();
}

void ConfigState::createPhysics()
{
    // Bounds and gravity
    AxisAlignedBox worldBounds = AxisAlignedBox(
                Vector3 (-1000, -1000, -1000),
                Vector3 (1000,  1000,  1000));

    Vector3 gravity = Vector3(0, -9.8, 0);

    // World
    _physicsManager = new PhysicsManager(m_pSceneMgr, worldBounds, gravity);
}

void ConfigState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Saliendo IntroState...");

    if(m_pSceneMgr){
        m_pSceneMgr->destroyCamera(m_pCamera);
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
    }

    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    CEGUI::MouseCursor::getSingleton().hide();

    OgreFramework::getSingletonPtr()->m_pSoundManager->stopAllAudio();
}

void ConfigState::createScene()
{
    Ogre::Plane plane;
    plane.d = 1000;
    plane.normal = Ogre::Vector3::UNIT_Z;

    m_pSceneMgr->setSkyPlane(true, plane, "Skyline", 1000, 75, true);
    m_pSceneMgr->createLight("Light")->setPosition(75,75,75);

    createOptionsPlayer();

    _characterTank = new CharacterTerrestrial ("Frank",
                                               Ogre::Vector3(0,4,0),
                                               500,
                                               5,
                                               3,
                                               3,
                                               2,
                                               m_pSceneMgr,
                                               _physicsManager);

    _angle = 0;

    _nodePlatform = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
    _platform = new Object3D("platform",
                             Ogre::Vector3(0, -1, 0),
                             Ogre::Quaternion::IDENTITY,
                             1,
                             5,
                             0,
                             BOX,
                             m_pSceneMgr,
                             _physicsManager);
}

void ConfigState::createOptionsPlayer()
{
    PlayerConfig model01 = {"01", 5, 3, 3};
    PlayerConfig model02 = {"02", 3, 4, 4};
    PlayerConfig model03 = {"03", 4, 4, 3};
    PlayerConfig model04 = {"04", 5, 2, 4};
    PlayerConfig model05 = {"05", 4, 2, 3};

    _optionsPlayer.push_back(model01);
    _optionsPlayer.push_back(model02);
    _optionsPlayer.push_back(model03);
    _optionsPlayer.push_back(model04);
    _optionsPlayer.push_back(model05);

    _choicePlayer = 0;
}

bool ConfigState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE)){
        OgreFramework::getSingletonPtr()->m_pLog->logMessage("Se pulso escape en AcercaState...");
        this->m_bQuit=true;
    }

    CEGUI::System::getSingleton().injectKeyDown(keyEventRef.key);
    CEGUI::System::getSingleton().injectChar(keyEventRef.text);
    return true;
}

bool ConfigState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    return true;
}

bool ConfigState::mouseMoved(const OIS::MouseEvent &evt)
{
    _ceguiSystem->injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
    return true;
}

bool ConfigState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
    return true;
}

bool ConfigState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
    return true;
}

bool ConfigState::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool ConfigState::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool ConfigState::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    return true;
}

bool ConfigState::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    return true;
}

CEGUI::MouseButton ConfigState::convertMouseButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}


void ConfigState::createGUI()
{
    _gui = CEGUI::WindowManager::getSingleton().loadWindowLayout("config.layout");
    _backButton = CEGUI::WindowManager::getSingleton().getWindow("Config/atras");
    _backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::atras, this));
    _continueButton = CEGUI::WindowManager::getSingleton().getWindow("Config/continuar");
    _continueButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::continuar, this));
    _rightButton = CEGUI::WindowManager::getSingleton().getWindow("Config/derecha");
    _rightButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::derecha, this));
    _leftButton = CEGUI::WindowManager::getSingleton().getWindow("Config/izquierda");
    _leftButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::izquierda, this));
    CEGUI::System::getSingleton().setGUISheet(_gui);


}

void ConfigState::drawGUI()
{
    _gui->show();
    _backButton->show();
    _continueButton->show();
    _rightButton->show();
    _leftButton->show();
    //mouse
    CEGUI::MouseCursor::getSingleton().show();

    this->changeAmmunition();
    this->changeArmour();
    this->changePower();
}

void ConfigState::createAudio()
{
    // Sound
    OgreFramework::getSingletonPtr()->m_pSoundManager->stopAllAudio();
    OgreFramework::getSingletonPtr()->m_pSoundManager->setListener(
                Ogre::Vector3(0.0f, 0.0f, 4.0f),
                Ogre::Vector3::ZERO,
                Ogre::Vector3::NEGATIVE_UNIT_Z,
                Ogre::Vector3::UNIT_Y);


    OgreFramework::getSingletonPtr()->m_pSoundManager->setSource(_soundMenu,
                                                               Ogre::Vector3::ZERO,
                                                               Ogre::Vector3::ZERO);

    OgreFramework::getSingletonPtr()->m_pSoundManager->playAudio(_soundMenu, false);
}

void ConfigState::hideGUI()
{
    _gui->hide();
    _backButton->hide();
    _continueButton->hide();
    _rightButton->hide();
    _leftButton->hide();
}

void ConfigState::saveOptions()
{
    std::stringstream aux;
    //nombre
    CEGUI::Editbox *editbox = static_cast<CEGUI::Editbox*>(CEGUI::WindowManager::getSingleton().getWindow("Config/NickBox"));
    CEGUI::String nick = editbox->getText();
    /* Si el usuario no introduce un nombre
     * se le asigna el nombre de player*/
    if(nick.compare("")==0)
        nick = "Player";

    aux << nick.c_str();
    _name=aux.str();

    //salvar en fichero de texto
    /*
    * Orden:
    *        1. name
    *        2. model
    *        3. armour;
    *        4. ammunition;
    *        5. power;
    */
    ofstream fs(FILE_CONFIG);
    fs << _name << "\n";
    fs << (_optionsPlayer.at(_choicePlayer))._model << "\n";
    fs << (_optionsPlayer.at(_choicePlayer))._armour << "\n";
    fs << (_optionsPlayer.at(_choicePlayer))._ammunition << "\n";
    fs << (_optionsPlayer.at(_choicePlayer))._power << "\n";
    fs.close();
}


bool ConfigState::atras(const CEGUI::EventArgs &e)
{
    //Volver a menu
    changeAppState((findByName("MenuState")));

    return true;
}

bool ConfigState::continuar(const CEGUI::EventArgs &e)
{
    //Salvar la configuracion
    saveOptions();
    //Comenzar juego
    changeAppState((findByName("GameState")));
    return true;
}

bool ConfigState::derecha(const CEGUI::EventArgs &e)
{
    //cambio seleccion personaje

    _choicePlayer++;
    _choicePlayer = _choicePlayer % _optionsPlayer.size();
    _choicePlayer = abs(_choicePlayer);

    _characterTank->changeTexture((_optionsPlayer.at(_choicePlayer))._model);
    this->changeArmour();
    this->changePower();
    this->changeAmmunition();
    return true;
}

bool ConfigState::izquierda(const CEGUI::EventArgs &e)
{
    //cambio seleccion personaje

    _choicePlayer--;
    _choicePlayer = _choicePlayer % _optionsPlayer.size();
    _choicePlayer = abs(_choicePlayer);

    _characterTank->changeTexture((_optionsPlayer.at(_choicePlayer))._model);
    this->changeArmour();
    this->changePower();
    this->changeAmmunition();

    return true;
}

void ConfigState::changeArmour()
{
    int i = 1;
    //mostrar
    for(i;i<=_optionsPlayer.at(_choicePlayer)._armour; i++){
        StringStream aux;
        aux << "Config/Armadura" << i;
        _armourImage = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow(aux.str()));
        _armourImage->setVisible(true);
    }
    //ocultar
    for(i;i<=MAX_ATTRIBUTE;i++){
        StringStream aux;
        aux << "Config/Armadura" << i;
        _armourImage = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow(aux.str()));
        _armourImage->setVisible(false);
    }

}

void ConfigState::changeAmmunition()
{
    int i = 1;
    //mostrar
    for(i;i<=_optionsPlayer.at(_choicePlayer)._ammunition; i++){
        StringStream aux;
        aux << "Config/Municion" << i;
        _ammunitionImage = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow(aux.str()));
        _ammunitionImage->setVisible(true);
    }
    //ocultar
    for(i;i<=MAX_ATTRIBUTE;i++){
        StringStream aux;
        aux << "Config/Municion" << i;
        _ammunitionImage = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow(aux.str()));
        _ammunitionImage->setVisible(false);
    }
}

void ConfigState::changePower()
{
    int i = 1;
    //mostrar
    for(i;i<=_optionsPlayer.at(_choicePlayer)._power; i++){
        StringStream aux;
        aux << "Config/Potencia" << i;
        _powerImage = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow(aux.str()));
        _powerImage->setVisible(true);
    }
    //ocultar
    for(i;i<=MAX_ATTRIBUTE;i++){
        StringStream aux;
        aux << "Config/Potencia" << i;
        _powerImage = static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow(aux.str()));
        _powerImage->setVisible(false);
    }
}

void ConfigState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    //    OgreFramework::getSingletonPtr()->m_pTrayMgr->frameRenderingQueued(m_FrameEvent);

    _physicsManager->getWorld()->stepSimulation(timeSinceLastFrame);

    _angle = _angle + (timeSinceLastFrame * 20);
    _characterTank->rotate(btQuaternion(btVector3(0,1,0), btRadians(_angle)));

    if(m_bQuit == true)
    {
        changeAppState((findByName("MenuState")));
    }
}
