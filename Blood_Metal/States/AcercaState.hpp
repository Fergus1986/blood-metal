#ifndef ACERCASTATE_HPP
#define ACERCASTATE_HPP

#include "AppState.hpp"
#include "EasyCamera.h"


using namespace std;
using namespace Ogre;

class AcercaState: public AppState
{
public:
    AcercaState();

    DECLARE_APPSTATE_CLASS(AcercaState)

    void enter();

    void exit();
    void update(double timeSinceLastFrame);
    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
    bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
    bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
    bool povMoved(const OIS::JoyStickEvent &arg, int index);


private:
    bool m_bQuit;
    Ogre::OverlayManager* _overlayManager;
    Ogre::Overlay * _overlayAcerca;

    CEGUI::Window* _gui;
    CEGUI::System* _ceguiSystem;
    CEGUI::Window* _backButton;
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
    bool back(const CEGUI::EventArgs &e);

    void createScene();
    void buildGui();

};

#endif // ACERCASTATE_HPP
