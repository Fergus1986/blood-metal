#ifndef PAUSE_STATE_HPP
#define PAUSE_STATE_HPP

#include "AppState.hpp"

class PauseState : public AppState
{
public:
    PauseState();

    DECLARE_APPSTATE_CLASS(PauseState)

    void enter();
    void createScene();
    void exit();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
    bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
    bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
    bool povMoved(const OIS::JoyStickEvent &arg, int index);

    void update(double timeSinceLastFrame);

private:

    bool                        m_bQuit;
    bool                        m_bQuitMenu;
    Ogre::OverlayManager* _overlayManager;
    CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
    CEGUI::System * _ceguiSystem;

    bool continuar(const CEGUI::EventArgs &e);
    bool salir(const CEGUI::EventArgs &e);
    void crearPausa();
    CEGUI::Window* pauseButton;
    CEGUI::Window* menuButton;
    CEGUI::Window* backButton;
    CEGUI::Window * _ceguiWindow;
    Ogre::Overlay * _overlayPausa;
};

#endif

