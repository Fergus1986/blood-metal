#ifndef OPTIONS_H
#define OPTIONS_H

#include <Ogre.h>
#include <iostream>
#include <fstream>

#define FILE_OPTIONS "options.dat"

class Options
{
public:
    Options();
   void createDefaultOptions();
   void recoverOptions();
   void saveOptions(int volumeAudio, Ogre::String activeAudio, int volumeFx, Ogre::String activeFx, Ogre::String controller);

    bool getAudioActive();
    bool getFxActive();
    int getVolumeAudio();
    int getVolumeFx();
    Ogre::String getController();
    bool getJoystickActive();
private:
    int _volumeAudio;
    int _volumeFx;
    Ogre::String _activeAudio;
    Ogre::String _activeFx;
    Ogre::String _controller;

};

#endif // OPTIONS_H
