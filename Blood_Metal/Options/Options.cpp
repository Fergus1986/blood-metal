#include "Options.h"

Options::Options()
{
    //inicializar por defecto para evitar problemas
    this->_volumeAudio = 100;
    this->_activeAudio = "yes";
    this->_volumeFx = 100;
    this->_activeFx = "yes";
    this->_controller = "keyboard";
}

void Options::createDefaultOptions()
{
    std::ofstream fs;
    fs.open(FILE_OPTIONS);

    /*
     * VolumeAudio 100
     * activeAudio yes
     * VolumeFx 100
     * activeFx yes
     *mode keyboard */

    fs << "100" << "\n" << "yes" << "\n" << "100" << "\n" << "yes" << "\n" << "keyboard";

    fs.close();
}
//recuperar de  o guardar en fichero
/*
    * Orden:
    *        1. VolumeSound
    *        2. SoundActive -> "yes" o "no"
    *        3. FxSound
    *        4.  FxActive   -> "yes" o "no"
    *        5. Controlador -> "keyboard" o "joystick"
    */
void Options::recoverOptions()
{

    std::ifstream fe(FILE_OPTIONS);
    Ogre::String auxAudio,auxFx;
    fe >> auxAudio;
    this->_volumeAudio = atoi(auxAudio.c_str());
    fe >> this->_activeAudio;
    fe >> auxFx;
    this->_volumeFx = atoi(auxFx.c_str());
    fe >> this->_activeFx;
    fe >> this->_controller;
    std::cout << "volume audio: " << _volumeAudio << std::endl;
    std::cout << "audio activo: " << _activeAudio << std::endl;
     std::cout << "volume fx: " << _volumeFx << std::endl;
     std::cout << "fx activo: " << _activeFx << std::endl;
     std::cout << "Controller: " << _controller << std::endl;
    fe.close();

}

void Options::saveOptions(int volumeAudio, Ogre::String activeAudio, int volumeFx, Ogre::String activeFx, Ogre::String controller)
{
    this->_volumeAudio = volumeAudio;
    this->_activeAudio = activeAudio;
    this->_volumeFx = volumeFx;
    this->_activeFx = activeFx;
    this->_controller = controller;

    std::fstream fs;
    fs.open(FILE_OPTIONS, std::ios::trunc | std::ios::out);
    fs << _volumeAudio << "\n";
    fs << _activeAudio << "\n";
    fs << _volumeFx << "\n";
    fs << _activeFx << "\n";
    fs << _controller;

    fs.close();
}

bool Options::getAudioActive()
{
    if(_activeAudio.compare("yes")==0){
        return true;
    }
    else{
        return false;
    }
}

bool Options::getFxActive()
{
    if(_activeFx.compare("yes")==0){
        return true;
    }
    else{
        return false;
    }
}

int Options::getVolumeAudio()
{
    return _volumeAudio;
}

int Options::getVolumeFx()
{
    return _volumeFx;
}

Ogre::String Options::getController()
{
    return _controller;
}

bool Options::getJoystickActive()
{
    if(_controller.compare("keyboard")==0){
        return false;
    }
    else{
        return true;
    }
}
