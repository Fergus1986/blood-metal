#include "Character.h"

Character::Character()
{
}

Character::~Character()
{
}

Ogre::SceneNode *Character::getNode()
{
    return this->_node;
}

Character::Character(const Ogre::String name,
                     const Ogre::Vector3 position,
                     const Ogre::Real mass,
                     const int armour,
                     const int ammunition,
                     const int power,
                     const int lifes,
                     Ogre::SceneManager *sceneManager,
                     PhysicsManager *physicsManager)
{
    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    // Charater properties
    _name = name;
    _origin = position;
    _mass = mass;
    _ammunition = ammunition;
    _armour = armour;
    _power = power;
    _nLifes = lifes;
    _maxLife = LIFE_C * _armour;
    _lifetime = _maxLife;
    _death = false;

    _node = _sceneManager->getRootSceneNode()->createChildSceneNode();

    setCollisionType(CHARACTER);

    _transform.setIdentity();

    _left = false;
    _right = false;
    _up = false;
    _down = false;
    _jump = false;
    _falling = false;
    _shootPrincipal = false;
    _shootSecondary = false;
}

void Character::signal_left()
{
    _left = true;
}

void Character::signal_right()
{
    _right = true;
}

void Character::signal_up()
{
    _up = true;
}

void Character::signal_down()
{
    _down = true;
}

void Character::signal_jump()
{
    _jump = true;
}

void Character::signal_shootPrincipal()
{
    _shootPrincipal=true;
}

void Character::signal_shootSecondary()
{
    _shootSecondary=true;
}

void Character::getHit()
{

}

void Character::setCollisionType(int type)
{
    this->CollisionType = type;
}


int Character::getCollisionType(){

    return this->CollisionType;
}
