#ifndef CHARACTERTERRESTRIAL_H
#define CHARACTERTERRESTRIAL_H

#include <OGRE/OgreString.h>
#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreBillboard.h>

#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsSphereShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsConvexHullShape.h>

#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCompoundShape.h>
#include <OgreBullet/Dynamics/Constraints/OgreBulletDynamicsRaycastVehicle.h>

#include <SDL/SDL_timer.h>

#include "PhysicsManager.h"
#include "Character.h"

class CharacterTerrestrial : public Character
{
public:
    CharacterTerrestrial();
    ~CharacterTerrestrial();
    CharacterTerrestrial(const Ogre::String name,
                         const Ogre::Vector3 position,
                         const Ogre::Real mass,
                         const int armour,
                         const int ammunition,
                         const int power,
                         const int lifes,
                         Ogre::SceneManager *sceneManager,
                         PhysicsManager *physicsManager);


    void initialState();

    // Character Interface
    void moveLeft(const float timeSinceLastFrame);
    void moveRight(const float timeSinceLastFrame);
    void jump(const float timeSinceLastFrame);
    void teleport(const Ogre::Vector3 position);
    void shootPrincipal();
    void shootSecondary();
    int getAmmunitationPrincipal();
    int getAmmunitationSecondary();
    void reloadPrincipal(int ammunition);
    void reloadSecondary(int ammunition);
    void reloadLife();
    void update(double timeSinceLastFrame);
    void changeTexture(const Ogre::String &material);
    void kill();

    int getLifetime();
    int getLifes();

    // CollisionClass Interface
    void getHit();

    OgreBulletDynamics::RaycastVehicle* getVehicle();
    OgreBulletDynamics::WheeledRigidBody* getVehicleBody();

    // Drive
    void calculateSteering();
    void rotate(const btQuaternion &rotation);
    void accelerate();
    void back();
    void brake();
    void stop();

    // Explotion
    void blastOn();
    void blastDeath();
    static Uint32 blastOff(Uint32 interval, void *param);
    static Uint32 blastDeathOff(Uint32 interval, void *param);


private:
    void createVehicle();
    void addWheel();
    void createWeapons();
    void createExplotions();
    void fall();


protected:

    // Character
    int _state;
    btVector3 _old;

    // Vehicle characteristics
    int _acceleration;  // makes the car faster
    int _maxEngineForce; // max power
    int _nitroForce;
    Ogre::Real _steeringIncrement;  // makes the wheels turn a little slower to avoid sudden 'flipping'
    Ogre::Real _steeringClamp; // the max wheel turning angle

    Ogre::Real _wheelRadius; // size of the wheel (height)
    Ogre::Real _wheelWidth; // wheel width, we wanted this wider for more 'race car' handling
    Ogre::Real _wheelFriction; // how much friction the wheel has

    Ogre::Real _rollInfluence; // how likely the car is to roll

    Ogre::Real _suspensionStiffness; // The stiffness constant for the suspension.  10.0 - Offroad buggy, 50.0 - Sports car, 200.0 - F1 Car
    Ogre::Real _suspensionCompression; // how much the suspension will compress when it lands
    Ogre::Real _suspensionDamping; // how much damping occurs when hitting bumps and jumps
    Ogre::Real _suspensionRestLenght; // The maximum length of the suspension (metres)
    Ogre::Real _maxSuspensionTravelCm; // The maximum distance the suspension can be compressed (centimetres)
    Ogre::Real _frictionSlip; // how much friction before it slips

    // Chassis
    Ogre::Entity* _chassisEntity;
    Ogre::Entity* _turretEntity;
    Ogre::SceneNode* _chassisNode;
    OgreBulletCollisions::CollisionShape* _chassisShape;

    // Vehicle
    Ogre::SceneNode* _vehicleNode;
    OgreBulletCollisions::CompoundCollisionShape* _vehicleShape;
    OgreBulletDynamics::WheeledRigidBody* _vehicleBody;
    OgreBulletDynamics::VehicleTuning* _vehicleTuning;
    OgreBulletDynamics::VehicleRayCaster* _vehicleRayCaster;
    OgreBulletDynamics::RaycastVehicle* _vehicle;

    // Wheels
    unsigned int _wheels;
    Ogre::SceneNode* _wheelsNodeParent;
    std::vector<Ogre::Entity*> _wheelEntities;
    std::vector<Ogre::SceneNode*> _wheelNodes;

    //Driving
    bool _steeringLeft;
    bool _steeringRight;
    Ogre::Real _steering;
    Ogre::Real _engineForce;

    // Explotion effect
    SDL_TimerID timerExplotionHit;
    SDL_TimerID timerExplotionDeath;
    Ogre::Light* _lightExplotionHit;
    Ogre::Light* _lightExplotionDeath;
    Ogre::SceneNode* _explotionHitNode;
    Ogre::SceneNode* _explotionHitDeath;
    Ogre::BillboardSet* _bbsHit;
    Ogre::BillboardSet* _bbsDeath;



};
#endif // CHARACTERTERRESTRIAL_H
