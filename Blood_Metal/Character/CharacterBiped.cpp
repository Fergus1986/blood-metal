#include "CharacterBiped.h"

CharacterBiped::CharacterBiped()
{
}

CharacterBiped::~CharacterBiped()
{
}


CharacterBiped::CharacterBiped(const Ogre::String name,
                               const Ogre::Vector3 position,
                               const Ogre::Real mass,
                               const int armour,
                               const int ammunition,
                               const int power,
                               const int lifes,
                               Ogre::SceneManager *sceneManager,
                               PhysicsManager *physicsManager):
    Character(name, position, mass, armour, ammunition, power, lifes, sceneManager, physicsManager)
{
    _object3D = new Object3D(_name,
                             _origin,
                             Ogre::Quaternion::IDENTITY,
                             0,
                             5,
                             mass,
                             CAPSULE,
                             _sceneManager,
                             _physicsManager);


    // 2D Movement
    _object3D->getRigidBody()->getBulletRigidBody()->setActivationState(DISABLE_DEACTIVATION);
    _object3D->getRigidBody()->getBulletRigidBody()->setLinearFactor(btVector3(1,1,0));
    _object3D->getRigidBody()->getBulletRigidBody()->setAngularFactor(btVector3(0,0,0));

    _characterMove = STAND;

}

void CharacterBiped::moveLeft(const float timeSinceLastFrame)
{
    rotateLeft();
    _velocity = _object3D->getRigidBody()->getBulletRigidBody()->getLinearVelocity();

    if(_characterMove==JUMP || _characterMove==FALL){
        if (_velocity.x() > (-MOVEMENT_JUMP)){
            _velocity.setX(-MOVEMENT_JUMP/2);
        }

    }else{
        if (_velocity.x() > (-MOVEMENT_NORMAL)){
            _velocity.setX(-MOVEMENT_NORMAL/2);
        }

        _characterMove = MOVE_LEFT;
    }

    _object3D->getRigidBody()->getBulletRigidBody()->setLinearVelocity(_velocity);
}

void CharacterBiped::moveRight(const float timeSinceLastFrame)
{
    rotateRight();
    _velocity = _object3D->getRigidBody()->getBulletRigidBody()->getLinearVelocity();

    if(_characterMove==JUMP || _characterMove==FALL){
        if (_velocity.x() < (MOVEMENT_JUMP)){
            _velocity.setX(MOVEMENT_JUMP/2);
        }

    }else{
        if (_velocity.x() < (MOVEMENT_NORMAL)){
            _velocity.setX(MOVEMENT_NORMAL/2);
        }
        _characterMove = MOVE_RIGHT;
    }
    _object3D->getRigidBody()->getBulletRigidBody()->setLinearVelocity(_velocity);
}

void CharacterBiped::jump(const float timeSinceLastFrame)
{
    _velocity = _object3D->getRigidBody()->getBulletRigidBody()->getLinearVelocity();

    if(_characterMove!=JUMP){

        if (_velocity.y() < JUMP_NORMAL){
            _velocity.setY(JUMP_NORMAL);

        }

        _object3D->getRigidBody()->getBulletRigidBody()->setLinearVelocity(_velocity);
        _characterMove = JUMP;
    }
}

void CharacterBiped::teleport(const Ogre::Vector3 position)
{
}

void CharacterBiped::shootPrincipal()
{
}

void CharacterBiped::shootSecondary()
{
}

int CharacterBiped::getAmmunitationPrincipal()
{
}

int CharacterBiped::getAmmunitationSecondary()
{
}

void CharacterBiped::reloadPrincipal(int ammunition)
{
}

void CharacterBiped::reloadSecondary(int ammunition)
{
}

void CharacterBiped::reloadLife()
{
}

void CharacterBiped::update(double timeSinceLastFrame)
{
    if (!fall()){

        if(_jump)jump(timeSinceLastFrame);
        else if (_left) moveLeft(timeSinceLastFrame);
        else if (_right) moveRight(timeSinceLastFrame);

    }

    if(_shootPrincipal){
        this->shootPrincipal();
    }

    if(_shootSecondary){
        this->shootSecondary();
    }


    _left = false;
    _right = false;
    _up = false;
    _down = false;
    _jump = false;
    _shootPrincipal = false;
    _shootSecondary = false;
}

void CharacterBiped::kill()
{
}

void CharacterBiped::changeTexture(const Ogre::String &material)
{
}

void CharacterBiped::getHit()
{
}

void CharacterBiped::setCollisionType(int type)
{
}

int CharacterBiped::getCollisionType()
{
}

void CharacterBiped::rotateLeft()
{
    _transform = _object3D->getRigidBody()->getBulletRigidBody()->getWorldTransform();
    _quaternion = btQuaternion(btVector3(0,1,0), -1.5708);

    _transform.setRotation(_quaternion);
    _object3D->getRigidBody()->getBulletRigidBody()->setWorldTransform(_transform);
}

void CharacterBiped::rotateRight()
{
    _transform = _object3D->getRigidBody()->getBulletRigidBody()->getWorldTransform();
    _quaternion = btQuaternion(btVector3(0,1,0), 1.5708);

    _transform.setRotation(_quaternion);
    _object3D->getRigidBody()->getBulletRigidBody()->setWorldTransform(_transform);
}


bool CharacterBiped::fall()
{
    _velocity = _object3D->getRigidBody()->getBulletRigidBody()->getLinearVelocity();

    if (_velocity.y() < -0.5){

        if(_velocity.y() < MOVEMENT_GRAVITY){
            _velocity.setY(MOVEMENT_GRAVITY);
        }

        _characterMove = FALL;
        return true;
    }else{
        _characterMove = STAND;
        return false;
    }
}
