#ifndef CHARACTERBIPED_H
#define CHARACTERBIPED_H

#include "Character.h"


class CharacterBiped: public Character
{
public:
    CharacterBiped();
    ~CharacterBiped();
    CharacterBiped(const Ogre::String name,
                   const Ogre::Vector3 position,
                   const Ogre::Real mass,
                   const int armour,
                   const int ammunition,
                   const int power,
                   const int lifes,
                   Ogre::SceneManager *sceneManager,
                   PhysicsManager *physicsManager);

    // Character Interface
    void moveLeft(const float timeSinceLastFrame);
    void moveRight(const float timeSinceLastFrame);
    void jump(const float timeSinceLastFrame);
    void teleport(const Ogre::Vector3 position);
    void shootPrincipal();
    void shootSecondary();
    int getAmmunitationPrincipal();
    int getAmmunitationSecondary();
    void reloadPrincipal(int ammunition);
    void reloadSecondary(int ammunition);
    void reloadLife();
    void update(double timeSinceLastFrame);
    void kill();
    void changeTexture(const Ogre::String &material);


    // CollisionClass Interface
    void getHit();
    void setCollisionType(int type);
    int getCollisionType();

protected:
    void rotateLeft();
    void rotateRight();
    bool fall();

private:
    Object3D* _object3D;
    int _characterMove;

};

#endif // CHARACTERBIPED_H
