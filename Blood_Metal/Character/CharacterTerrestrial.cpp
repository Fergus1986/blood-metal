#include "CharacterTerrestrial.h"

CharacterTerrestrial::CharacterTerrestrial()
{
}

CharacterTerrestrial::~CharacterTerrestrial()
{
    _sceneManager->getRootSceneNode()->removeAndDestroyChild(_node->getName());
    _sceneManager->getRootSceneNode()->removeAndDestroyChild(_wheelsNodeParent->getName());
    _physicsManager->getWorld()->getBulletDynamicsWorld()->removeRigidBody(_vehicleBody->getBulletRigidBody());

    std::cout << "CharacterTerrestial eliminado " << std::endl;
}

CharacterTerrestrial::CharacterTerrestrial(const Ogre::String name,
                                           const Ogre::Vector3 position,
                                           const Ogre::Real mass,
                                           const int armour,
                                           const int ammunition,
                                           const int power,
                                           const int lifes,
                                           Ogre::SceneManager *sceneManager,
                                           PhysicsManager *physicsManager):
    Character(name, position, mass, armour, ammunition, power, lifes, sceneManager, physicsManager)
{

    std::cout << "Contructor Vehicle" << std::endl;

    _wheels = 4;
    _suspensionStiffness = 10.f;
    _suspensionCompression = 4.4f;
    _suspensionDamping = 2.3f;
    _maxSuspensionTravelCm = 250.f;
    _frictionSlip = 10.5f;

    _engineForce = 0;
    _acceleration = _power * 300;
    _maxEngineForce = _acceleration * 3;
    _nitroForce = _power * 150;
    _steeringIncrement = 0.02f;
    _steeringClamp = 0.7f;

    _wheelRadius = 0.6f;
    _wheelWidth = 0.5f;
    _wheelFriction = 1e30f;

    _rollInfluence = 0.1f;

    _suspensionRestLenght = 0.8f;

    createVehicle();
    initialState();

    // Character
    _vehicleBody->getBulletRigidBody()->setActivationState(DISABLE_DEACTIVATION);
    _vehicleBody->getBulletRigidBody()->setLinearFactor(btVector3(1,1,0));
    _vehicleBody->getBulletRigidBody()->setAngularFactor(btVector3(0,0,1));
    _state = Character::STAND;

    // Collisions Manager
    this->setCollisionType(CollisionClass::CHARACTER);
    _vehicleBody->getBulletRigidBody()->setUserPointer(this);
    _vehicleBody->getBulletRigidBody()->setCollisionFlags(_vehicleBody->getBulletRigidBody()->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);

    createWeapons();
    createExplotions();

    std::cout << "Vehicle created" << std::endl;
}

void CharacterTerrestrial::createVehicle()
{
    const Ogre::Vector3 chassisShift(0, 1.1, 0);

    // Chassis & Vehicle Nodes
    _vehicleNode = _node;
    _chassisNode = _vehicleNode->createChildSceneNode();
    _chassisEntity = _sceneManager->createEntity(_name + "_body.mesh");
    _turretEntity = _sceneManager->createEntity(_name + "_turret.mesh");
    _chassisNode->attachObject(_chassisEntity);
    _chassisNode->attachObject(_turretEntity);
    _chassisNode->setPosition(chassisShift);

    //    std::cout << "Vehicle GraphicalBody created" << std::endl;

    // Chassis & Vehicle Shapes
    Ogre::Vector3 size = _chassisEntity->getBoundingBox().getSize();
    size /= 2;   // El tamano en Bullet se indica desde el centro
    size *= 0.9f;   // Bullet margin is a bit bigger so we need a smaller size

    size.y = size.y + _turretEntity->getBoundingBox().getSize().y;
    size.x = size.x + 0.1; // Make a bigger target

    _chassisShape = new OgreBulletCollisions::BoxCollisionShape(size);
    _chassisNode->setPosition(chassisShift);
    _vehicleShape = new OgreBulletCollisions::CompoundCollisionShape();
    _vehicleShape->addChildShape(_chassisShape, chassisShift);

    // Vehicle Body
    _vehicleBody = new WheeledRigidBody(_vehicleNode->getName(), _physicsManager->getWorld());
    _vehicleBody->setShape(_vehicleNode, _vehicleShape, 0.7, 0.4, _mass,_origin);
    _vehicleBody->setDamping(0.2,0.2);

    //    std::cout << "Vehicle PhysicalBody created" << std::endl;


    // Vehicle Tuning
    _vehicleTuning =  new VehicleTuning (
                _suspensionStiffness,
                _suspensionCompression,
                _suspensionDamping,
                _maxSuspensionTravelCm,
                _frictionSlip);

    // Vehicle
    _vehicleRayCaster = new VehicleRayCaster(_physicsManager->getWorld());
    _vehicle = new RaycastVehicle(_vehicleBody, _vehicleTuning, _vehicleRayCaster);
    _vehicle->setCoordinateSystem(0,1,2);

    std::cout << "Creating wheels..." << std::endl;

    // Wheels
    Ogre::Vector3 wheelDirectionCS0(0,-1,0);
    Ogre::Vector3 wheelAxleCS(-1,0,0);

    _wheelEntities.reserve(_wheels);
    _wheelNodes.reserve(_wheels);

    _wheelsNodeParent = _sceneManager->getRootSceneNode()->createChildSceneNode();
    for (size_t i = 0; i <_wheels; i++) {
        _wheelEntities[i] = _sceneManager->createEntity(_name + "_wheel.mesh");
        _wheelNodes[i] = _wheelsNodeParent->createChildSceneNode();
        _wheelNodes[i]->attachObject(_wheelEntities[i]);
        _wheelNodes[i]->scale(1.3, 1.3, 1.3);
    }

    float connectionHeight = 0.7f;

    bool isFrontWheel = true;
    Ogre::Vector3 connectionPointCS0 (1-(0.3*_wheelWidth),
                                      connectionHeight, 2-_wheelRadius);

    _vehicle->addWheel(_wheelNodes[0],
            connectionPointCS0,
            wheelDirectionCS0,
            wheelAxleCS,
            _suspensionRestLenght,
            _wheelRadius,
            isFrontWheel,
            _wheelFriction,
            _rollInfluence);

    connectionPointCS0 = Ogre::Vector3(-1+(0.3*_wheelWidth),
                                       connectionHeight, 2-_wheelRadius);

    _vehicle->addWheel(_wheelNodes[1],
            connectionPointCS0,
            wheelDirectionCS0,
            wheelAxleCS,
            _suspensionRestLenght,
            _wheelRadius,
            isFrontWheel,
            _wheelFriction,
            _rollInfluence);

    isFrontWheel = false;
    connectionPointCS0 = Ogre::Vector3(-1+(0.3*_wheelWidth),
                                       connectionHeight,-2+_wheelRadius);

    _vehicle->addWheel(_wheelNodes[2],
            connectionPointCS0,
            wheelDirectionCS0,
            wheelAxleCS,
            _suspensionRestLenght,
            _wheelRadius,
            isFrontWheel,
            _wheelFriction,
            _rollInfluence);


    connectionPointCS0 = Ogre::Vector3(1-(0.3*_wheelWidth),
                                       connectionHeight,-2+_wheelRadius);

    _vehicle->addWheel(_wheelNodes[3],
            connectionPointCS0,
            wheelDirectionCS0,
            wheelAxleCS,
            _suspensionRestLenght,
            _wheelRadius,
            isFrontWheel,
            _wheelFriction,
            _rollInfluence);

    //    std::cout << "Vehicle " << _name << " created" << std::endl;
}

void CharacterTerrestrial::addWheel()
{
}

void CharacterTerrestrial::createWeapons()
{
    // Weapons
    _weaponPrincipal = new Weapon("machine_gun",
                                  _ammunition * PRIMARY_WEAPON_C,
                                  PRIMARY_WEAPON_RATE,
                                  PRIMARY_WEAPON_POWER,
                                  PRIMARY_WEAPON_VELOCITY,
                                  _node,
                                  _sceneManager,
                                  _physicsManager);

    _weaponSecundary = new Weapon("cannon",
                                  _ammunition * SECUNDARY_WEAPON_C,
                                  SECUNDARY_WEAPON_RATE,
                                  SECUNDARY_WEAPON_POWER,
                                  SECUNDARY_WEAPON_VELOCITY,
                                  _node,
                                  _sceneManager,
                                  _physicsManager);
}

void CharacterTerrestrial::createExplotions()
{
    // Explotion
    _lightExplotionHit = _sceneManager->createLight();
    _lightExplotionHit->setPosition(Ogre::Vector3::ZERO);
    _lightExplotionHit->setType(Ogre::Light::LT_POINT);
    _lightExplotionHit->setDiffuseColour(1.0, 0.0, 0.0);
    _lightExplotionHit->setSpecularColour(1.0, 0.0, 0.0);

    _lightExplotionDeath = _sceneManager->createLight();
    _lightExplotionDeath->setPosition(Ogre::Vector3::ZERO);
    _lightExplotionDeath->setType(Ogre::Light::LT_POINT);
    _lightExplotionDeath->setDiffuseColour(1.0, 0.0, 0.0);
    _lightExplotionDeath->setSpecularColour(1.0, 0.0, 0.0);

    _bbsHit = _sceneManager->createBillboardSet(1);
    _bbsHit->createBillboard(Ogre::Vector3::ZERO);
    _bbsHit->setMaterialName("Explotion4");
    _bbsHit->setDefaultDimensions(5,5);
    _bbsHit->setCommonDirection(Ogre::Vector3(0,1,0));

    _bbsDeath = _sceneManager->createBillboardSet(1);
    _bbsDeath->createBillboard(Ogre::Vector3::ZERO);
    _bbsDeath->setMaterialName("Explotion3");
    _bbsDeath->setDefaultDimensions(7,7);
    _bbsDeath->setCommonDirection(Ogre::Vector3(0,1,0));

    _explotionHitNode = _sceneManager->getRootSceneNode()->createChildSceneNode();
    _explotionHitNode->attachObject(_bbsHit);
    _explotionHitNode->attachObject(_lightExplotionHit);
    _explotionHitNode->setVisible(false);

    _explotionHitDeath = _sceneManager->getRootSceneNode()->createChildSceneNode();
    _explotionHitDeath->attachObject(_bbsDeath);
    _explotionHitDeath->attachObject(_lightExplotionDeath);
    _explotionHitDeath->setVisible(false);
}

void CharacterTerrestrial::fall()
{
    Ogre::Ray ray =Ogre::Ray (_node->getPosition(), Ogre::Vector3(0,-1,0));
    CollisionClosestRayResultCallback cQuery = CollisionClosestRayResultCallback(ray, _physicsManager->getWorld(), 4);
    this->_physicsManager->getWorld()->launchRay(cQuery);
    if(cQuery.doesCollide()){
        _falling = false;
    }else{
        _falling = true;
    }
}

void CharacterTerrestrial::blastOn()
{
    Ogre::Vector3 position = _node->getPosition();
    position.y = position.y + 2;
    position.z = position.z + 1;

    _explotionHitNode->setPosition(position);
    _explotionHitNode->setVisible(true);
    timerExplotionHit = SDL_AddTimer(200, this->blastOff, _explotionHitNode);
}

void CharacterTerrestrial::blastDeath()
{
    Ogre::Vector3 position = _node->getPosition();
    position.y = position.y + 2;
    position.z = position.z + 1;

    _explotionHitDeath->setPosition(position);
    _explotionHitDeath->setVisible(true);
    timerExplotionDeath = SDL_AddTimer(500, this->blastOff, _explotionHitDeath);
}

Uint32 CharacterTerrestrial::blastOff(Uint32 interval, void *param)
{
    Ogre::SceneNode* node = static_cast <Ogre::SceneNode*> (param);
    node->setVisible(false);
    return 0;
}

Uint32 CharacterTerrestrial::blastDeathOff(Uint32 interval, void *param)
{
    Ogre::SceneNode* node = static_cast <Ogre::SceneNode*> (param);
    node->setVisible(false);
    return 0;
}

void CharacterTerrestrial::rotate(const btQuaternion &rotation)
{
    _transform = _vehicle->getBulletVehicle()->getRigidBody()->getWorldTransform();
    _transform.setRotation(rotation);
    _vehicle->getBulletVehicle()->getRigidBody()->setWorldTransform(_transform);
}

void CharacterTerrestrial::accelerate()
{
    if(_engineForce <= _maxEngineForce){
        _engineForce += _acceleration;
    }
}

void CharacterTerrestrial::back()
{
    if(_engineForce > -_maxEngineForce){
        _engineForce -= _acceleration;
    }
}

void CharacterTerrestrial::brake()
{
    if(_engineForce != 0){
        _engineForce = 0;
    }
}

void CharacterTerrestrial::stop()
{
    _old = _vehicle->getBulletVehicle()->getRigidBody()->getLinearVelocity();
    _old.setX(0.1);
    _vehicle->getBulletVehicle()->getRigidBody()->setLinearVelocity(_old);
}

void CharacterTerrestrial::initialState()
{
    _steeringLeft = false;
    _steeringRight = false;
    _steering = 0;
    _engineForce = 0;
}

void CharacterTerrestrial::moveLeft(const float timeSinceLastFrame)
{
    if(_state !=Character::MOVE_LEFT){
        this->rotate(btQuaternion(btVector3(0,1,0), -DEGREE_90));
        _state = Character::MOVE_LEFT;
    }
    this->accelerate();

}

void CharacterTerrestrial::moveRight(const float timeSinceLastFrame)
{
    if(_state !=Character::MOVE_RIGHT){
        this->rotate(btQuaternion(btVector3(0,1,0), DEGREE_90));
        _state = Character::MOVE_RIGHT;
    }
    this->accelerate();
}

void CharacterTerrestrial::jump(const float timeSinceLastFrame)
{

    if (_state != Character::JUMP){

        _old = _vehicle->getBulletVehicle()->getRigidBody()->getLinearVelocity();

        if(_left || _right){

            if(_left){
                _vehicle->getBulletVehicle()->getRigidBody()->setLinearVelocity(btVector3(-10,8,0));

            }else{
                _vehicle->getBulletVehicle()->getRigidBody()->setLinearVelocity(btVector3(10,8,0));

            }

        }

        else{
            _old.setY(8);
            _vehicle->getBulletVehicle()->getRigidBody()->setLinearVelocity(_old);

        }

        _state = Character::JUMP;
    }
}

void CharacterTerrestrial::teleport(const Ogre::Vector3 position)
{
    // Teleport to position
    _transform = _vehicle->getBulletVehicle()->getRigidBody()->getWorldTransform();
    _transform.setOrigin(btVector3(position.x, position.y, position.z));
    _vehicle->getBulletVehicle()->getRigidBody()->setWorldTransform(_transform);

    // Set Horizontal
    rotate(btQuaternion(btVector3(0,0,1), 0));

    // Stop the vehicle
    stop();
}

void CharacterTerrestrial::shootPrincipal()
{
    _weaponPrincipal->shoot();
}

void CharacterTerrestrial::shootSecondary()
{
    _weaponSecundary->shoot();
}

int CharacterTerrestrial::getAmmunitationPrincipal()
{
    return _weaponPrincipal->getAmmunition();
}

int CharacterTerrestrial::getAmmunitationSecondary()
{
    return _weaponSecundary->getAmmunition();

}

void CharacterTerrestrial::update(double timeSinceLastFrame)
{

    fall();

    if (!_falling){
        if(_left || _right || _jump){

            if (_jump) jump(timeSinceLastFrame);
            if (_left) moveLeft(timeSinceLastFrame);
            if (_right) moveRight(timeSinceLastFrame);

        }else{
            this->brake();
        }
    }

    // Apply engine Force on relevant wheels
    _vehicle->applyEngineForce(_engineForce, 0);
    _vehicle->applyEngineForce(_engineForce, 1);

    //    calculateSteering();

    if(_shootPrincipal){
        this->shootPrincipal();
    }

    if(_shootSecondary){
        this->shootSecondary();
    }

    _left = false;
    _right = false;
    _up = false;
    _down = false;
    _jump = false;
    _shootPrincipal = false;
    _shootSecondary = false;
}

void CharacterTerrestrial::kill()
{
    _nLifes = _nLifes -1;

    if(_nLifes <= 0){
        _death = true;

    }else{
        _lifetime = _maxLife;
    }
}

void CharacterTerrestrial::changeTexture(const Ogre::String &material)
{
    _chassisEntity->setMaterialName(_name + "_body_" + material);
    _turretEntity->setMaterialName(_name + "_turret_" + material);
}

void CharacterTerrestrial::getHit()
{
    _lifetime -= 1;

    if (_lifetime <= 0){

        blastDeath();
        kill();

        if(_death){
            delete this;
        }

    }else{
        blastOn();
    }
}

int CharacterTerrestrial::getLifetime()
{
    return _lifetime;
}

int CharacterTerrestrial::getLifes()
{
    return _nLifes;
}

void CharacterTerrestrial::reloadPrincipal(int ammunition)
{
    _weaponPrincipal->reload(ammunition);
}

void CharacterTerrestrial::reloadSecondary(int ammunition)
{
    _weaponSecundary->reload(ammunition);
}

void CharacterTerrestrial::reloadLife()
{
    _lifetime = _maxLife;
}

RaycastVehicle *CharacterTerrestrial::getVehicle()
{
    return _vehicle;
}

WheeledRigidBody *CharacterTerrestrial::getVehicleBody()
{
    return _vehicleBody;
}

void CharacterTerrestrial::calculateSteering()
{
    // Calculate Steering
    if (_steeringLeft)
    {
        _steering += _steeringIncrement;
        if (_steering > _steeringClamp)
            _steering = _steeringClamp;
    }

    if (_steeringRight)
    {
        _steering -= _steeringIncrement;
        if (_steering < -_steeringClamp)
            _steering = -_steeringClamp;
    }

    if(!_steeringLeft && !_steeringRight){
        // mSteering = 0;         // Arcade 100%

        // Arcade 50%
        if(_steering < 0){
            // Right
            _steering += _steeringIncrement;

            // Center
            if(_steering > 0){
                _steering =0;
            }

        }else{
            // Left
            _steering -= _steeringIncrement;

            // Center
            if(_steering < 0){
                _steering =0;
            }

        }
    }

    // Apply Steering on relevant wheels
    _vehicle->setSteeringValue(_steering, 0);
    _vehicle->setSteeringValue(_steering, 1);
}

