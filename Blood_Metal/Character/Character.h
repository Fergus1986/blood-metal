﻿#ifndef CHARACTER_H
#define CHARACTER_H

#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreString.h>
#include <OGRE/OgreVector3.h>
#include <OGRE/OgreQuaternion.h>


#include "Object3D.h"
#include "PhysicsManager.h"
#include "CollisionClass.h"
#include "Weapon.h"

#define MOVEMENT_NORMAL 6
#define MOVEMENT_JUMP 3
#define MOVEMENT_GRAVITY -9.8
#define JUMP_NORMAL 14

#define DEGREE_90 1.57079633
#define DEGREE_45 0.785398163


class PhysicsManager;

class Character : public CollisionClass
{
public:

    enum characterWeapon{
        PRIMARY_WEAPON_RATE = 300,
        PRIMARY_WEAPON_POWER = 10,
        PRIMARY_WEAPON_VELOCITY = 30,
        SECUNDARY_WEAPON_RATE = 750,
        SECUNDARY_WEAPON_POWER = 50,
        SECUNDARY_WEAPON_VELOCITY = 15
    };


    enum characterCoefficient{
        LIFE_C = 2,
        PRIMARY_WEAPON_C = 10,
        SECUNDARY_WEAPON_C = 1
    };

    enum characterMove{
        STAND,
        MOVE_LEFT,
        MOVE_RIGHT,
        JUMP,
        FALL
    };

    enum characterShoot{
        SHOOT_UP,
        SHOOT_LEFT,
        SHOOT_RIGHT,
        SHOOT_DOWN
    };

    Character();
    Character(const Ogre::String name,
              const Ogre::Vector3 position,
              const Ogre::Real mass,
              const int armour,
              const int ammunition,
              const int power,
              const int lifes,
              Ogre::SceneManager *sceneManager,
              PhysicsManager *physicsManager);

    ~Character();

    Ogre::SceneNode* getNode();

    // Character Interface
    virtual void moveLeft(const float timeSinceLastFrame)=0;
    virtual void moveRight(const float timeSinceLastFrame)=0;
    virtual void jump(const float timeSinceLastFrame)=0;
    virtual void teleport(const Ogre::Vector3 position) = 0;
    virtual void shootPrincipal()=0;
    virtual void shootSecondary()=0;
    virtual int getAmmunitationPrincipal() = 0;
    virtual int getAmmunitationSecondary() = 0;
    virtual void reloadPrincipal(int ammunition) = 0;
    virtual void reloadSecondary(int ammunition) = 0;
    virtual void reloadLife() = 0;
    virtual void update(double timeSinceLastFrame)=0;
    virtual void kill() = 0;
    virtual void changeTexture(const Ogre::String &material) = 0;

    // Signals
    void signal_left();
    void signal_right();
    void signal_up();
    void signal_down();
    void signal_jump();
    void signal_shootPrincipal();
    void signal_shootSecondary();

    // CollisionClass Interface
    void getHit();
    void setCollisionType(int type);
    int getCollisionType();

protected:
    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physicsManager;

    Ogre::SceneNode* _node;
    Ogre::Vector3 _origin;

    // Charater properties
    Ogre::String _name;
    Ogre::Real _mass;
    int _armour;
    int _ammunition;
    int _power;
    int _nLifes;
    int _maxLife;
    int _lifetime;
    bool _death;

    // Weapons
    Weapon* _weaponPrincipal;
    Weapon* _weaponSecundary;

    // Movement
    bool _left;
    bool _right;
    bool _up;
    bool _down;
    bool _jump;
    bool _shootPrincipal;
    bool _shootSecondary;
    bool _falling;

    btTransform _transform;
    btQuaternion _quaternion;
    btVector3 _velocity;

};

#endif // CHARACTER_H
