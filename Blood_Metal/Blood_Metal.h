#ifndef BLOOD_METAL_H
#define BLOOD_METAL_H

#include "AdvancedOgreFramework.hpp"
#include "AppStateManager.hpp"

#include "MenuState.hpp"
#include "GameState.hpp"
#include "PauseState.hpp"
#include "AcercaState.hpp"
#include "LogrosState.hpp"
#include "TutorialState.hpp"
#include "OpcionesState.hpp"
#include "IntroState.hpp"
#include "ConfigState.hpp"
#include "EnhorabuenaState.hpp"
#include "MuerteState.hpp"
class Blood_Metal
{
public:
    Blood_Metal();
    ~Blood_Metal();

    void startGame();

private:
    AppStateManager*	m_pAppStateManager;
};

#endif // BLOOD_METAL_H
