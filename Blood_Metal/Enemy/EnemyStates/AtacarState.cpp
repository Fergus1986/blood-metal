#include "AtacarState.h"
#define DELAY_SHOT 300
template<> AtacarState* Ogre::Singleton<AtacarState>::msSingleton = 0;
AtacarState::AtacarState()
{
    this->_name="AtacarState";
}

void AtacarState::enter(Enemy* e)
{
    //Iniciar Valores de Delay
    e->setWaitDelay(false);
    e->getTimer()->reset();
}

void AtacarState::exit()
{
}

int AtacarState::update(Enemy* e,double timeSinceLastFrame)
{
    // Frenar
    e->stop();
    // Disparar con DELAY
    if(!e->getWaitDelay()){
        e->setWaitDelay(true);
        e->getTimer()->reset();
    }else{
        if(e->getTimer()->getMilliseconds()>=DELAY_SHOT){
            e->signal_shootPrincipal();
        }
    }

    return e->VisionRay();
}

string AtacarState::getName()
{
    return this->_name;
}

AtacarState &AtacarState::getSingleton()
{
    assert(msSingleton);
    return *msSingleton;
}

AtacarState *AtacarState::getSingletonPtr()
{
    return msSingleton;
}


