#include "BuscarState.h"

template<> BuscarState* Ogre::Singleton<BuscarState>::msSingleton = 0;
BuscarState::BuscarState()
{
    this->_name="BuscarState";
}

void BuscarState::enter(Enemy* e)
{
}

void BuscarState::exit()
{
}

int BuscarState::update(Enemy* e,double timeSinceLastFrame)
{
    std::cout<<"BUSCARSTATE\n"<<endl;
    if(e->CollisionRay(40)==CollisionClass::PLAYER){
        if(e->getOrientation()==1){
            std::cout<<"SEGUIR DER"<<endl;
            return RUTADER;

        }else{
            std::cout<<"SERGUIR IZQ"<<endl;
            return RUTAIZQ;

        }
    }else{
        if(e->getOrientation()==1){
            std::cout<<"MIRAR_IZQ"<<endl;
            return MIRARIZQ;

        }else{
            std::cout<<"MIRAR_DER"<<endl;
            return MIRARDER;

        }
    }
}

string BuscarState::getName()
{
    return this->_name;
}

BuscarState &BuscarState::getSingleton()
{
    assert(msSingleton);
    return *msSingleton;
}

BuscarState *BuscarState::getSingletonPtr()
{
    return msSingleton;
}
