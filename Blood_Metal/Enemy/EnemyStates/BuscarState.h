#ifndef BUSCARSTATE_H
#define BUSCARSTATE_H
#include "EnemyState.h"
class BuscarState:public Ogre::Singleton<BuscarState>,public EnemyState
{
public:
    BuscarState();
    void enter(Enemy* e);
    void exit();
    int update(Enemy* e,double timeSinceLastFrame);
    string getName();

    static BuscarState& getSingleton ();
    static BuscarState* getSingletonPtr ();
};

#endif // BUSCARSTATE_H
