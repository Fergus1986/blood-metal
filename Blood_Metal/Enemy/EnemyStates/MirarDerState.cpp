#include "MirarDerState.h"
template<> MirarDerState* Ogre::Singleton<MirarDerState>::msSingleton = 0;
MirarDerState::MirarDerState()
{
    this->_name="MirarDerState";
}
void MirarDerState::enter(Enemy* e)
{

    e->signal_right();
    //Cambiar Límite de Ruta
    e->setlimiteLeftPatrol();
}

void MirarDerState::exit()
{
}

int MirarDerState::update(Enemy* e,double timeSinceLastFrame)
{
    // FRENO
    e->stop();
    int colision;
    colision=e->CollisionRay(10);
    if(colision==CollisionClass::PLAYER){
        return ATACAR;
    }else{
        if(colision==CollisionClass::ENEMY){
            return MIRARIZQ;
        }else
            return RUTADER;
    }
}

string MirarDerState::getName()
{
    return this->_name;
}

MirarDerState &MirarDerState::getSingleton()
{
    assert(msSingleton);
    return *msSingleton;
}

MirarDerState *MirarDerState::getSingletonPtr()
{
    return msSingleton;
}
