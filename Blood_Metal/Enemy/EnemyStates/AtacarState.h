#ifndef ATACARSTATE_H
#define ATACARSTATE_H
#include "EnemyState.h"
class AtacarState:public Ogre::Singleton<AtacarState>,public EnemyState
{
public:
    AtacarState();
    void enter(Enemy* e);
    void exit();
    int update(Enemy* e,double timeSinceLastFrame);
    string getName();

    static AtacarState& getSingleton ();
    static AtacarState* getSingletonPtr ();
};

#endif // ATACARSTATE_H
