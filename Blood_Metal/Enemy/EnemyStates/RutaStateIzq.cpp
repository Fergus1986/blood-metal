#include "RutaStateIzq.h"

template<> RutaStateIzq* Ogre::Singleton<RutaStateIzq>::msSingleton = 0;

RutaStateIzq::RutaStateIzq()
{
    this->_name="RutaStateIzq";
}
void RutaStateIzq::enter(Enemy* e)
{
    e->setPatrol();
}

void RutaStateIzq::exit()
{
}

int RutaStateIzq::update(Enemy* e,double timeSinceLastFrame)
{
    int result;
    //COMPROBAR HUECO EN EL SUELO
    if (e->RayGround(4)==2){
        result=MIRARDER;
        //COMPROBAR LIMITE DE PATRULLA
    }else if(e->getNode()->getPosition().x<=e->getlimiteLeftPatrol()){
        result=MIRARDER;
    }else{
        e->signal_left();
        result=e->VisionRay();
    }
    return result;
}

RutaStateIzq &RutaStateIzq::getSingleton()
{
    assert(msSingleton);
    return *msSingleton;
}

RutaStateIzq *RutaStateIzq::getSingletonPtr()
{
    return msSingleton;
}

string RutaStateIzq::getName()
{
    return this->_name;
}
