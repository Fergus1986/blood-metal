#include "StateManager.h"
#include "Character.h"

template<> StateManager* Ogre::Singleton<StateManager>::msSingleton = 0;

StateManager::StateManager()
{
    RutaStateIzq* rutaizq=new RutaStateIzq();
    RutaStateDer* rutader=new RutaStateDer();
    AtacarState* atacar=new AtacarState();
    MirarDerState* mirard= new MirarDerState();
    MirarIzqState* mirari=new MirarIzqState();
    BuscarState* buscar=new BuscarState();
    ENEMY_STATE_CREATE(rutaizq);
    ENEMY_STATE_CREATE(rutader);
    ENEMY_STATE_CREATE(atacar);
    ENEMY_STATE_CREATE(mirard);
    ENEMY_STATE_CREATE(mirari);
    ENEMY_STATE_CREATE(buscar);
    this->cids=0;

}
StateManager::StateManager(std::vector<Enemy*> v)
{
    enemigos=v;
    RutaStateIzq* rutaizq=new RutaStateIzq();
    RutaStateDer* rutader=new RutaStateDer();
    AtacarState* atacar=new AtacarState();
    MirarDerState* mirard= new MirarDerState();
    MirarIzqState* mirari=new MirarIzqState();
    BuscarState* buscar=new BuscarState();
    ENEMY_STATE_CREATE(rutaizq);
    ENEMY_STATE_CREATE(rutader);
    ENEMY_STATE_CREATE(atacar);
    ENEMY_STATE_CREATE(mirard);
    ENEMY_STATE_CREATE(mirari);
    ENEMY_STATE_CREATE(buscar);
    this->cids=0;
}

StateManager::~StateManager()
{
}

void StateManager::init()
{
    for(it=enemigos.begin();it!=this->enemigos.end();it++)
        this->ChangeState(RutaStateDer::getSingletonPtr(),(*it));
}


void StateManager::ChangeState(EnemyState* s,Enemy* e)
{
    if(e->getCurrentState()){
        e->getCurrentState()->exit();
    }
    e->setCurrentState(s);
    e->getCurrentState()->enter(e);
}

void StateManager::UpdateState(double timeSinceLastFrame)
{

    int state;
    for(it=enemigos.begin();it!=this->enemigos.end();it++){
        state=(*it)->getCurrentState()->update((*it),timeSinceLastFrame);
        switch (state){
        case RUTAIZQ:
            if((*it)->getCurrentState()->getName()!="RutaStateIzq"){
                this->ChangeState(RutaStateIzq::getSingletonPtr(),(*it));
            }
            break;
        case RUTADER:
            if((*it)->getCurrentState()->getName()!="RutaStateDer"){
                this->ChangeState(RutaStateDer::getSingletonPtr(),(*it));
            }
            break;
        case ATACAR:
            if((*it)->getCurrentState()->getName()!="AtacarState"){
                this->ChangeState(AtacarState::getSingletonPtr(),(*it));
            }
            break;
        case MIRARDER:
            if((*it)->getCurrentState()->getName()!="MirarDerState"){
                this->ChangeState(MirarDerState::getSingletonPtr(),(*it));
            }
            break;

        case MIRARIZQ:
            if((*it)->getCurrentState()->getName()!="MirarIzqState"){
                this->ChangeState(MirarIzqState::getSingletonPtr(),(*it));
            }
            break;
        case BUSCAR:
            if((*it)->getCurrentState()->getName()!="BuscarState"){
                this->ChangeState(BuscarState::getSingletonPtr(),(*it));
            }
            break;
        default:
            break;
        }

        (*it)->update(timeSinceLastFrame);

    }

}

StateManager &StateManager::getSingleton()
{
    assert(msSingleton);
    return *msSingleton;
}

StateManager *StateManager::getSingletonPtr()
{
    return msSingleton;
}

void StateManager::DeleteEnemy(int id){

    std::cout << "StateManager: Eliminado enemigo" << std::endl;

    // erase() will invalidate existing iterators. It returns a new valid iterator
    for(it=this->enemigos.begin(); it!=this->enemigos.end();){
        if((*it)->getId()==id){
            delete *(it);
            it = enemigos.erase(it);
        }else{
            ++it;
        }
    }

    std::cout << "StateManager: Enemigo eliminado" << std::endl;
}

void StateManager::ClearEnemies(){

    for(it=this->enemigos.begin(); it!=this->enemigos.end();){
        delete *(it);
        it = enemigos.erase(it);
    }

    this->enemigos.clear();
}

void StateManager::setEnemies(std::vector<Enemy*> v)
{
    this->enemigos=v;
}

