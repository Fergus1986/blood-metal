#ifndef MIRARDERSTATE_H
#define MIRARDERSTATE_H
#include "EnemyState.h"
class MirarDerState:public Ogre::Singleton<MirarDerState>,public EnemyState
{
public:
    MirarDerState();
   void enter(Enemy* e);//,const float timeSinceLastFrame);
    void exit();
    int update(Enemy* e,double timeSinceLastFrame);
    string getName();

    static MirarDerState& getSingleton ();
    static MirarDerState* getSingletonPtr ();
};

#endif // MIRARDERSTATE_H
