#ifndef STATEMANAGER_H
#define STATEMANAGER_H
#include "OIS.h"
#include "EnemyState.h"
#include "RutaStateIzq.h"
#include "RutaStateDer.h"
#include "AtacarState.h"
#include "MirarDerState.h"
#include "MirarIzqState.h"
#include "BuscarState.h"
#include "AdvancedOgreFramework.hpp"
#include "Enemy.h"

#define ENEMY_STATE_CREATE(x) (void)x

class StateManager:public Ogre::Singleton<StateManager>
{
public:
    StateManager();
    StateManager(std::vector<Enemy*> e);
    ~StateManager();
    void init();
    void ChangeState(EnemyState* s,Enemy* e);
    void UpdateState(double timeSinceLastFrame);

    static StateManager& getSingleton ();
    static StateManager* getSingletonPtr ();
    void DeleteEnemy(int id);
    void ClearEnemies();

    void setEnemies(std::vector<Enemy*> v);



private:
       std::vector<Enemy*> enemigos;
       std::vector<Enemy*>::iterator it;
       int cids;

};

#endif // STATEMANAGER_H
