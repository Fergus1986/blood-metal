#ifndef RUTASTATE_H
#define RUTASTATE_H
#include "EnemyState.h"
class RutaStateIzq: public Ogre::Singleton<RutaStateIzq>,public EnemyState
{
public:
    RutaStateIzq();
    void enter(Enemy* e);
    void exit();
    int update(Enemy* e,double timeSinceLastFrame);

    static RutaStateIzq& getSingleton ();
    static RutaStateIzq* getSingletonPtr ();
    string getName();



    string _name;
};

#endif // RUTASTATE_H
