#ifndef ENEMYSTATE_H
#define ENEMYSTATE_H
#include "iostream"
#include "Enemy.h"


enum States{
    RUTAIZQ,
    RUTADER,
    ATACAR,
    MIRARIZQ,
    MIRARDER,
    BUSCAR,
    OTRO
};

class Enemy;

using namespace std;


class EnemyState
{

public:

    EnemyState() {}
    virtual void enter(Enemy* e)=0;
    virtual void exit()=0;
    virtual int update(Enemy* e,double timeSinceLastFrame)=0;
    virtual string getName()=0;

    string _name;
};
#endif // ENEMYSTATE_H
