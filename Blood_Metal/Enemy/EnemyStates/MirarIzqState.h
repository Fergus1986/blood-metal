#ifndef MIRARIZQSTATE_H
#define MIRARIZQSTATE_H
#include "EnemyState.h"
class MirarIzqState:public Ogre::Singleton<MirarIzqState>,public EnemyState
{
public:
    MirarIzqState();
    void enter(Enemy* e);
    void exit();
    int update(Enemy* e,double timeSinceLastFrame);
    string getName();

    static MirarIzqState& getSingleton ();
    static MirarIzqState* getSingletonPtr ();
};

#endif // MIRARIZQSTATE_H
