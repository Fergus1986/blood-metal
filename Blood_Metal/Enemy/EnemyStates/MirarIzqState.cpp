#include "MirarIzqState.h"
template<> MirarIzqState* Ogre::Singleton<MirarIzqState>::msSingleton = 0;

MirarIzqState::MirarIzqState()
{
    this->_name="MirarIzqState";
}

void MirarIzqState::enter(Enemy* e)
{   
    e->signal_left();
    //Cambiar Límite de Ruta
    e->setlimiteRightPatrol();
}

void MirarIzqState::exit()
{
}

int MirarIzqState::update(Enemy* e,double timeSinceLastFrame)
{
    e->stop();
    int colision;
    colision=e->CollisionRay(10);
    if(colision==CollisionClass::PLAYER){
        return ATACAR;
    }else{
        if (colision==CollisionClass::ENEMY)
            return MIRARDER;
        else
            return RUTAIZQ;
    }
}



string MirarIzqState::getName()
{
    return this->_name;
}

MirarIzqState &MirarIzqState::getSingleton()
{
    assert(msSingleton);
    return *msSingleton;
}

MirarIzqState *MirarIzqState::getSingletonPtr()
{
    return msSingleton;
}


