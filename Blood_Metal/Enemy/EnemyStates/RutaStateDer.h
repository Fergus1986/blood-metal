#ifndef RUTASTATEDER_H
#define RUTASTATEDER_H
#include "EnemyState.h"

class RutaStateDer: public Ogre::Singleton<RutaStateDer>,public EnemyState
{
public:
    RutaStateDer();
    void enter(Enemy* e);
    void exit();
    int update(Enemy* e,double timeSinceLastFrame);
    static RutaStateDer& getSingleton ();
    static RutaStateDer* getSingletonPtr ();
    string getName();
    string _name;
};

#endif // RUTASTATEDER_H
