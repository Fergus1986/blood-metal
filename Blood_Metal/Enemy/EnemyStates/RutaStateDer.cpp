#include "RutaStateDer.h"
template<> RutaStateDer* Ogre::Singleton<RutaStateDer>::msSingleton = 0;

RutaStateDer::RutaStateDer()
{
    this->_name="RutaStateDer";
}


void RutaStateDer ::enter(Enemy* e)
{
    e->setPatrol();
}

void RutaStateDer ::exit()
{   
}

int RutaStateDer ::update(Enemy* e,double timeSinceLastFrame)
{
    int result;
    //COMPROBAR HUECO EN EL SUELO
    if (e->RayGround(4)==1){
        result=MIRARIZQ;
        //COMPROBAR LIMITE DE PATRULLA
    }else if(e->getNode()->getPosition().x>=e->getlimiteRightPatrol()){
        result=MIRARIZQ;
    }else{
        e->signal_right();
        result=e->VisionRay();
    }
    return result;
}

RutaStateDer  &RutaStateDer ::getSingleton()
{
    assert(msSingleton);
    return *msSingleton;
}

RutaStateDer  *RutaStateDer ::getSingletonPtr()
{
    return msSingleton;
}

string RutaStateDer ::getName()
{
    return this->_name;
}
