#include "Enemy.h"
#include "EnemyStates/StateManager.h"
#include "EnemyStates/BuscarState.h"

Enemy::Enemy()
{
}

Enemy::Enemy(const int id,
             const Ogre::String name,
             const Ogre::Vector3 position,
             const Ogre::Real mass,
             const int armour,
             const int ammunition,
             const int power,
             const int lifes,
             Ogre::SceneManager *sceneManager,
             PhysicsManager *physicsManager):
    CharacterTerrestrial(name, position, mass, armour, ammunition, power, lifes, sceneManager, physicsManager)
{

    this->setCollisionType(ENEMY);

    this->_vehicleBody->getBulletRigidBody()->setUserPointer(this);

    this->_id=id;
    this->CurrentState=RutaStateDer::getSingletonPtr();

    mRayScnQuery =sceneManager->createRayQuery(Ogre::Ray());
    this->_ShotDelay=new Ogre::Timer();
    this->_ShotDelay->reset();
    this->_waitDelay=false;

}

Enemy::~Enemy()
{
    std::cout << "Destructor Enemy" << std::endl;
}

void Enemy::getHit()
{
    std::cout << "Enemy golpeado" << std::endl;

    _lifetime -= 1;

    if (_lifetime <= 0){

        blastDeath();
        kill();
        if (_death){
            StateManager::getSingletonPtr()->DeleteEnemy(this->_id);
        }
    }else{
        blastOn();
        StateManager::getSingletonPtr()->ChangeState(BuscarState::getSingletonPtr(),this);
    }
}

void Enemy::setCollisionType(int type)
{
    this->CollisionType = type;
}

int Enemy::getCollisionType()
{
    return this->CollisionType;
}
void Enemy::patrol(double timeSinceLastFrame)
{
    if(this->getNode()->getPosition().x > _limitLeftPatrol && _leftPatrol) this->moveLeft(timeSinceLastFrame);
    if(this->getNode()->getPosition().x < _limitLeftPatrol) this->_leftPatrol = false;
    if(_leftPatrol ==false) this->moveRight(timeSinceLastFrame);
    if(this->getNode()->getPosition().x > _limitRightPatrol) this->_leftPatrol = true;
    //RigidBody* b=this->collisionRay();
}

void Enemy::setPatrol()
{
    _limitLeftPatrol = this->getNode()->getPosition().x -24;
    _limitRightPatrol = this->getNode()->getPosition().x +24;
}
void Enemy::setlimiteLeftPatrol()
{
    _limitLeftPatrol = this->getNode()->getPosition().x;
    _limitRightPatrol = this->getNode()->getPosition().x +24;

}
void Enemy::setlimiteRightPatrol()
{
    _limitRightPatrol = this->getNode()->getPosition().x;
    _limitLeftPatrol = this->getNode()->getPosition().x-24;

}

int Enemy::getlimiteLeftPatrol()
{
    return this->_limitLeftPatrol;
}

int Enemy::getlimiteRightPatrol()
{
    return this->_limitRightPatrol;
}

void Enemy::setCurrentState(EnemyState* es)
{
    CurrentState=es;
}

EnemyState* Enemy::getCurrentState()
{
    return this->CurrentState;
}

int Enemy::getId()
{
    return this->_id;
}
int Enemy::VisionRay()
{
    int result=OTRO;
    int orient=this->getOrientation();
    Ogre::Ray rayEnemy =Ogre::Ray (Ogre::Vector3(this->getNode()->getPosition().x,this->getNode()->getPosition().y+1,this->getNode()->getPosition().z),Ogre::Vector3(orient,0,0));
    CollisionClosestRayResultCallback cQuery = CollisionClosestRayResultCallback(rayEnemy,this->_physicsManager->getWorld(),12);
    this->_physicsManager->getWorld()->launchRay(cQuery);
    if(cQuery.doesCollide()){

        RigidBody* body =  static_cast<RigidBody *> (cQuery.getCollidedObject());
        CollisionClass* c=(CollisionClass*)body->getBulletRigidBody()->getUserPointer();
        if( c->getCollisionType()==CollisionClass::PLAYER && this->getCurrentState()->getName()=="RutaStateIzq"){// || this->getCurrentState()->getName()=="RutaStateDer")){
            result=ATACAR;
        }else if(c->getCollisionType()==CollisionClass::PLAYER && this->getCurrentState()->getName()=="RutaStateDer"){
            result=ATACAR;
        }else if(c->getCollisionType()==CollisionClass::ENEMY && this->getCurrentState()->getName()=="RutaStateDer"){
            result=MIRARIZQ;
        }else if(c->getCollisionType()==CollisionClass::ENEMY && this->getCurrentState()->getName()=="RutaStateIzq"){
            result=MIRARDER;
        }else if(c->getCollisionType()==CollisionClass::PLAYER && this->getCurrentState()->getName()=="AtacarState"){
            result=ATACAR;
        }
    }else{
        if(this->getCurrentState()->getName()=="AtacarState"){
            if(orient==1){
                result=MIRARDER;
            }else if(orient==-1)
                result=MIRARIZQ;
        }else if(this->getCurrentState()->getName()=="RutaStateIzq"){
            result=RUTAIZQ;
        }else if(this->getCurrentState()->getName()=="RutaStateDer"){
            result=RUTADER;
        }
    }
    return result;
}



int Enemy::CollisionRay(Ogre::Real MaxDistance)
{
    int result=OTHER;
    int orient=getOrientation();
    Ogre::Ray rayEnemy =Ogre::Ray (Ogre::Vector3(this->getNode()->getPosition().x,this->getNode()->getPosition().y+1,this->getNode()->getPosition().z),Ogre::Vector3(orient,0,0));
    CollisionClosestRayResultCallback cQuery = CollisionClosestRayResultCallback(rayEnemy,this->_physicsManager->getWorld(),MaxDistance);
    this->_physicsManager->getWorld()->launchRay(cQuery);
    if(cQuery.doesCollide()){
        RigidBody* body =  static_cast<RigidBody *> (cQuery.getCollidedObject());
        CollisionClass* c=(CollisionClass*)body->getBulletRigidBody()->getUserPointer();
        result=c->getCollisionType();
    }else
        result=-1;
    return result;
}

int Enemy::RayGround(int offset){
    int result=0;
    int orient=getOrientation();
    Ogre::Ray rayEnemy;
    if(orient==1) rayEnemy =Ogre::Ray (Ogre::Vector3(this->getNode()->getPosition().x+offset,this->getNode()->getPosition().y,this->getNode()->getPosition().z),Ogre::Vector3(0,-1,0));
    else if(orient==-1) rayEnemy =Ogre::Ray (Ogre::Vector3(this->getNode()->getPosition().x-offset,this->getNode()->getPosition().y,this->getNode()->getPosition().z),Ogre::Vector3(0,-1,0));
    CollisionClosestRayResultCallback cQuery = CollisionClosestRayResultCallback(rayEnemy,this->_physicsManager->getWorld(),8);
    this->_physicsManager->getWorld()->launchRay(cQuery);
    if(!cQuery.doesCollide()){
        if(orient==1){
            result=1;
        }
        else if(orient==-1){
            result=2;
        }
    }else{
        result=-1;
    }
    return result;
}

int Enemy::getOrientation(){
    if(this->getNode()->getOrientation().y>0) return 1;
    else return -1;
}

Ogre::Timer *Enemy::getTimer()
{
    return this->_ShotDelay;
}

bool Enemy::getWaitDelay()
{
    return this->_waitDelay;
}

void Enemy::setWaitDelay(bool v)
{
    this->_waitDelay=v;
}

