#ifndef ENEMY_H
#define ENEMY_H

#include "CharacterTerrestrial.h"
#include "EnemyStates/EnemyState.h"

#include "Physics/CollisionClass.h"

class EnemyState;

class Enemy : public CharacterTerrestrial
{
public:
    Enemy();
    Enemy(const int id,
          const Ogre::String name,
          const Ogre::Vector3 position,
          const Ogre::Real mass,
          const int armour,
          const int ammunition,
          const int power,
          const int lifes,
          Ogre::SceneManager *sceneManager,
          PhysicsManager *physicsManager);

    ~Enemy();

    // CollisionClass
    void getHit();
    void setCollisionType(int type);
    int getCollisionType();
    /*enemy states*/
    void patrol(double timeSinceLastFrame);
    void setPatrol();
    void setCurrentState(EnemyState* es);
    EnemyState* getCurrentState();
    int getId();

    int VisionRay();
    int CollisionRay(Ogre::Real MaxDistance);
//    int RayGround();
    int RayGround(int offset);


    void setlimiteLeftPatrol();
    void setlimiteRightPatrol();

    int getlimiteLeftPatrol();
    int getlimiteRightPatrol();

    int getOrientation();
    int getLife();


    //int VisionRay(Ogre::Vector3 orientation);

    Ogre::Timer* getTimer();
    bool getWaitDelay();
    void setWaitDelay(bool v);
private:
    int _id;
    int _limitLeftPatrol;
    int _limitRightPatrol;
    bool _leftPatrol;
    EnemyState* CurrentState;
    Ogre::RaySceneQuery* mRayScnQuery;
    Ogre::Timer* _ShotDelay;
    bool _waitDelay;


};

#endif // ENEMY_H
