#include "Blood_Metal.h"

Blood_Metal::Blood_Metal()
{
    m_pAppStateManager = 0;
}

Blood_Metal::~Blood_Metal()
{
    if(m_pAppStateManager){
        delete m_pAppStateManager;
//        delete OgreFramework::getSingletonPtr();
    }
}


void Blood_Metal::startGame()
{
    new OgreFramework();
    if(!OgreFramework::getSingletonPtr()->initOgre("Blood & Metal", 0, 0, 0)){
        // ERROR

    }else{
        OgreFramework::getSingletonPtr()->m_pLog->logMessage("Game initialized!");

        m_pAppStateManager = new AppStateManager();

        MenuState::create(m_pAppStateManager, "MenuState");
        GameState::create(m_pAppStateManager, "GameState");
        PauseState::create(m_pAppStateManager, "PauseState");
        AcercaState::create(m_pAppStateManager, "AcercaState");
        LogrosState::create(m_pAppStateManager,"LogrosState");
        TutorialState::create(m_pAppStateManager, "TutorialState");
        OpcionesState::create(m_pAppStateManager, "OpcionesState");
        IntroState::create(m_pAppStateManager, "IntroState");
        ConfigState::create(m_pAppStateManager, "ConfigState");
        EnhorabuenaState::create(m_pAppStateManager, "EnhorabuenaState");
        MuerteState::create(m_pAppStateManager, "MuerteState");
        m_pAppStateManager->start(m_pAppStateManager->findByName("IntroState"));
    }
}
