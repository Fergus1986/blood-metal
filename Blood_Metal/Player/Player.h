#ifndef PLAYER_H
#define PLAYER_H


#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreString.h>

#include "PhysicsManager.h"
#include "CharacterTerrestrial.h"

class Player : public CharacterTerrestrial
{
public:
    Player();
    ~Player();
    Player(const Ogre::String name,
           const Ogre::Vector3 position,
           const Ogre::Real mass,
           const int armour,
           const int ammunition,
           const int power,
           const int lifes,
           Ogre::SceneManager *sceneManager,
           PhysicsManager *physicsManager);

    bool isDeath();
    void reboot();

    // CollisionClass Interface
    void getHit();

};

#endif // PLAYER_H
