#include "Player.h"

Player::Player()
{
}

Player::~Player()
{
}

Player::Player(const Ogre::String name,
               const Ogre::Vector3 position,
               const Ogre::Real mass,
               const int armour,
               const int ammunition,
               const int power,
               const int lifes,
               Ogre::SceneManager *sceneManager,
               PhysicsManager *physicsManager):
    CharacterTerrestrial(name, position, mass, armour, ammunition, power, lifes, sceneManager, physicsManager)
{


    // Collisions Manager
    this->setCollisionType(CollisionClass::PLAYER);

}

bool Player::isDeath()
{
    return this->_death;
}

void Player::reboot()
{
    // Original position
    teleport(_origin);

    // Original life
    _lifetime = _maxLife;
    _maxLife = LIFE_C * _armour;
    _lifetime = _maxLife;
    _nLifes = LIFE_C;
    _death = false;

    // Original state
    _state = Character::STAND;

    // Original ammunition
    reloadPrincipal(_ammunition * PRIMARY_WEAPON_C);
    reloadSecondary(_ammunition * SECUNDARY_WEAPON_C);
}

void Player::getHit()
{
    _lifetime -= 1;

    if (_lifetime <= 0){

        blastDeath();
        kill();

    }else{
        blastOn();
    }
}
