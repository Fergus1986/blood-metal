#include "Achievement.h"


bool orderAchievement(_achievementStruct lUno, _achievementStruct lDos)
{
    return (lDos._score < lUno._score);
}

Achievement::Achievement()
{
    //empty
}

void Achievement::addAchievement(string name, int score)
{
       ofstream fsalida;
       _achievementStruct l;
       stringstream aux;
       aux << name;
       strcpy(l._name,name.c_str());
       l._score = score;
       fsalida.open(FICHEROLOGROS, ios::out|ios::app|ios::binary);

        fsalida.write(reinterpret_cast<char *>(&l), sizeof(l));
        fsalida.close();
}

std::vector<_achievementStruct> Achievement::getAchievements()
{
        v.reserve(TAM_MAX_LOGROS);
        fstream fentrada;
        fentrada.open(FICHEROLOGROS, ios::in|ios::binary);

        if(fentrada.is_open()){
            _achievementStruct aux;
            while(!fentrada.eof() && !fentrada.fail())
            {
                fentrada.read(reinterpret_cast<char *>(&aux), sizeof(aux));
                if(!fentrada.eof() && !fentrada.fail()) v.push_back(aux);
            }
            sort(v.begin(), v.end() , orderAchievement);
            fentrada.close();
        }
        else{
            //Nothing
        }

        return v;
}


