#ifndef ACHIEVEMENT_H
#define ACHIEVEMENT_H

#include "StructAchievement.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <stdio.h>
#include <string.h>
#include <sstream>
#define FICHEROLOGROS "logros.dat"
#define TAM_MAX_LOGROS 4
using namespace std;


class Achievement
{
public:

    Achievement();
   void  addAchievement(string name,int score);
   std::vector<_achievementStruct> getAchievements();

private:
   std::vector<_achievementStruct> v;




};

#endif // ACHIEVEMENT_H
