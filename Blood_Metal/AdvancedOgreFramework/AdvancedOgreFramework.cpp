#include "AdvancedOgreFramework.hpp"

using namespace Ogre;

template<> OgreFramework* Ogre::Singleton<OgreFramework>::msSingleton = 0;

OgreFramework::OgreFramework()
{
    m_pRoot				= 0;
    m_pRenderWnd		= 0;
    m_pViewport			= 0;
    m_pLog				= 0;
    m_pTimer			= 0;

    m_pInputMgr			= 0;
    m_pKeyboard			= 0;
    m_pMouse			= 0;
    m_pJoystick         = 0;
}


OgreFramework::~OgreFramework()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Shutdown OGRE...");
    if(m_pInputMgr)		OIS::InputManager::destroyInputSystem(m_pInputMgr);
    if(m_pRoot)			delete m_pRoot;
}


bool OgreFramework::initOgre(
        Ogre::String wndTitle,
        OIS::KeyListener *pKeyListener,
        OIS::MouseListener *pMouseListener,
        OIS::JoyStickListener *pJoyStickListener)
{
    Ogre::LogManager* logMgr = new Ogre::LogManager();

    m_pLog = Ogre::LogManager::getSingleton().createLog("OgreLogfile.log", true, true, false);
    m_pLog->setDebugOutputEnabled(true);

    m_pRoot = new Ogre::Root();

    if(!m_pRoot->showConfigDialog())
        return false;

    // Initialize random seed
    srand (time(NULL));

    m_pRenderWnd = m_pRoot->initialise(true, wndTitle);

    m_pViewport = m_pRenderWnd->addViewport(0);
    m_pViewport->setBackgroundColour(ColourValue(0.5f, 0.5f, 0.5f, 1.0f));

    m_pViewport->setCamera(0);

    m_pOverlaySystem = new Ogre::OverlaySystem();

    size_t hWnd = 0;
    OIS::ParamList paramList;
    m_pRenderWnd->getCustomAttribute("WINDOW", &hWnd);

    paramList.insert(OIS::ParamList::value_type("WINDOW", Ogre::StringConverter::toString(hWnd)));

    m_pInputMgr = OIS::InputManager::createInputSystem(paramList);

    // Keyboard & Mouse

    m_pKeyboard = static_cast<OIS::Keyboard*>(m_pInputMgr->createInputObject(OIS::OISKeyboard, true));
    m_pMouse = static_cast<OIS::Mouse*>(m_pInputMgr->createInputObject(OIS::OISMouse, true));

    m_pMouse->getMouseState().height = m_pRenderWnd->getHeight();
    m_pMouse->getMouseState().width	 = m_pRenderWnd->getWidth();

    if(pKeyListener == 0)
        m_pKeyboard->setEventCallback(this);
    else
        m_pKeyboard->setEventCallback(pKeyListener);

    if(pMouseListener == 0)
        m_pMouse->setEventCallback(this);
    else
        m_pMouse->setEventCallback(pMouseListener);

    // Joystick

    try{
        m_pJoystick = static_cast<OIS::JoyStick*>(m_pInputMgr->createInputObject(OIS::OISJoyStick, true));

    }catch(...){
        m_pJoystick = 0;
        std::cerr << "OIS::JoyStick not found" << std::endl;
    }

    if(m_pJoystick){
        std::cout << "OIS::JoyStick found" << std::endl;

        if (pJoyStickListener == 0)
            m_pJoystick->setEventCallback(this);
        else
            m_pJoystick->setEventCallback(pJoyStickListener);
    }


    Ogre::String secName, typeName, archName;
    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(1);
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    OgreBites::InputContext inputContext;
    inputContext.mMouse = m_pMouse;
    inputContext.mKeyboard = m_pKeyboard;

    if(m_pJoystick){
        inputContext.mAccelerometer = m_pJoystick;
    }

    m_pTimer = new Ogre::Timer();
    m_pTimer->reset();

    m_pRenderWnd->setActive(true);

    initSDL();
    initCegui();
    initSoundManager();

    return true;
}

bool OgreFramework::initSDL()
{
    // Initialization SDL
    if(SDL_Init(SDL_INIT_TIMER) < 0){
        return false;
    }

    atexit(SDL_Quit);
    return true;
}

void OgreFramework::initCegui()
{
    m_pCEGUIRenderer = &CEGUI::OgreRenderer::bootstrapSystem();
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
    CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
    CEGUI::System::getSingleton().setDefaultFont("fuenteCtrl_freak");
    CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook","MouseArrow");
}

void OgreFramework::initSoundManager()
{
    // Create the sound manager.
    m_pSoundManager = OgreAL::SoundManager::createManager();
    // Init the sound manager.
    m_pSoundManager->init();
    // Set the audio path.
    m_pSoundManager->setAudioPath("media/audio/");
}

bool OgreFramework::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    if(m_pKeyboard->isKeyDown(OIS::KC_SYSRQ))
    {
        m_pRenderWnd->writeContentsToTimestampedFile("Blood_Metal_shot_", ".png");
        return true;
    }

    return true;
}

bool OgreFramework::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    return true;
}


bool OgreFramework::mouseMoved(const OIS::MouseEvent &evt)
{
    return true;
}


bool OgreFramework::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}


bool OgreFramework::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}

bool OgreFramework::buttonPressed(const OIS::JoyStickEvent &arg, int button)
{
    std::cout << arg.device->vendor() << " Button: " << button << std::endl;
    return true;
}

bool OgreFramework::buttonReleased(const OIS::JoyStickEvent &arg, int button)
{
    return true;
}

bool OgreFramework::axisMoved(const OIS::JoyStickEvent &arg, int axis)
{
    std::cout << arg.device->vendor() << " Axis: " << axis << std::endl;

    return true;
}

bool OgreFramework::povMoved(const OIS::JoyStickEvent &arg, int index)
{
    std::cout << arg.device->vendor() << " Pov: " << index << std::endl;

    return true;
}

void OgreFramework::updateOgre(double timeSinceLastFrame)
{
//    std::cout << "T: " << timeSinceLastFrame << std::endl;
}
