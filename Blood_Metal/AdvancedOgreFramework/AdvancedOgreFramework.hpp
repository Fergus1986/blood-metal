#ifndef OGRE_FRAMEWORK_HPP
#define OGRE_FRAMEWORK_HPP

#include <iostream>
#include <cstdlib>
#include <ctime>

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreOverlay.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>
#include <OISJoyStick.h>

#include <SdkTrays.h>

#include <CEGUI.h>
#include <CEGUI/RendererModules/Ogre/CEGUIOgreRenderer.h>

#include <SDL/SDL.h>

#include "SoundManager.h"

class OgreFramework :
        public Ogre::Singleton<OgreFramework>,
        public OIS::KeyListener,
        public OIS::MouseListener,
        public OIS::JoyStickListener
{
public:
	OgreFramework();
	~OgreFramework();

    bool initOgre(Ogre::String wndTitle,
                  OIS::KeyListener *pKeyListener = 0,
                  OIS::MouseListener *pMouseListener = 0,
                  OIS::JoyStickListener *pJoyStickListener = 0);
    bool initSDL();
    void initCegui();
    void initSoundManager();
    void updateOgre(double timeSinceLastFrame);

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &evt);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    bool buttonPressed(const OIS::JoyStickEvent &arg, int button);
    bool buttonReleased(const OIS::JoyStickEvent &arg, int button);
    bool axisMoved(const OIS::JoyStickEvent &arg, int axis);
    bool povMoved(const OIS::JoyStickEvent &arg, int index);

    Ogre::Root*             m_pRoot;
    Ogre::RenderWindow*     m_pRenderWnd;
    Ogre::Viewport*         m_pViewport;
    Ogre::Log*              m_pLog;
    Ogre::Timer*            m_pTimer;

    OIS::InputManager*      m_pInputMgr;
    OIS::Keyboard*          m_pKeyboard;
    OIS::Mouse*             m_pMouse;
    OIS::JoyStick*          m_pJoystick;

    Ogre::OverlaySystem*    m_pOverlaySystem;
    CEGUI::OgreRenderer*    m_pCEGUIRenderer;

    // Sound manager
    OgreAL::SoundManager*   m_pSoundManager;

private:
	OgreFramework(const OgreFramework&);
	OgreFramework& operator= (const OgreFramework&);
};


#endif
