TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += -std=c++0x
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3
QMAKE_LFLAGS_RELEASE -= -O1
QMAKE_LFLAGS_RELEASE += -O3

TARGET = Blood_Metal

INCLUDEPATH += AdvancedOgreFramework/
INCLUDEPATH += Character/
INCLUDEPATH += EasyOgre/
INCLUDEPATH += Enemy/
INCLUDEPATH += Explosion/
INCLUDEPATH += Level/
INCLUDEPATH += Object3D/
INCLUDEPATH += Physics/
INCLUDEPATH += States/
INCLUDEPATH += Weapon/
INCLUDEPATH += SoundManager/
INCLUDEPATH += Achievement/
INCLUDEPATH += Options/

INCLUDEPATH += /usr/local/include/OGRE/Overlay
INCLUDEPATH += /usr/local/include/OGRE/Terrain
INCLUDEPATH += /usr/local/include/OGRE/
INCLUDEPATH += /usr/include/OIS/
INCLUDEPATH += /usr/include/xercesc

INCLUDEPATH += /usr/local/include/bullet/
INCLUDEPATH += /usr/local/include/OgreBullet/

INCLUDEPATH += /usr/local/include/CEGUI
INCLUDEPATH += /usr/include/SDL/
INCLUDEPATH += /usr/include/glib-2.0

INCLUDEPATH += /usr/include/AL/

CONFIG += link_pkgconfig
PKGCONFIG += gl OGRE OIS bullet OgreBullet xerces-c

PKGCONFIG += glib-2.0
PKGCONFIG += CEGUI

LIBS += -lstdc++ -lpthread
LIBS += -lopenal -lalut
LIBS += -lSDL
LIBS += -L/usr/local/lib/ -lOgreTerrain
LIBS += -L/usr/local/lib/ -lOgreOverlay
LIBS += -lCEGUIBase -lCEGUIOgreRenderer


SOURCES += main.cpp \
    EasyOgre/EasyCamera.cpp \
    AdvancedOgreFramework/DotSceneLoader.cpp \
    AdvancedOgreFramework/AppStateManager.cpp \
    AdvancedOgreFramework/AdvancedOgreFramework.cpp \
    States/PauseState.cpp \
    States/MenuState.cpp \
    States/GameState.cpp \
    Blood_Metal.cpp \
    Level/Level.cpp \
    Level/LevelManager.cpp \
    Level/SquareType.cpp \
    Character/Character.cpp \
    States/TutorialState.cpp \
    States/OpcionesState.cpp \
    States/LogrosState.cpp \
    States/IntroState.cpp \
    States/AcercaState.cpp \
    EasyOgre/EasyInput.cpp \
    EasyOgre/EasyTimer.cpp \
    Level/Square.cpp \
    Explosion/Explosion.cpp \
    Explosion/ExplosionManager.cpp \
    Character/CharacterManager.cpp \
    Object3D/Object3D.cpp \
    Physics/PhysicsManager.cpp \
    Object3D/Object3dManager.cpp \
    Level/SquareManager.cpp \
    Character/CharacterBiped.cpp \
    Character/CharacterTerrestrial.cpp \
    Character/CharacterAerial.cpp \
    States/ConfigState.cpp \
    Weapon/Weapon.cpp \
    Weapon/Shot.cpp \
    States/MuerteState.cpp \
    States/EnhorabuenaState.cpp \
    Enemy/Enemy.cpp \
    Enemy/EnemyStates/StateManager.cpp \
    Enemy/EnemyStates/RutaStateIzq.cpp \
    Enemy/EnemyStates/RutaStateDer.cpp \
    Enemy/EnemyStates/MirarIzqState.cpp \
    Enemy/EnemyStates/MirarDerState.cpp \
    Enemy/EnemyStates/AtacarState.cpp \
    Level/PowerUp.cpp \
    Level/CheckPoint.cpp \
    SoundManager/SoundManager.cpp \
    Enemy/EnemyStates/BuscarState.cpp \
    Player/Player.cpp \
    Achievement/Achievement.cpp \
    Options/Options.cpp

HEADERS += \
    States/PlayState.h \
    EasyOgre/GameState.h \
    EasyOgre/EasyCamera.h \
    AdvancedOgreFramework/rapidxml.hpp \
    AdvancedOgreFramework/DotSceneLoader.hpp \
    AdvancedOgreFramework/AppStateManager.hpp \
    AdvancedOgreFramework/AppState.hpp \
    AdvancedOgreFramework/AdvancedOgreFramework.hpp \
    States/PauseState.hpp \
    States/MenuState.hpp \
    States/GameState.hpp \
    Blood_Metal.h \
    Level/Level.h \
    Level/LevelManager.h \
    Level/SquareType.h \
    Character/Character.h \
    States/TutorialState.hpp \
    States/OpcionesState.hpp \
    States/LogrosState.hpp \
    States/IntroState.hpp \
    States/AcercaState.hpp \
    EasyOgre/EasyInput.h \
    EasyOgre/EasyTimer.h \
    Level/Square.h \
    Explosion/Explosion.h \
    Explosion/ExplosionManager.h \
    Character/CharacterManager.h \
    Object3D/Object3D.h \
    Physics/PhysicsManager.h \
    Physics/CollisionClass.h \
    Object3D/Object3dManager.h \
    Level/SquareManager.h \
    Character/CharacterBiped.h \
    Character/CharacterTerrestrial.h \
    Character/CharacterAerial.h \
    States/ConfigState.hpp \
    Weapon/Weapon.h \
    Weapon/Shot.h \
    States/MuerteState.hpp \
    States/EnhorabuenaState.hpp \
    Enemy/Enemy.h \
    Enemy/EnemyStates/StateManager.h \
    Enemy/EnemyStates/RutaStateIzq.h \
    Enemy/EnemyStates/RutaStateDer.h \
    Enemy/EnemyStates/MirarIzqState.h \
    Enemy/EnemyStates/MirarDerState.h \
    Enemy/EnemyStates/EnemyState.h \
    Enemy/EnemyStates/AtacarState.h \
    Level/PowerUp.h \
    Level/CheckPoint.h \
    SoundManager/SoundManager.h \
    Enemy/EnemyStates/BuscarState.h \
    Player/Player.h \
    Achievement/Achievement.h \
    Options/Options.h \
    EasyOgre/Xbox360.h \
    Achievement/StructAchievement.h

