#include "Square.h"

Square::Square()
{
}

Square::~Square()
{
//    std::cout << "Destroying square" << std::endl;
    delete _object3D;
}

Square::Square(SquareType *squareType,
               const int x,
               const int y,
               const int z,
               bool physicalBody,
               Ogre::SceneManager *sceneManager,
               PhysicsManager *physicsManager)
{
    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    _squareType = squareType;
    _physicalBody = physicalBody;

    _rotation = Ogre::Quaternion(Ogre::Degree(squareType->getRotation()), Ogre::Vector3(0,1,0));


    if(_physicalBody){
        // Square with physical body

//        if(_squareType->getScale() >= SMALLEST_SCALE){

            _object3D = new Object3D(_squareType->getName(),
                                     Ogre::Vector3(x, y, z),
                                     _rotation,
                                     _squareType->getRestitution(),
                                     _squareType->getFriction(),
                                     _squareType->getMass(),
                                     TRIMESH,
                                     _sceneManager,
                                     _physicsManager);

            // Collision Manager
            if(_squareType->getDestructible()){
                this->setCollisionType(CollisionClass::DESTRUCTIBLE);
            }else{
                this->setCollisionType(CollisionClass::INDESTRUCTIBLE);
            }

            _object3D->getRigidBody()->getBulletRigidBody()->setUserPointer(this);
            _object3D->getRigidBody()->getBulletRigidBody()->setCollisionFlags(_object3D->getRigidBody()->getBulletRigidBody()->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);

//        }

    }else{
        // Square without physical body

        _object3D = new Object3D(_squareType->getName(),
                                 Ogre::Vector3(x, y, z),
                                 _rotation,
                                 _sceneManager);
    }


}

void Square::getHit()
{
    std::cout << "Square " << this->_object3D->getNode()->getName() << " golpeado" << std::endl;
    if(this->_squareType->getDestructible()){
        this->dissolve();
    }
}

void Square::setCollisionType(int type)
{
    this->CollisionType = type;
}

int Square::getCollisionType()
{
    return this->CollisionType;
}

void Square::dissolve()
{
//    _squareType = new SquareType(_squareType->getID(),
//                                 _squareType->getName(),
//                                 _squareType->getMaterial(),
//                                 _squareType->getDestructible(),
//                                 _squareType->getScale()/DIVIDE_RATE,
//                                 _squareType->getRotation(),
//                                 _squareType->getRestitution(),
//                                 _squareType->getFriction(),
//                                 90);

//    // Create 8 smaller

//    Ogre::Vector3 position = _object3D->getNode()->getPosition();
//    Square* square1 = new Square (_squareType,
//                                  position.x - _squareType->getScale(),
//                                  position.y + _squareType->getScale(),
//                                  position.z + _squareType->getScale(),
//                                  true,
//                                  _sceneManager,
//                                  _physicsManager);

//    Square* square2 = new Square (_squareType,
//                                  position.x + _squareType->getScale(),
//                                  position.y + _squareType->getScale(),
//                                  position.z + _squareType->getScale(),
//                                  true,
//                                  _sceneManager,
//                                  _physicsManager);

//    Square* square3 = new Square (_squareType,
//                                  position.x - _squareType->getScale(),
//                                  position.y - _squareType->getScale(),
//                                  position.z + _squareType->getScale(),
//                                  true,
//                                  _sceneManager,
//                                  _physicsManager);

//    Square* square4 = new Square (_squareType,
//                                  position.x + _squareType->getScale(),
//                                  position.y - _squareType->getScale(),
//                                  position.z + _squareType->getScale(),
//                                  true,
//                                  _sceneManager,
//                                  _physicsManager);

//    Square* square5 = new Square (_squareType,
//                                  position.x - _squareType->getScale(),
//                                  position.y + _squareType->getScale(),
//                                  position.z - _squareType->getScale(),
//                                  true,
//                                  _sceneManager,
//                                  _physicsManager);

//    Square* square6 = new Square (_squareType,
//                                  position.x + _squareType->getScale(),
//                                  position.y + _squareType->getScale(),
//                                  position.z - _squareType->getScale(),
//                                  true,
//                                  _sceneManager,
//                                  _physicsManager);

//    Square* square7 = new Square (_squareType,
//                                  position.x - _squareType->getScale(),
//                                  position.y - _squareType->getScale(),
//                                  position.z - _squareType->getScale(),
//                                  true,
//                                  _sceneManager,
//                                  _physicsManager);

//    Square* square8 = new Square (_squareType,
//                                  position.x + _squareType->getScale(),
//                                  position.y - _squareType->getScale(),
//                                  position.z - _squareType->getScale(),
//                                  true,
//                                  _sceneManager,
//                                  _physicsManager);


    delete _object3D;

}
