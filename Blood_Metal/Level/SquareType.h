#ifndef SQUARETYPE_H
#define SQUARETYPE_H

#include <OGRE/OgreString.h>


class SquareType
{
public:
    SquareType();
    SquareType(const int id,
               const Ogre::String name,
               const bool destructible,
               const int rotation,
               const float restitution,
               const float friction,
               const float mass);

    ~SquareType();

    int getID();
    Ogre::String getName();
    bool getDestructible();
    float getRestitution();
    float getFriction();
    float getMass();
    int getRotation();

private:

    int _id;
    Ogre::String _name;
    bool _destructible;
    float _restitution;
    float _friction;
    float _mass;
    int _rotation;

};

#endif // SQUARETYPE_H
