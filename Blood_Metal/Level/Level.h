#ifndef LEVEL_H
#define LEVEL_H

#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreString.h>

#include <vector>
#include <cstdlib>

#include "Level/Square.h"
#include "Level/SquareType.h"
#include "Level/PowerUp.h"
#include "Level/CheckPoint.h"

#include "PhysicsManager.h"
#include "CharacterTerrestrial.h"
#include "Enemy/EnemyStates/StateManager.h"
#include "Enemy.h"

class Level
{
public:
    Level(const Ogre::String &name,
          Ogre::SceneManager* sceneManager,
          PhysicsManager* physicsManager);
    ~Level();

    void addLayerObject(SquareType* squareType,
                        const int x,
                        const int y,
                        const int z,
                        bool physicalBody);

    void addEnemyLayerObject(SquareType* squareType,
                             const int x,
                             const int y,
                             const int z);

    void addPowerUPLayerObject(SquareType* squareType,
                               const int x,
                               const int y,
                               const int z);

    void addCheckPoint(SquareType* squareType,
                       const int x,
                       const int y,
                       const int z);

    void addPlayerPosition(const int x,
                           const int y,
                           const int z);

    void setLevelLimit(const int limitLeft,
                       const int limitRght,
                       const int limitBelow);

    bool outOfLimits(const Ogre::Vector3 position);


    const Ogre::Vector3 getPlayerPosition();

    std::vector<Enemy*> getEnemies();

protected:
    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physicsManager;

    Ogre::String _name;

    std::vector<Square*> _squares;
    std::vector<PowerUP*> _powerUps;
    std::vector<CheckPoint*> _checkPoints;
    std::vector<Enemy*> _enemies;
    std::vector<Ogre::String> _enemyTextures;
    int _randomTexture;
    int enemies_id;

    Ogre::Vector3 _playerPosition;
    int _limitLeft;
    int _limitRight;
    int _limitBelow;

};

#endif // LEVEL_H
