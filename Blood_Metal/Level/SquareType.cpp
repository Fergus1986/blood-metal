#include "SquareType.h"

SquareType::SquareType()
{
}

SquareType::SquareType(const int id,
                       const Ogre::String name,
                       const bool destructible,
                       const int rotation,
                       const float restitution,
                       const float friction,
                       const float mass)
{
    _id = id;
    _name = name;
    _destructible = destructible;
    this->_rotation = rotation;
    this->_restitution = restitution;
    this->_friction = friction;
    this->_mass= mass;
}

SquareType::~SquareType()
{
}

int SquareType::getID()
{
    return this->_id;
}

Ogre::String SquareType::getName()
{
    return this->_name;
}

bool SquareType::getDestructible()
{
    return this->_destructible;
}

float SquareType::getRestitution()
{
    return this->_restitution;
}

float SquareType::getFriction()
{
    return this->_friction;
}

float SquareType::getMass()
{
    return this->_mass;
}

int SquareType::getRotation()
{
    return this->_rotation;
}


