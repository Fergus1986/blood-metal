#include "CheckPoint.h"

#include "Level/SquareType.h"



CheckPoint::CheckPoint()
{
}

CheckPoint::~CheckPoint()
{
    delete _object3D;
}

CheckPoint::CheckPoint(SquareType *squareType,
                       const int x,
                       const int y,
                       const int z,
                       Ogre::SceneManager *sceneManager,
                       PhysicsManager *physicsManager)
{
    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    _squareType = squareType;

    _object3D = new Object3D(_squareType->getName(),
                             Ogre::Vector3(x, y, z),
                             Ogre::Quaternion(Ogre::Degree(squareType->getRotation()), Ogre::Vector3(0,1,0)),
                             _squareType->getRestitution(),
                             _squareType->getFriction(),
                             _squareType->getMass(),
                             TRIMESH,
                             _sceneManager,
                             _physicsManager);

    _checked = false;

    // Collision Manager
    if(_squareType->getName() == "start"){
        this->setCollisionType(CollisionClass::START);

    }else if(_squareType->getName() == "finish"){
        this->setCollisionType(CollisionClass::FINISH);

    }

    _object3D->getRigidBody()->getBulletRigidBody()->setUserPointer(this);
    _object3D->getRigidBody()->getBulletRigidBody()->setCollisionFlags(_object3D->getRigidBody()->getBulletRigidBody()->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);

}

void CheckPoint::Check(bool check)
{
    _checked = check;
}

bool CheckPoint::Checked()
{
    return this->_checked;
}

void CheckPoint::getHit()
{
    delete _object3D;
}

void CheckPoint::setCollisionType(int type)
{
    this->CollisionType = type;
}

int CheckPoint::getCollisionType()
{
    return this->CollisionType;

}
