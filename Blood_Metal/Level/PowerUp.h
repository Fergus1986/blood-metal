#ifndef POWERUP_H
#define POWERUP_H

#include <OGRE/OgreSceneManager.h>

#include "Physics/CollisionClass.h"
#include "Physics/PhysicsManager.h"
#include "Object3D/Object3D.h"

#include "SquareType.h"

class PowerUP : public CollisionClass
{
public:
    PowerUP();
    ~PowerUP();
    PowerUP(SquareType *squareType,
            const int x,
            const int y,
            const int z,
            Ogre::SceneManager *sceneManager,
            PhysicsManager *physicsManager);

    // CollisionClass
    void getHit();
    void setCollisionType(int type);
    int getCollisionType();

protected:
    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physicsManager;

    SquareType* _squareType;
    Object3D * _object3D;
    bool _destroyed;
};

#endif // POWERUP_H
