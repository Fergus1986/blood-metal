#include "Level.h"

Level::Level(const String &name,
             SceneManager *sceneManager,
             PhysicsManager *physicsManager)
{
    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    _name = name;
    this->enemies_id=0;

    _enemyTextures.push_back("01");
    _enemyTextures.push_back("02");
    _enemyTextures.push_back("03");
    _enemyTextures.push_back("04");

}

Level::~Level()
{
    std::cout << "Destroying level" << std::endl;

    for (int i = 0; i < _squares.size(); i++ ) {
        delete _squares[i];
    }
    _squares.clear();
    std::cout << "Squares erased" << std::endl;

    for (int i = 0; i < _checkPoints.size(); i++ ) {
        delete _checkPoints[i];
    }
    _checkPoints.clear();
    std::cout << "CheckPoints erased" << std::endl;

    for (int i = 0; i < _powerUps.size(); i++ ) {
        delete _powerUps[i];
    }
    _powerUps.clear();
    std::cout << "PowerUps erased" << std::endl;

    StateManager::getSingletonPtr()->ClearEnemies();
    _enemies.clear();

    std::cout << "Enemies erased" << std::endl;
}

void Level::addLayerObject(SquareType *squareType,
                           const int x,
                           const int y,
                           const int z,
                           bool physicalBody)
{
    Square* square = new Square(squareType,
                                x,
                                y,
                                z,
                                physicalBody,
                                _sceneManager,
                                _physicsManager);

    _squares.push_back(square);
}

void Level::addEnemyLayerObject(SquareType *squareType, const int x, const int y, const int z)
{
    Enemy* enemy = new Enemy (this->enemies_id++,
                              squareType->getName(),
                              Ogre::Vector3(x,y,z),
                              1000,
                              1,
                              100,
                              2,
                              1,
                              _sceneManager,
                              _physicsManager);

    _randomTexture = rand() % _enemyTextures.size();
    enemy->changeTexture(_enemyTextures.at(_randomTexture));

    this->_enemies.push_back(enemy);
}

void Level::addPowerUPLayerObject(SquareType *squareType, const int x, const int y, const int z)
{
    PowerUP* powerUP = new PowerUP(squareType,
                                   x,
                                   y,
                                   z,
                                   _sceneManager,
                                   _physicsManager);

    _powerUps.push_back(powerUP);
}

void Level::addCheckPoint(SquareType *squareType, const int x, const int y, const int z)
{
    CheckPoint* checkPoint = new CheckPoint(squareType,
                                            x,
                                            y,
                                            z,
                                            _sceneManager,
                                            _physicsManager);

    _checkPoints.push_back(checkPoint);
}

void Level::addPlayerPosition(const int x, const int y, const int z)
{
    _playerPosition = Ogre::Vector3(x, y, z);

}

void Level::setLevelLimit(const int limitLeft,
                          const int limitRght,
                          const int limitBelow)
{
    _limitLeft = limitLeft;
    _limitRight = limitRght;
    _limitBelow = limitBelow;
}

bool Level::outOfLimits(const Vector3 position)
{
    bool out = false;

    if(position.x < _limitLeft || position.x > _limitRight || position.y < _limitBelow){
        out = true;
    }

    return out;
}

const Vector3 Level::getPlayerPosition()
{
    return this->_playerPosition;
}

std::vector<Enemy*> Level::getEnemies()
{
    return this->_enemies;
}

