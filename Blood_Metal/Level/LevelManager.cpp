#include "LevelManager.h"

LevelManager::LevelManager(SceneManager *sceneManager, PhysicsManager *physicsManager)
{
    _sceneManager = sceneManager;
    _physicsManager = physicsManager;
}

LevelManager::~LevelManager()
{
}

Level *LevelManager::loadLevel(const String &name)
{
    _playerLayerID.clear();
    _checkPointLayerID.clear();
    _enemyLayerID.clear();
    _powerUPLayerID.clear();
    _columnLayerID.clear();
    _groundLayerID.clear();
    _backLayerID.clear();

    _level = new Level(name, _sceneManager, _physicsManager);
    loadFile(name);

    createGroundLayer();
    std::cout << "GroundLayer built" << std::endl;
    createPlayerLayer();
    std::cout << "PlayerLayer built" << std::endl;
    createCheckPointLayer();
    std::cout << "CheckPointLayer built" << std::endl;
    createColumnLayer();
    std::cout << "ColumnLayer built" << std::endl;
    createEnemyLayer();
    std::cout << "EnemyLayer built" << std::endl;
    createPowerUPLayer();
    std::cout << "PowerUpLayer built" << std::endl;
    createLimitLevel();
    std::cout << "LevelLimit built" << std::endl;

    return _level;
}

void LevelManager::destroyLevel()
{
    delete _level;
}

void LevelManager::loadFile(const String &name)
{
    cout << "Reading file... " << name << endl;
    // Initialiation
    try {
        XMLPlatformUtils::Initialize();
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        cerr << "Error in initialization! :\n"
             << message << "\n";
        XMLString::release(&message);
        return;
    }

    XercesDOMParser* parser = new XercesDOMParser();
    parser->setValidationScheme(XercesDOMParser::Val_Always);

    // Parsing the xml file
    try {
        parser->parse(name.c_str());
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        cout << "Exception: \n" << message << endl;
        XMLString::release(&message);
    }
    catch (const DOMException& toCatch) {
        char* message = XMLString::transcode(toCatch.msg);
        cout << "Exception : \n" << message << endl;
        XMLString::release(&message);
    }
    catch (...) {
        cout << "Exception!" << endl;
        return;
    }

    DOMDocument* xmlDoc;
    DOMElement* elementRoot;

    try {
        // Root element
        xmlDoc = parser->getDocument();
        elementRoot = xmlDoc->getDocumentElement();

        if(!elementRoot)
            throw(std::runtime_error("XML document empty"));

    }
    catch (xercesc::XMLException& e ) {
        char* message = xercesc::XMLString::transcode( e.getMessage() );
        ostringstream errBuf;
        errBuf << "Error while parsing: " << message << flush;
        XMLString::release( &message );
        return;
    }

    // Map properties
    DOMNamedNodeMap* attributes = elementRoot->getAttributes();
    DOMNode* width = attributes->getNamedItem(XMLString::transcode("width"));
    DOMNode* height = attributes->getNamedItem(XMLString::transcode("height"));

    _levelWidth = atoi(XMLString::transcode(width->getNodeValue()));
    _levelHeight = atoi(XMLString::transcode(height->getNodeValue()));

    XMLCh* properties = XMLString::transcode("properties");
    XMLCh* scale = XMLString::transcode("scale");
    XMLCh* square = XMLString::transcode("tileset");
    XMLCh* layer = XMLString::transcode("layer");

    // Processing nodes from root node
    for (XMLSize_t i = 0; i < elementRoot->getChildNodes()->getLength(); ++i ) {

        DOMNode* node = elementRoot->getChildNodes()->item(i);

        if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
            // Node <properties>?
            if (XMLString::equals(node->getNodeName(), properties)){
                for (XMLSize_t j = 0; j < node->getChildNodes()->getLength(); ++j) {
                    DOMNode* nodeProperty = node->getChildNodes()->item(j);
                    // Node <property>?
                    if (nodeProperty->getNodeType() == DOMNode::ELEMENT_NODE){
                        DOMNamedNodeMap* attributesProperty = nodeProperty->getAttributes();
                        DOMNode* nameProperty = attributesProperty->getNamedItem(XMLString::transcode("name"));
                        DOMNode* valueProperty = attributesProperty->getNamedItem(XMLString::transcode("value"));

                        // Scale
                        if (XMLString::equals(nameProperty->getNodeValue(), scale)){
                            _levelScale = atoi(XMLString::transcode(valueProperty->getNodeValue()));
                        }
                    }
                }
                // Node <properties>?
            }else if (XMLString::equals(node->getNodeName(), square)){

                // Parser square
                parserSquare(node);
            }else{
                // Node <layer>?
                if (XMLString::equals(node->getNodeName(), layer)){
                    // Parser layer
                    parserLayer(node);
                }
            }
        }
    }

    // Freeing resources
    XMLString::release(&square);
    XMLString::release(&layer);

    delete parser;
}

void LevelManager::parserLayer(const DOMNode *layer)
{
    // Layer Attributes
    DOMNamedNodeMap* attributes = layer->getAttributes();

    DOMNode* name = attributes->getNamedItem(XMLString::transcode("name"));
    Ogre::String layerName = (XMLString::transcode(name->getNodeValue()));

    XMLCh* data_ch = XMLString::transcode("data");
    XMLCh* title_ch = XMLString::transcode("tile");

    for (XMLSize_t i = 0; i < layer->getChildNodes()->getLength(); ++i ) {

        DOMNode* nodeData = layer->getChildNodes()->item(i);
        if (nodeData->getNodeType() == DOMNode::ELEMENT_NODE) {
            // Nodo <data>?
            if (XMLString::equals(nodeData->getNodeName(), data_ch)){
                for (XMLSize_t j = 0; j < nodeData->getChildNodes()->getLength(); ++j ) {

                    DOMNode* nodeTile = nodeData->getChildNodes()->item(j);
                    if (nodeTile->getNodeType() == DOMNode::ELEMENT_NODE) {
                        // Nodo <tile>?
                        if (XMLString::equals(nodeTile->getNodeName(), title_ch)){
                            // Tile attributes
                            DOMNamedNodeMap* TileAttributes = nodeTile->getAttributes();
                            DOMNode* tile = TileAttributes->getNamedItem(XMLString::transcode("gid"));
                            int square = atoi(XMLString::transcode(tile->getNodeValue()));

                            if(layerName.compare("playerLayer")==0){
                                _playerLayerID.push_back(square);

                            }else if(layerName.compare("checkPointLayer")==0){
                                _checkPointLayerID.push_back(square);

                            }else if(layerName.compare("enemyLayer")==0){
                                _enemyLayerID.push_back(square);

                            }else if(layerName.compare("powerUPLayer")==0){
                                _powerUPLayerID.push_back(square);

                            }else if(layerName.compare("columnLayer")==0){
                                _columnLayerID.push_back(square);

                            }else if(layerName.compare("groundLayer")==0){
                                _groundLayerID.push_back(square);

                            }else if(layerName.compare("backLayer")==0){
                                _backLayerID.push_back(square);

                            }
                        }
                    }
                }
            }
        }
    }
}

void LevelManager::parserSquare(const DOMNode *square)
{
    // Square Attributes
    int squareID;
    Ogre::String squareName;
    //    Ogre::String squareMaterial;
    float squareFriction;
    float squareMass;
    float squareRestitution;
    int squareRotation;
    //    float squareScale;
    bool squareDestructible;

    DOMNamedNodeMap* attributesSquare = square->getAttributes();
    DOMNode* id = attributesSquare->getNamedItem(XMLString::transcode("firstgid"));
    DOMNode* name = attributesSquare->getNamedItem(XMLString::transcode("name"));

    squareID = atoi(XMLString::transcode(id->getNodeValue()));
    squareName = XMLString::transcode(name->getNodeValue());

    XMLCh* properties = XMLString::transcode("properties");
    //    XMLCh* material = XMLString::transcode("material");
    XMLCh* friction = XMLString::transcode("friction");
    //    XMLCh* scale = XMLString::transcode("scale");
    XMLCh* mass = XMLString::transcode("mass");
    XMLCh* restitution = XMLString::transcode("restitution");
    XMLCh* rotation = XMLString::transcode("rotation");
    XMLCh* destructible = XMLString::transcode("destructible");



    // Property Attributes
    for (XMLSize_t i = 0; i < square->getChildNodes()->getLength(); ++i ) {

        DOMNode* node = square->getChildNodes()->item(i);
        // Node <properties>?
        if (node->getNodeType() == DOMNode::ELEMENT_NODE){
            if (XMLString::equals(node->getNodeName(), properties)){
                for (XMLSize_t j = 0; j < node->getChildNodes()->getLength(); ++j) {
                    DOMNode* nodeProperty = node->getChildNodes()->item(j);
                    // Node <property>?
                    if (nodeProperty->getNodeType() == DOMNode::ELEMENT_NODE){
                        DOMNamedNodeMap* attributesProperty = nodeProperty->getAttributes();
                        DOMNode* nameProperty = attributesProperty->getNamedItem(XMLString::transcode("name"));
                        DOMNode* valueProperty = attributesProperty->getNamedItem(XMLString::transcode("value"));

                        // Friction
                        if (XMLString::equals(nameProperty->getNodeValue(), friction)){
                            squareFriction = atof(XMLString::transcode(valueProperty->getNodeValue()));
                        }
                        // Mass
                        else if (XMLString::equals(nameProperty->getNodeValue(), mass)){
                            squareMass = atof(XMLString::transcode(valueProperty->getNodeValue()));
                        }

                        // Restitution
                        else if (XMLString::equals(nameProperty->getNodeValue(), restitution)){
                            squareRestitution = atof(XMLString::transcode(valueProperty->getNodeValue()));
                        }
                        // Rotation
                        else if (XMLString::equals(nameProperty->getNodeValue(), rotation)){
                            squareRotation = atof(XMLString::transcode(valueProperty->getNodeValue()));
                        }
                        // Destructible
                        else if (XMLString::equals(nameProperty->getNodeValue(), destructible)){
                            squareDestructible = atoi(XMLString::transcode(valueProperty->getNodeValue())) != 0;
                        }
                    }
                }
            }
        }
    }

    // SquareType
    SquareType* squareType = new SquareType(squareID,
                                            squareName,
                                            squareDestructible,
                                            squareRotation,
                                            squareRestitution,
                                            squareFriction,
                                            squareMass);
    _squareTypes.push_back(squareType);
}

void LevelManager::createPlayerLayer()
{
    int squareIndex=-1;
    int squareType=-1;

    for(int x=0; x<_levelWidth; x++){
        for(int y = 0; y<_levelHeight; y++){
            squareIndex = (y * _levelWidth) + x;
            squareType = _playerLayerID.at(squareIndex) -1;

            if(squareType < 0){
                // Empty Space
            }else{

                _level->addPlayerPosition(_levelScale * x,
                                          -_levelScale * y,
                                          _levelScale);
            }
        }
    }
}

void LevelManager::createCheckPointLayer()
{
    int squareIndex=-1;
    int squareType=-1;

    for(int x=0; x<_levelWidth; x++){
        for(int y = 0; y<_levelHeight; y++){
            squareIndex = (y * _levelWidth) + x;
            squareType = _checkPointLayerID.at(squareIndex) -1;

            if(squareType < 0){
                // Empty Space
            }else{

                _level->addCheckPoint(_squareTypes.at(squareType),
                                      _levelScale * x,
                                      -_levelScale * y,
                                      _levelScale);
            }
        }
    }
}

void LevelManager::createEnemyLayer()
{
    int squareIndex=-1;
    int squareType=-1;

    for(int x=0; x<_levelWidth; x++){
        for(int y = 0; y<_levelHeight; y++){
            squareIndex = (y * _levelWidth) + x;
            squareType = _enemyLayerID.at(squareIndex) -1;

            if(squareType < 0){
                // Empty Space
            }else{

                _level->addEnemyLayerObject(_squareTypes.at(squareType),
                                            _levelScale * x,
                                            -_levelScale * y,
                                            _levelScale);
            }
        }
    }
}

void LevelManager::createPowerUPLayer()
{
    int squareIndex=-1;
    int squareType=-1;

    for(int x=0; x<_levelWidth; x++){
        for(int y = 0; y<_levelHeight; y++){
            squareIndex = (y * _levelWidth) + x;
            squareType = _powerUPLayerID.at(squareIndex) -1;

            if(squareType < 0){
                // Empty Space
            }else{

                _level->addPowerUPLayerObject(_squareTypes.at(squareType),
                                              _levelScale * x,
                                              -_levelScale * y,
                                              _levelScale);
            }
        }
    }
}

void LevelManager::createColumnLayer()
{
    int squareIndex=-1;
    int squareType=-1;

    for(int x=0; x<_levelWidth; x++){
        for(int y = 0; y<_levelHeight; y++){
            squareIndex = (y * _levelWidth) + x;
            squareType = _columnLayerID.at(squareIndex) -1;

            if(squareType < 0){
                // Empty Space
            }else{

                _level->addLayerObject(_squareTypes.at(squareType),
                                       _levelScale * x,
                                       -_levelScale * y,
                                       0,
                                       false);
            }
        }
    }
}

void LevelManager::createGroundLayer()
{
    int squareIndex=-1;
    int squareType=-1;

    for(int x=0; x<_levelWidth; x++){
        for(int y = 0; y<_levelHeight; y++){
            squareIndex = (y * _levelWidth) + x;
            squareType = _groundLayerID.at(squareIndex) -1;

            if(squareType < 0){
                // Empty Space
            }else{

                for(int z=0; z<3; z++){

                    switch(z){

                    case 0:
                        _level->addLayerObject(_squareTypes.at(squareType),
                                               _levelScale * x,
                                               -_levelScale * y,
                                               _levelScale * z,
                                               false);
                        break;

                    case 1:
                        _level->addLayerObject(_squareTypes.at(squareType),
                                               _levelScale * x,
                                               -_levelScale * y,
                                               _levelScale * z,
                                               true);
                        break;


                    case 2:
                        _level->addLayerObject(_squareTypes.at(squareType),
                                               _levelScale * x,
                                               -_levelScale * y,
                                               _levelScale * z,
                                               false);
                        break;

                    default:
                        _level->addLayerObject(_squareTypes.at(squareType),
                                               _levelScale * x,
                                               -_levelScale * y,
                                               _levelScale * z,
                                               false);
                        break;
                    }
                }
            }
        }
    }
}

void LevelManager::createFrontLayer()
{
}

void LevelManager::createLimitLevel()
{
    _level->setLevelLimit(-_levelScale,
                          ((_levelWidth * _levelScale) + _levelScale),
                          ((-_levelHeight * _levelScale) - _levelScale));

}
