#ifndef CHECKPOINT_H
#define CHECKPOINT_H

#include <OGRE/OgreSceneManager.h>

#include "Physics/CollisionClass.h"
#include "Physics/PhysicsManager.h"
#include "Object3D/Object3D.h"

#include "SquareType.h"

class CheckPoint: public CollisionClass
{
public:
    CheckPoint();
    ~CheckPoint();
    CheckPoint(SquareType *squareType,
               const int x,
               const int y,
               const int z,
               Ogre::SceneManager *sceneManager,
               PhysicsManager *physicsManager);

    void Check(bool check);
    bool Checked();

    // CollisionClass
    void getHit();
    void setCollisionType(int type);
    int getCollisionType();

protected:
    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physicsManager;

    SquareType* _squareType;
    Object3D * _object3D;

    bool _checked;
};

#endif // CHECKPOINT_H
