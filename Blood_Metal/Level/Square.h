#ifndef SQUARE_H
#define SQUARE_H

#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreString.h>

#include "CollisionClass.h"
#include "PhysicsManager.h"
#include "SquareType.h"
#include "Object3D.h"

#define SMALLEST_SCALE 1
#define DIVIDE_RATE 2.0

using namespace Ogre;
using namespace std;


class Square : public CollisionClass
{
public:
    Square();
    ~Square();
    Square(SquareType * squareType,
           const int x,
           const int y,
           const int z,
           bool physicalBody,
           Ogre::SceneManager* sceneManager,
           PhysicsManager* physicsManager);

    // CollisionClass
    void getHit();
    void setCollisionType(int type);
    int getCollisionType();


    void dissolve();


protected:
    SceneManager* _sceneManager;
    PhysicsManager* _physicsManager;


    SquareType* _squareType;
    bool _physicalBody;

    Ogre::Quaternion _rotation;
    Object3D * _object3D;



};

#endif // SQUARE_H
