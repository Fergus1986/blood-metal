#ifndef LEVELMANAGER_H
#define LEVELMANAGER_H

#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreString.h>


#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include "Level.h"
#include "SquareType.h"
#include "PhysicsManager.h"

using namespace Ogre;
using namespace xercesc;
using namespace std;

class LevelManager
{
public:
    LevelManager(Ogre::SceneManager* sceneManager, PhysicsManager* physicsManager);
    ~LevelManager();

    Level* loadLevel(const Ogre::String &name);
    void destroyLevel();

private:
    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physicsManager;

    // Level
    Level* _level;
    int _levelWidth;
    int _levelHeight;
    int _levelScale;
    std::vector<int> _playerLayerID;
    std::vector<int> _checkPointLayerID;
    std::vector<int> _enemyLayerID;
    std::vector<int> _powerUPLayerID;
    std::vector<int> _columnLayerID;
    std::vector<int> _groundLayerID;
    std::vector<int> _backLayerID;

    // Types of Squares
    std::vector<SquareType*> _squareTypes;

    void loadFile(const Ogre::String &name);
    void parserLayer(const xercesc::DOMNode* layer);
    void parserSquare(const xercesc::DOMNode* square);

    void createPlayerLayer();
    void createCheckPointLayer();
    void createEnemyLayer();
    void createPowerUPLayer();
    void createColumnLayer();
    void createGroundLayer();
    void createFrontLayer();
    void createLimitLevel();

};

#endif // LEVELMANAGER_H
