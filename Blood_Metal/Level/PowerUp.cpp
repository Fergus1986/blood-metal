#include "PowerUp.h"

PowerUP::PowerUP()
{
}

PowerUP::~PowerUP()
{
    if(!_destroyed){
        delete _object3D;
    }
}

PowerUP::PowerUP(SquareType *squareType,
                 const int x,
                 const int y,
                 const int z,
                 Ogre::SceneManager *sceneManager,
                 PhysicsManager *physicsManager)
{

    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    _squareType = squareType;

    _object3D = new Object3D(_squareType->getName(),
                             Ogre::Vector3(x, y, z),
                             Ogre::Quaternion::IDENTITY,
                             _squareType->getRestitution(),
                             _squareType->getFriction(),
                             _squareType->getMass(),
                             BOX,
                             _sceneManager,
                             _physicsManager);

    _destroyed = false;

    // Collision Manager

    if(_squareType->getName() == "crate_cannon"){
        this->setCollisionType(CollisionClass::CRATE_CANNON);

    }else if(_squareType->getName() == "crate_shot"){
        this->setCollisionType(CollisionClass::CRATE_SHOT);

    }else if(_squareType->getName() == "crate_points"){
        this->setCollisionType(CollisionClass::CRATE_POINTS);

    }else if (_squareType->getName() == "crate_life"){
        this->setCollisionType(CollisionClass::CRATE_LIFE);
    }

    _object3D->getRigidBody()->getBulletRigidBody()->setUserPointer(this);
    _object3D->getRigidBody()->getBulletRigidBody()->setCollisionFlags(_object3D->getRigidBody()->getBulletRigidBody()->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);


}

void PowerUP::getHit()
{
    delete _object3D;
    _destroyed = true;
}

void PowerUP::setCollisionType(int type)
{
    this->CollisionType = type;
}

int PowerUP::getCollisionType()
{
    return this->CollisionType;
}
