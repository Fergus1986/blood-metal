#ifndef SHOT_H
#define SHOT_H

#include <OGRE/OgreString.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreVector3.h>
#include <OGRE/OgreQuaternion.h>

#include "Object3D.h"
#include "CollisionClass.h"
#include "PhysicsManager.h"

class Shot: public CollisionClass
{
public:
    Shot();
    ~Shot();
    Shot(const Ogre::String name,
         const Ogre::Vector3 origin,
         const Ogre::Quaternion orientation,
         const Ogre::Vector3 velocity,
         Ogre::SceneManager* sceneManager,
         PhysicsManager* physicsManager);

    // CollisionClass
    void getHit();
    void setCollisionType(int type);
    int getCollisionType();


protected:
    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physicsManager;

    Ogre::Vector3 _origin;
    Ogre::Vector3 _position;
    Ogre::Quaternion _orientation;
    Ogre::Vector3 _velocity;

    Object3D* _object3D;

    // Explotion effect
    SDL_TimerID timer_id;
    Ogre::Light* _light;
    Ogre::SceneNode* _explotionNode;
    Ogre::BillboardSet* _bbs;

private:
    void release();
    void blastOn();
    static Uint32 blastOff(Uint32 interval, void *param);


};

#endif // SHOT_H
