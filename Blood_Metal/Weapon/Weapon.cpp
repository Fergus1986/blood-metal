#include "Weapon.h"

Weapon::Weapon()
{
}

Weapon::~Weapon()
{
}

Weapon::Weapon(const Ogre::String name,
               int maxAmmunition,
               int rate,
               int power,
               int velocity,
               Ogre::SceneNode *parentNode,
               Ogre::SceneManager *sceneManager,
               PhysicsManager *physicsManager)
{
    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    _name = name;
    _maxAmmunition = maxAmmunition;
    _ammunition = maxAmmunition;
    _rate = rate;
    _power = power;
    _velocity = velocity;
    _parentNode = parentNode;
    _timerRate = new Ogre::Timer();
    _timerRate->reset();

    // Explotion
    _light = _sceneManager->createLight();
    _light->setPosition(Ogre::Vector3::ZERO);
    _light->setType(Ogre::Light::LT_POINT);
    _light->setDiffuseColour(1.0, 0.0, 0.0);
    _light->setSpecularColour(1.0, 0.0, 0.0);

    _bbs = _sceneManager->createBillboardSet(1);
    _bbs->createBillboard(Ogre::Vector3::ZERO);
    _bbs->setMaterialName("Explotion");
    _bbs->setDefaultDimensions(5,5);
    _bbs->setCommonDirection(Ogre::Vector3(0,1,0));
    // _bbs->setSortingEnabled(true);
    // _bbs->setBillboardType(Ogre::BBT_PERPENDICULAR_SELF);

    _explotionNode = _sceneManager->getRootSceneNode()->createChildSceneNode();
    _explotionNode->attachObject(_bbs);
    _explotionNode->attachObject(_light);
    _explotionNode->setVisible(false);
}


int Weapon::getAmmunition()
{
    return _ammunition;
}

void Weapon::reload(int ammunition)
{
    _ammunition +=ammunition;

    if(_ammunition > _maxAmmunition){
        _ammunition = _maxAmmunition;
    }
}

void Weapon::shoot()
{
    if(_ammunition > 0){

        if(_timerRate->getMilliseconds() > _rate){

            calculateTrajectory();

            Shot* shot = new Shot(_name + "_shot",
                                  _position,
                                  _parentNode->getOrientation(),
                                  _trayectory,
                                  _sceneManager,
                                  _physicsManager);

            blastOn();

            _ammunition --;
            _timerRate->reset();
        }
    }
}

void Weapon::calculateTrajectory()
{
    _position = _parentNode->getPosition();

    if(_parentNode->getOrientation().y >= 0){
        _position.x = _position.x + 2.5;
        _trayectory = Ogre::Vector3(_velocity, -1, 0);

    }else{
        _position.x = _position.x - 2.5;
        _trayectory = Ogre::Vector3(-_velocity, -1, 0);

    }

    _position.y = _position.y + 2;
    _position.z = _position.z + 1;
}

void Weapon::blastOn()
{
    _explotionNode->setPosition(_position);
    _explotionNode->setVisible(true);
    timer_id = SDL_AddTimer(150, this->blastOff, _explotionNode);
}

Uint32 Weapon::blastOff(Uint32 interval, void *param)
{
    Ogre::SceneNode* node = static_cast <Ogre::SceneNode*> (param);
    node->setVisible(false);
    return 0;
}
