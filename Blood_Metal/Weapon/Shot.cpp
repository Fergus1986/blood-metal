#include "Shot.h"

Shot::Shot()
{
}

Shot::~Shot()
{
    delete _object3D;
}

Shot::Shot(const Ogre::String name,
           const Ogre::Vector3 origin,
           const Ogre::Quaternion orientation,
           const Ogre::Vector3 velocity,
           Ogre::SceneManager* sceneManager,
           PhysicsManager* physicsManager){

    _sceneManager = sceneManager;
    _physicsManager = physicsManager;

    _origin = origin;
    _velocity = velocity;

    _orientation = orientation;

    _object3D = new Object3D(name,
                             _origin,
                             _orientation,
                             0.5,
                             5,
                             90,
                             BOX,
                             _sceneManager,
                             _physicsManager);


    // Explotion
    _bbs = _sceneManager->createBillboardSet(1);
    _bbs->createBillboard(Ogre::Vector3::ZERO);
    _bbs->setMaterialName("Explotion2");
    _bbs->setDefaultDimensions(3,3);
    _bbs->setCommonDirection(Ogre::Vector3(0,1,0));

    _explotionNode = _sceneManager->getRootSceneNode()->createChildSceneNode();
    _explotionNode->attachObject(_bbs);
    _explotionNode->setVisible(false);

    // Collisions Manager
    _object3D->getRigidBody()->getBulletRigidBody()->setUserPointer(this);
    this->setCollisionType(CollisionClass::DESTRUCTOR);
    _object3D->getRigidBody()->getBulletRigidBody()->setCollisionFlags(_object3D->getRigidBody()->getBulletRigidBody()->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);

    release();
}

void Shot::getHit()
{
    blastOn();
    delete _object3D;
}

void Shot::setCollisionType(int type)
{
    this->CollisionType = type;
}

int Shot::getCollisionType()
{
    return this->CollisionType;
}

void Shot::release()
{
    // 2D Movement
    _object3D->getRigidBody()->getBulletRigidBody()->setLinearFactor(btVector3(1,1,0));
    _object3D->getRigidBody()->getBulletRigidBody()->setAngularFactor(btVector3(0,0,1));

    // Queda por hacer el calculo de la trayectoria
    _object3D->getRigidBody()->getBulletRigidBody()->applyCentralImpulse(btVector3(_velocity.x * 100, _velocity.y * 100, 0));

}

void Shot::blastOn()
{
    _position = _object3D->getNode()->getPosition();
    _position.z = _position.z + 1;
    _explotionNode->setPosition(_position);
    _explotionNode->setVisible(true);
    timer_id = SDL_AddTimer(150, this->blastOff, _explotionNode);
}

Uint32 Shot::blastOff(Uint32 interval, void *param)
{
    Ogre::SceneNode* node = static_cast <Ogre::SceneNode*> (param);
    node->setVisible(false);
    return 0;
}
