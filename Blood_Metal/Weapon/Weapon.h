#ifndef WEAPON_H
#define WEAPON_H

#include <OGRE/OgreString.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreBillboard.h>
#include <OGRE/OgreTimer.h>
#include <OgreMatrix3.h>
#include <OgreMath.h>

#include <SDL/SDL_timer.h>

#include "PhysicsManager.h"
#include "Shot.h"

class Weapon
{
public:
    Weapon();
    ~Weapon();
    Weapon(const Ogre::String name,
           int ammunition,
           int rate,
           int power,
           int velocity,
           Ogre::SceneNode* parentNode,
           Ogre::SceneManager* sceneManager,
           PhysicsManager* physicsManager);

    int getAmmunition();
    void reload(int ammunition);
    void shoot();

private:
    void calculateTrajectory();
    void blastOn();
    static Uint32 blastOff(Uint32 interval, void *param);


protected:
    Ogre::SceneManager* _sceneManager;
    PhysicsManager* _physicsManager;

    Ogre::String _name;
    int _maxAmmunition;
    int _ammunition;
    int _rate;
    int _power;
    int _velocity;

    Ogre::Vector3 _position;
    Ogre::Vector3 _trayectory;
    Ogre::Radian _angle;
    Ogre::Matrix3 _matrix;

    Ogre::SceneNode * _parentNode;
    Ogre::Timer* _timerRate;

    // Explotion effect
    SDL_TimerID timer_id;
    Ogre::Light* _light;
    Ogre::SceneNode* _explotionNode;
    Ogre::BillboardSet* _bbs;
};

#endif // WEAPON_H
