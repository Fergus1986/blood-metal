#include "EasyTimer.h"

EasyTimer::EasyTimer()
{
    this->tiempoActivo = false;
    this->minuto = 0;
    this->segundo =0;
    tiempo = g_timer_new();
    //se detiene para que no empieze a funcionar en la inicializacion
    this->pararTiempo();
}

EasyTimer::~EasyTimer()
{
    this->pararTiempo();
    g_timer_destroy (tiempo);
}

/*
 * Activa el cronometro hacia delante o hacia atras
 * dependiendo del estado pasado
 */

void EasyTimer::contar(int estado, int m, int s)
{
    this->estado = estado;
    this->minuto = m;
    this->segundo = s;
    this->tiempoActivo = true;
    this->continuarTiempo();
}
bool EasyTimer::getTiempoCorriendo()
{
    if(this->estado==ADELANTE){
            if(this->getMinutosHaciaDelante()>=this->minuto &&
               this->getSegundosHaciaDelante() > this->segundo){
                  //  this->tiempoActivo = false;
                    this->pararTiempo();
            }
        }
        // estado = atras
    else{
             if(this->getMinutosHaciaAtras()==0 && this->getSegundosHaciaAtras()<0){
                // this->tiempoActivo = false;
                 this->pararTiempo();

             }
             //No se encuentra en el minuto 0
             //comprobar si se cambia de minuto
             else if(this->getMinutosHaciaAtras()!=0 && this->getSegundosHaciaAtras()<0){
                this->segundo = 59;
                this->minuto -= 1;
                this->resetearTiempo();
            }

        }


    return this->tiempoActivo;
}

int EasyTimer::getMinuto()
{
    if(this->estado==ADELANTE){
            return this->getMinutosHaciaDelante();
        }
        else{
            return this->getMinutosHaciaAtras();
       }
}

int EasyTimer::getSegundo()
{
    if(this->estado==ADELANTE){
           return this->getSegundosHaciaDelante();
       }
       else{
           return this->getSegundosHaciaAtras();
       }
}

// usarlo para cambiar completamente de estado
void EasyTimer::pararTiempo()
{
    g_timer_stop(tiempo);
    this->tiempoActivo=false;
}
// pausa el tiempo pero el cronometro sigue activo
// usarlo para para pausar el tiempo durante el juego o para ir a pausa
void EasyTimer::pausarTiempo()
{
     g_timer_stop(tiempo);
}

void EasyTimer::continuarTiempo()
{
      g_timer_continue(tiempo);
      this->tiempoActivo = true;
}

void EasyTimer::resetearTiempo()
{
        g_timer_reset(tiempo);
}

std::string EasyTimer::getCadenaTiempo()
{
    std::stringstream msg;
    if(minuto<10) msg << "0";
        if(this->estado==ADELANTE){
            msg << this->getMinutosHaciaDelante() << ":";
            if(this->getSegundosHaciaDelante()<10){
                 msg << "0";
            }
            msg << this->getSegundosHaciaDelante();
         }
         else{
            msg << this->minuto << ":";
            if(this->getSegundosHaciaAtras()<10){
                msg << "0";
            }
            msg << this->getSegundosHaciaAtras();
        }
        return msg.str();
}


int EasyTimer::getMinutosHaciaDelante()
{
    return (int) (g_timer_elapsed(tiempo,0))/60;

}

int EasyTimer::getSegundosHaciaDelante()
{
    return (int)(g_timer_elapsed(tiempo,0))%60;

}

int EasyTimer::getMinutosHaciaAtras()
{
    return this->minuto;

}

int EasyTimer::getSegundosHaciaAtras()
{

     return this->segundo -  (int)(g_timer_elapsed(tiempo,0));
}
