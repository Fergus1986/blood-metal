#ifndef XBOX360_H
#define XBOX360_H

#define XBOX360_JOY_DEATH_ZONE              7000
#define XBOX360_JOY_MAX                     32767
#define XBOX360_JOY_LEFT_VERTICAL           1
#define XBOX360_JOY_LEFT_HORIZONTAL         0
#define XBOX360_JOY_RIGHT_VERTICAL          4
#define XBOX360_JOY_RIGHT_HORIZONTAL        3
#define XBOX360_LEFT_TRIGGER                2
#define XBOX360_RIGHT_TRIGGER               5

#define XBOX360_BUTTON_A                    0
#define XBOX360_BUTTON_B                    1
#define XBOX360_BUTTON_X                    2
#define XBOX360_BUTTON_Y                    3

#define XBOX360_BUTTON_LB                   4
#define XBOX360_BUTTON_RB                   5

#define XBOX360_BUTTON_BACK                 6
#define XBOX360_BUTTON_START                7

#endif // XBOX360_H
