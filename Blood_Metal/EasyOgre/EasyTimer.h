#ifndef EASYTIMER_H
#define EASYTIMER_H

enum{ADELANTE,ATRAS};

#include <glib.h>
#include <iostream>
#include <sstream>

class EasyTimer
{
public:
    EasyTimer();
    ~EasyTimer();

    void contar(int estado, int m, int s);
    bool getTiempoCorriendo();
    int getMinuto();
    int getSegundo();
    void pararTiempo();
    void pausarTiempo();
    void continuarTiempo();
    void resetearTiempo();
    std::string getCadenaTiempo();

    private:
        GTimer* tiempo;
        int estado;
        int minuto;
        int segundo;
        bool tiempoActivo;
        int getMinutosHaciaDelante();
        int getSegundosHaciaDelante();
        int getMinutosHaciaAtras();
        int getSegundosHaciaAtras();

};

#endif // EASYTIMER_H
