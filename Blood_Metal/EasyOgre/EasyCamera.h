#ifndef EASYCAMERA_H
#define EASYCAMERA_H

#include <OGRE/OgreCamera.h>
#include <OGRE/OgreString.h>
#include <OGRE/OgreSceneNode.h>

#include "AdvancedOgreFramework.hpp"

using namespace Ogre;
using namespace std;

class EasyCamera
{
public:
    EasyCamera();
    ~EasyCamera();
    EasyCamera(String name, SceneManager* sceneManager);

    void setTightness(Real tightness);
    Real getTightness();
    Camera* getCamera();
    Vector3 getCameraPosition();
    void instantUpdate(Vector3 cameraPosition, Vector3 targetPosition);
    void update(Vector3 cameraPosition, Vector3 targetPosition);

protected:
    SceneManager* _sceneManager;
    String _name;
    SceneNode* _targetNode;
    SceneNode* _cameraNode;
    Camera* _camera;
    Real _tightness; // Determines the movement of the camera: 1 means tight movement, while 0 means no movement
    // Handlet displacement
    Vector3 _displacement;

};

#endif // EASYCAMERA_H
