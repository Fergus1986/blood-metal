﻿* Blood & Metal

Blood & Metal es un juego de tipo Run'n'gun en 2.5D. Controlas a un soldado que tiene la misión de salvar el mundo.

* COMPILACIÓN E INSTALACIÓN

En el archivo comprimido se encuentra lo siguiente:
- Blood_Metal -> contiene el código fuente, el archivo de configuración del proyecto QtCreator
- build-Blood_Metal-Release -> contiene el ejecutable y el makefile
- modelos -> contiene los recursos utilizados (gráficos, música, niveles, ...)
- doc -> documentación del proyecto

Para realizar una nueva compilación se debe respetar esta jerarquía de carpetas.

* DEPENDENCIAS: 
- OGRE
- OIS
- SDL_Mixer
- Xerces
- bullet
- OgreBullet
- CEGUI
Para su compilación se adjunta un Makefile distinto al sugerido durante el curso, ya que, nosotros hemos realizado el proyecto utilizando qmake, que genera automáticamente su propio Makefile.

* REPOSITORIO PÚBLICO

https://bitbucket.org/Fergus1986/blood-metal
