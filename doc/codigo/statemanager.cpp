void StateManager::UpdateState(double timeSinceLastFrame){ 
    int state;
		//RECORRER VECTOR DE ENEMIGOS
    for(it=enemigos.begin();it!=this->enemigos.end();it++){
				//ACTUALIZAR ESTADO DE ENEMIGO
        state=(*it)->getCurrentState()->update((*it),timeSinceLastFrame);
				//CAMBIAR ESTADO
        switch (state){
        case RUTAIZQ:
            if((*it)->getCurrentState()->getName()!="RutaStateIzq"){
                this->ChangeState(RutaStateIzq::getSingletonPtr(),(*it));
            }
            break;
        case RUTADER:
            if((*it)->getCurrentState()->getName()!="RutaStateDer"){
                this->ChangeState(RutaStateDer::getSingletonPtr(),(*it));
            }
            break;
        case ATACAR:
            if((*it)->getCurrentState()->getName()!="AtacarState"){
                this->ChangeState(AtacarState::getSingletonPtr(),(*it));
            }
            break;
        case MIRARDER:
            if((*it)->getCurrentState()->getName()!="MirarDerState"){
                this->ChangeState(MirarDerState::getSingletonPtr(),(*it));
            }
            break;
        case MIRARIZQ:
            if((*it)->getCurrentState()->getName()!="MirarIzqState"){
                this->ChangeState(MirarIzqState::getSingletonPtr(),(*it));
            }
            break;
        case BUSCAR:
            if((*it)->getCurrentState()->getName()!="BuscarState"){
                this->ChangeState(BuscarState::getSingletonPtr(),(*it));
            }
            break;
        default:
            break;
        }
        (*it)->update(timeSinceLastFrame);
    }
}

