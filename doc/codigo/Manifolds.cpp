for (unsigned int i=0; i<_nManifolds; i++) {

        _contactManifold = _bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);

        _obA = static_cast<btCollisionObject*>(_contactManifold->getBody0());
        _obB = static_cast<btCollisionObject*>(_contactManifold->getBody1());

        if (std::find(_recentContact.begin(), _recentContact.end(), _obA) == _recentContact.end()){
            if (std::find(_recentContact.begin(), _recentContact.end(), _obB) == _recentContact.end()){

                CollisionClass* o1=(CollisionClass*)_obA->getUserPointer();
                CollisionClass* o2=(CollisionClass*)_obB->getUserPointer();

                if( (o1->getCollisionType() == CollisionClass::DESTRUCTOR) ||
                        (o2->getCollisionType() == CollisionClass::DESTRUCTOR)){

                    processDestructionHit(o1, o2);


                }else if ( (o1->getCollisionType() == CollisionClass::PLAYER) ||
                           (o2->getCollisionType() == CollisionClass::PLAYER)){

                    processTriggerHit(o1, o2);

                }
            }
        }
    }
