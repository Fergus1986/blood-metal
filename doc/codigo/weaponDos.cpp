void Weapon::blastOn()
{
    _explotionNode->setPosition(_position);
    _explotionNode->setVisible(true);
    timer_id = SDL_AddTimer(150, this->blastOff, _explotionNode);
}

Uint32 Weapon::blastOff(Uint32 interval, void *param)
{
    Ogre::SceneNode* node = static_cast <Ogre::SceneNode*> (param);
    node->setVisible(false);
    return 0;
} 
