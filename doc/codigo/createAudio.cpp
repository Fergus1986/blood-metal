void MenuState::createAudio()
{
    OgreFramework::getSingletonPtr()->mSoundManager->setListener(
                Ogre::Vector3(0.0f, 0.0f, 4.0f),
                Ogre::Vector3::ZERO,
                Ogre::Vector3::NEGATIVE_UNIT_Z,
                Ogre::Vector3::UNIT_Y);


    OgreFramework::getSingletonPtr()->mSoundManager->setSource(_soundMenu,
                                                               Ogre::Vector3::ZERO,
                                                               Ogre::Vector3::ZERO);

    OgreFramework::getSingletonPtr()->mSoundManager->playAudio(_soundMenu, false);
} 
