    _light = _sceneManager->createLight();
    _light->setPosition(Ogre::Vector3::ZERO);
    _light->setType(Ogre::Light::LT_POINT);
    _light->setDiffuseColour(1.0, 0.0, 0.0);
    _light->setSpecularColour(1.0, 0.0, 0.0);

    _bbs = _sceneManager->createBillboardSet(1);
    _bbs->createBillboard(Ogre::Vector3::ZERO);
    _bbs->setMaterialName("Explotion");
    _bbs->setDefaultDimensions(5,5);
    _bbs->setCommonDirection(Ogre::Vector3(0,1,0));

    _explotionNode = _sceneManager->getRootSceneNode()->createChildSceneNode();
    _explotionNode->attachObject(_bbs);
    _explotionNode->attachObject(_light);
    _explotionNode->setVisible(false); 
