void OgreFramework::initCegui()
{
    m_renderer = &CEGUI::OgreRenderer::bootstrapSystem();
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
    CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
    CEGUI::System::getSingleton().setDefaultFont("fuenteCtrl_freak");
    CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook","MouseArrow");
} 
