void AppStateManager::start(AppState* state)
{
  changeAppState(state);
  double timeSinceLastFrame = 1.0;
  double startTime = 0.0;
  while(!m_bShutdown){
    if(OgreFramework::getSingletonPtr()->m_pRenderWnd->isClosed())m_bShutdown = true;
    if(OgreFramework::getSingletonPtr()->m_pRenderWnd->isActive()){
      startTime = OgreFramework::getSingletonPtr()->m_pTimer->getMilliseconds()/1000.0;
      OgreFramework::getSingletonPtr()->m_pKeyboard->capture();
      OgreFramework::getSingletonPtr()->m_pMouse->capture();
      
      if(OgreFramework::getSingletonPtr()->m_pJoystick){
	OgreFramework::getSingletonPtr()->m_pJoystick->capture();
      }
      m_ActiveStateStack.back()->update(timeSinceLastFrame);
      
      OgreFramework::getSingletonPtr()->updateOgre(timeSinceLastFrame);
      
      OgreFramework::getSingletonPtr()->m_pRoot->renderOneFrame();
      
      timeSinceLastFrame = (OgreFramework::getSingletonPtr()->m_pTimer->getMilliseconds()/1000.0) - startTime;
    }
  }
}
