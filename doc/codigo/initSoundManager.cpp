void OgreFramework::initSoundManager()
{
    mSoundManager = OgreAL::SoundManager::createManager();
    mSoundManager->init();
    mSoundManager->setAudioPath("media/audio/");
} 
