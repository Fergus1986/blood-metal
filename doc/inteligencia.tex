\clearpage
\section{Inteligencia Artificial} 
\subsection{Introducción}
Para modelar el comportamiento de los enemigos de Blood\&Metal  se ha decidido implementar una máquina de estados finitos determinista. Dicha implementación se llevará a cabo mediante programación orientada a objetos.

Antes de comenzar a documentar el desarrollo llevado a cabo, los autores creen conveniente introducir de forma muy breve la idea principal de dos patrones de diseño utilizados para llevar a cabo la solución propuesta. En concreto, los dos patrones de diseño que se van a explicar serán el patrón \verb State  y el patrón \verb Singleton .

\subsection{Patrones de Diseño Utilizados}
Antes de comenzar a documentar el desarrollo llevado a cabo, los autores creen conveniente introducir de forma muy breve la idea principal de dos patrones de diseño utilizados para llevar a cabo la solución propuesta. En concreto, los dos patrones de diseño que se van a explicar serán el patrón \verb State  y el patrón \verb Singleton .

La idea fundamental del patrón de diseño \verb State, se centra en que el comportamiento de un objeto pueda verse alterado a medida que cambia el estado del objeto o su entorno. 
Este patrón propone una solución de implementación basada en herencia y clases abstractas.

Se define una clase abstracta \verb State, la cual incluirá los atributos y métodos comunes a todos los estados; con esto, se proporciona la base sobre la que se sustentará la implementación de todos y cada uno de los estados concretos del comportamiento a implementar.

Para implementar cada uno de los estados específicos del comportamiento propuesto, se declara una clase por cada uno de ellos. Estas clases heredan de la clase \verb State  todos los atributos y métodos que hayan sido declarados, los cuales se inicializan con sus valores correspondientes al estado en el caso de los atributos y realizarán su implementación concreta en el caso de los métodos.

Para que el objeto cuyo comportamiento queremos modelar pueda alterar su conducta debe haber alguna relación entre dicho objeto y los objetos de cada estado específico. Dicha relación se lleva a cabo a través del polimorfismo y se traduce en una referencia a la clase abstracta \verb State  que el objeto    modelado debe incluir en su implementación y que se corresponde con el estado actual del objeto. Dicha referencia irá alternando dinámicamente entre los distintos estados específicos según el estado del objeto y su entorno.

El último elemento de la solución propuesta por el patrón se trata de un mecanismo que haga posible la transición entre los diferentes estados del objeto.

Otro patrón utilizado, es el patrón \verb Singleton. Dicho patrón se utiliza para restringir la creación de objetos de una misma clase a una unidad. La idea principal es garantizar que sólo existe una única instancia de una clase y proporcionar un mecanismo o punto de acceso global a dicha instancia. 
\subsection{Solución Propuesta}
En la solución propuesta para Blood\&Metal, se ha implementado el patrón State que se acaba de describir. Se procede a explicar las características concretas de cada uno de los elementos y su correspondencia con el patrón explicado anteriormente.


\subsubsection{Clase EnemyState}
Esta es la clase abstracta de la cual heredan el resto de estados. En ella se declaran los atributos y métodos que serán comunes al conjunto de estados que definen el comportamiento. 
Sus componentes son:
\begin{itemize}
  \item Atributos:
  \begin{itemize}
    \item \verb _name:  Almacena el nombre del estado.
  \end{itemize}
    \item Métodos:
  \begin{itemize}
    \item \verb enter():  Incluye el código que se ejecuta al entrar al estado.	
    \item \verb update():  Incluye el código que se ejecuta mientras se permanece en ese estado.
    \item \verb exit():  Incluye el código que se ejecuta al salir del estado.
  \end{itemize}
\end{itemize}

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{imagenes/enemyState.png}
  \caption{Diagrama de herencia de la clase EnemyState}
  \label{enemystate}
\end{figure}


\subsubsection{Clase Enemy}
Esta clase es la encargada de definir a los enemigos  como tal, características, cuerpo gráfico, cuerpo físico y además incluye la referencia a un objeto de tipo \verb EnemyState  etiquetada como \verb _CurrentState  y que se corresponderá con el estado en que se encuentre el enemigo en cada instante de tiempo.

\subsubsection{Clase StateManager}

En la solución propuesta para Blood\& Metal se ha decidido delegar las tareas de inicialización, actualización y transición de estados en una clase implementada específicamente para ello. Dicha clase recibe el nombre de \verb StateManager. Este objeto necesita conocer tanto las instancias de enemigos como las de los estados específicos. La clase \verb StateManager  el implementa el patrón \verb Singleton  de \textbf{Ogre3D}, por lo que sólo se crea una instancia de esta clase y se hace desde el constructor del estado de Juego (\verb GameState). Es muy importante que sólo exista un \verb StateManager, encargado de gestionar los estados y transiciones de todos los enemigos, ya que si se creara un \verb StateManager  para cada instancia de enemigo el rendimiento se vería afectado notablemente.

Como se ha comentado anteriormente, esta clase debe conocer las instancias de enemigos, por lo que se inicializa un vector que contiene referencias a todos los enemigos que habrá presentes en el escenario. Cada vez que se cargue un nuevo nivel, el \verb StateManager  debe recibir la referencia a vector de enemigos mediante el método \verb SetEnemies() y posteriormente inicializar el valor del estado actual de cada enemigo a través del método \verb init(). Una vez hecho esto, todos los enemigos quedarán inicializados, es decir, todos tendrán un estado inicial asignado desde donde comenzará su ejecución. El componente principal de esta clase es el método \verb UpdateState() (ver listado \ref{statemanager}), el cual es invocado en cada iteración del bucle de juego y asume la responsabilidad de recorrer el vector de enemigos y actualizar cada uno de los estados actuales en los que se encuentran en cada instante los enemigos.  El primer paso a realizar es invocar el método \verb Update() del estado actual. Cada 
vez que se invoca un método \verb Update() de alguno de los estados, el valor de retorno se corresponde con el estado al que debe transitar. Para finalizar se invoca el método \verb Update() de enemigo para que actualice su representación gráfica y física en el grafo de escena.

\lstinputlisting[language=C++, label=statemanager, caption = Método UpdateState() de la clase StateManager]{codigo/statemanager.cpp}

Además, es necesario la implementación de un método para el cambio de estado de un enemigo concreto. El método \verb ChageState(Enemy,State) recibe un enemigo como parámetro y un estado al que tiene que transitar. Se debe comprobar si el estado actual del enemigo está inicializado, si es así, se invoca el método \verb exit() del estado actual y posteriormente se cambia el estado actual del enemigo por el estado que se recibe como parámetro y para terminar se invoca el método \verb enter() del nuevo estado actual.
	
\subsection{Estados}	
A continuación se procede a explicar cada uno de los estados específicos que componen la versión final de la máquina de estados implementada para los enemigos de Blood\& Metal.
Todos los estados que se describen, heredan de la clase abstracta \verb EnemyState.h  y cada uno de ellos describe un comportamiento específico que se podrá asignar a un objeto enemigo. A la hora de diseñar, se puede caer en la tentación de crear una instancia de cada estado específico para cada enemigo, pero en lugar de eso se opta por una solución en la que cada uno de los estados implementa el patrón \verb Singleton  proporcionado por \textbf{Ogre3D}, ya que el código a ejecutar en cada estado debe ser el mismo para todos los enemigos y si se crea una instancia de cada estado para cada enemigo el rendimiento empeoraría considerablemente.
\subsubsection{AtacarState}	
Al entrar a este estado, se inicializan los valores correspondientes al contador de tiempo que controla el retardo(\textbf{delay}) que el enemigo debe esperar desde que entra al estado \verb AtacarState  hasta que comienza a disparar.

Cuando el enemigo se encuentra en el estado \verb AtacarState , se ejecutará la función \verb Update() de dicho estado, en ese caso el enemigo debe frenar su avance y comenzar a disparar siempre y cuando haya expirado el delay que se establece desde que el enemigo detecta al protagonista hasta que comienza a disparar. En la versión final de la máquina de estados de enemigos de Blood\& Metal, este delay es de 300 milisegundos.

Por útlimo, se invoca el método \verb VisionRay() del enemigo que se está tratando en ese momento. Esta función simula el campo de visión del enemigo y el estado al que se debe transitar según se encuentre el enemigo y su entorno en ese momento. Este valor enumerado será a su vez el valor de retorno del método \verb Update() de \verb AtacarState .

Transiciones con Origen \verb AtacarState  y condiciones de transición:
\begin{itemize}
  \item Si se detecta al protagonista: \verb AtacarState .
  \item Si no hay obstáculos y Dirección = Derecha: \verb MirarDerState .
  \item Si no hay obstáculos y Dirección = Izquierda: \verb MirarIzqState .
  \item Si el enemigo es golpeado: \verb BuscarState .
\end{itemize}
\subsubsection{BuscarState}	
A este estado se transita desde cualquier otro estado y siempre que el enemigo recibe un golpe. 

Su única función es la de detectar el origen del disparo que ha golpeado al enemigo. Para ello, se lanzará un rayo en la dirección que esté mirando el enemigo y de mayor distancia que el resto de rayos utilizados en los demás estados. Si este rayo alcanza al protagonista el enemigo debe transitar al estado de ruta correspondiente a la dirección que mira el enemigo para avanzar hasta encontrarse con el enemigo. El motivo de que la distancia sea mayor es porque el protagonista puede disparar a un enemigo desde una posición más alejada que la distancia del rayo convencional que utilizan el resto de estados, por lo que el enemigo, aunque el protagonista estuviese en frente de él, no sería capaz de detectarlo. De esta manera, en el caso de que el rayo no colisione con el protagonista, quiere decir que se encuentra detrás de el enemigo, por tanto éste debe transitar al estado de mirar, correspondiente a la dirección contraria a la actual.

Transiciones con origen \verb BuscarState  y condiciones de transición:	
\begin{itemize}
  \item Si se detecta al protagonista y Dirección = Izquierda: \verb RutaIzqState .
  \item Si se detecta al protagonista y Dirección = Derecha: \verb RutaDerState .
  \item Si no se detecta al protagonista y Dirección = Derecha: \verb MirarIzqState .
  \item Si no se detecta al protagonista y Dirección = Izquierda: \verb MirarDerState .
\end{itemize}

\subsubsection{MirarDerState}
Lo primero que se tiene que hacer al entrar en este estado es invocar el método \verb signal_right() de la clase Enemy para establecer la dirección y posteriormente establecer los límites de patrulla, estableciendo el límite izquierdo en la posición en el eje x actual del enemigo y el límite derecho esa posición más un desplazamiento. Con esto se consigue que el enemigo altere dinámicamente su ruta según su entorno, por ejemplo, si a este estado se ha llegado a partir del evento generado cuando se detecta un hueco en el escenario por el que no se puede pasar u otro enemigo patrullando por la misma zona.

El código del método \verb Update() de éste estado, debe comenzar frenando el protagonista, mediante la invocación del método \verb stop() de \verb Enemy . Después, el enemigo lanza un rayo, si detecta al enemigo devuelve  el estado \verb AtacarState, si detecta a otro enemigo devuelve el estado \verb MirarIzqState  y en el resto de casos devolverá el estado \verb RutaDerState  para que el enemigo continúe con su patrulla.

Transiciones con origen \verb MirarDerState  y condiciones de transición:
\begin{itemize}
  \item Si se detecta al protagonista: \verb AtacarState .
  \item Si se detecta a otro enemigo: \verb MirarIzqState .
  \item Si no hay obstáculos: \verb RutaDerState .
  \item Si es golpeado: \verb BuscarState .
\end{itemize}
\subsubsection{MirarIzqState}
Este es el estado opuesto al anterior, varían en los conceptos relativos a la dirección del enemigo.
En concreto, los puntos que varían son:
\begin{itemize}
  \item Se invoca el método \verb signal_left() , para establecer la dirección en vez de \verb signal_right() .
  \item Al establecer los límites de patrulla, el límite derecho sería ahora la posición actual en el eje x del enemigo y el límite izquierdo, la posición actual menos un desplazamiento.
  \item En el método \verb Update() si se detecta a un enemigo se devolverá el estado \verb MirarDerState si se detecta a un enemigo y si no se detecta ni otros enemigos ni al protagonista, devolverá el estado \verb RutaIzqState .
\end{itemize}
Transiciones con origen \verb MirarIzqState  y condiciones de transición:
\begin{itemize}
  \item Si se detecta al protagonista: \verb AtacarState .
  \item Si se detecta a otro enemigo: \verb MirarDerState .
  \item Si no hay obstáculos: \verb RutaIzqState .
  \item Si es golpeado: \verb BuscarState .
\end{itemize}
\subsubsection{RutaDerState}

Mientras el enemigo permanece en este estado su función será la de patrullar por el escenario a la espera de que  se acerque el enemigo. Lo primero que debe hacer es comprobar si puede continuar su avance. Con el método \verb RayGround() de la clase \verb Enemy  puede comprobar si existe un hueco en la  siguiente posición del escenario y en ese caso transitar al estado \verb MirarIzqState. También debe comprobar que no ha sobrepasado el límite derecho de patrulla establecido, en caso contrario también  cambia al estado \verb MirarIzqState. Si ninguna de estas condiciones impide el avance, se invoca el método \verb signal_right(), para avanzar y posteriormente el método \verb VisionRay() para detectar el entorno.

Transiciones con origen \verb RutaDerState y  condiciones de transición:
\begin{itemize}
  \item Si se detecta al protagonista: \verb AtacarState .
  \item Si se detecta un hueco en el escenario: \verb MirarIzqState .
  \item Si se detecta a otro enemigo: \verb MirarIzqState .
  \item Si se sobrepasa el límite de patrulla: \verb MirarIzqState .
  \item Si no hay obstáculos:  \verb RutaDerState .
  \item Si es golpeado: \verb BuscarState .
\end{itemize}

\subsubsection{RutaIzqState}

Este estado es el opuesto al estado descrito en el párrafo anterior. El principal cambio es la dirección  del enemigo. Los eventos que generaban transiciones al estado \verb MirarIzqState  en el estado anterior, ahora cambian al estado \verb MirarDerState, tanto los huecos, como el límite de patrulla. Además la llamada al método \verb signal_right() se sustituye por \verb signal_left() .

Transiciones con origen \verb RutaDerState  y condiciones de transición:
\begin{itemize}
  \item Si se detecta al protagonista: \verb AtacarState .
  \item Si se detecta un hueco en el escenario: \verb MirarDerState .
  \item Si se detecta a otro enemigo: \verb MirarDerState .
  \item Si se sobrepasa el límite de patrulla: \verb MirarDerState .
  \item Si no hay obstáculos: \verb RutaIzqState .
  \item Si es golpeado: \verb BuscarState .
\end{itemize}

\subsection{Diagramas de Estados}
Para facilitar la legibilidad del diagrama de estados de la versión final de la máquina de estados, se ha diseñado un diagrama simplificado donde no se diferencia entre los estados que varían entre ellos en la dirección del personaje:

\begin{itemize}
 \item \verb RutaDerState/RutaIzqState .
 \item \verb MirarDerState/MirarIzqState .
\end{itemize}
representándose como un único estado.

 \begin{figure}[!ht]
   \centering
   \includegraphics[width= 19cm, angle=-90]{imagenes/IAStatesSimple.eps}
   \caption{Diagrama de IA simplificado}
   \label{IA_simplificado}
 \end{figure}
 
\begin{figure}[!ht]
   \centering
   \includegraphics[width= 19cm, angle=-90]{imagenes/IAStates.eps}
   \caption{Diagrama de IA completo}
   \label{IA_completo}
 \end{figure}



	
